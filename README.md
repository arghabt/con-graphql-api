# GES GraphQL API

The GraphQL API is the server that the apps use to communicate with the GES Tools database. This file will focus on development, for how to access the server to develop apps see GraphQL API Client or access the bundled GraphiQL at `/graphql` to see what functionality is available.

## Setup
### Database
The database uses MSSQL, see the GES Database repository for more info on how to set it up. Two databases are required: `GESDB` and `GESExtraDB`.

### Clone the repository
node_modules are commited

### Set up config files
Copy `config/local-EXAMPLE.json` to `config/local.json` and fill in the database settings. More settings can be overriden, see `default.json`.

### Run
Run `npm start` you should see roughly the output below, any errors on startup will be displayed here, almost always these are database related.
```
2017-08-14T10:14:18.850Z - info: Running in: development mode
2017-08-14T10:14:26.823Z - info: Using dummy Auth
2017-08-14T10:14:27.206Z - verbose: Generated GraphQL types for: CapabilityArea, Clearance, Customer, ...
2017-08-14T10:14:28.157Z - info: Listening on: http://localhost:4000/graphql
```

**The server needs to be restarted after every change, a tool such as [watchman](https://facebook.github.io/watchman/) could solve this**




## Directory structure
### `/api`
Contains the business logic

### `/auth`
Contains authentication and authorisation functionality, including the token generation, authentication functions, express middleware, and authorisation functions.

`AuthUser.js` is the only file that will need to be changed regularly as it contains the authorisation functions.

### `/database`
Abstracation layer on top of the MSSQL database, `apiToDBType.js` contains the mapping between the GraphQL types and the database types.

### `/graphql`
Layer between the api functions and GraphQL.














