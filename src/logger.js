// Winston instances for logging
const config = require("config");
const winston = require("winston");

const transports = config.get("logging.transport");
const level = config.get("logging.level");


let sysTransports = [];
let requestTransports = [];
if(transports.includes("console")){
  sysTransports.push(new winston.transports.Console({
    timestamp : true,
    colorize : true,
  }));
  // Don't ever log requests to the console
}
if(transports.includes("file")){
  sysTransports.push(new winston.transports.File({
    timestamp : true,
    filename : config.util.getEnv("NODE_ENV") + ".log",
  }));
  requestTransports.push(new winston.transports.File({
    timestamp : true,
    label : "GQL Request",
    filename : config.util.getEnv("NODE_ENV") + "-requests.log",
  }));
}

const sysLogger = new winston.Logger({
  level : level,
  transports : sysTransports
});

const requestLogger = new winston.Logger({
  level : level,
  transports : requestTransports
});

module.exports = {
  sysLogger,
  requestLogger
};
