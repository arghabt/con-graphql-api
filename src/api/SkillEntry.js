// Skill entry functions

const APIGenerator = require("./APIGenerator");
const makeAPIFunction = APIGenerator.makeAPIFunction;
const { AuthorisationError } = require("./Errors.js");

const skillEntryAPI = new APIGenerator({
  tableName : "SkillEntries",
  schemaName : "skills",
  columns : [
    [ "staffID", "FK" ],
    [ "skillID", "FK" ],
    [ "skillLevel", "Int" ],
    [ "lastUpdateTimestamp", "Date" ]
  ]
});

// Add filter to remove skill levels === 0
/*skillEntryAPI.addFilter([ "search", "getAll" ], function(results, _, user){
  return results.filter(({ skillLevel }) => skillLevel > 0);
});*/
skillEntryAPI.addSearchOption({
  optionName : "inactive",
  description : "Whether skill entry is related to an active person",
  type : "Bit",
  queryFragment : function(is){
    if(is){
      return `EXISTS (
        SELECT 1 AS A FROM [staff].[Staff] WHERE
            (${this.table}.[startDate] > GETDATE() OR ${this.table}.[endDate] < GETDATE())
          AND
            [staff].[Staff].[ID] = ${this.table}.[staffID]
      )`;
    }else{
      return `EXISTS (
        SELECT 1 AS A FROM [staff].[Staff] WHERE (
            ([staff].[Staff].[startDate] <= GETDATE() OR [staff].[Staff].[startDate] IS NULL)
          AND
            ([staff].[Staff].[endDate] >= GETDATE() OR [staff].[Staff].[endDate] IS NULL)
        ) AND [staff].[Staff].[ID] = ${this.table}.[staffID]
      )`;
    }
  }
});


skillEntryAPI.addSearchOption({
  optionName : "returnZeros",
  type : "Bit",
  queryFragment : null
});

skillEntryAPI.addSQLManipulator([ "getAll", "search" ], function(query, args, user){
  if(!args.returnZeros){
    return query + ` AND ${this.table}.[skillLevel] > 0`;
  }else{
    return query;
  }
});


const makeFunctions = require("./skillMutationFunctions");
const mutationfunctions = makeFunctions({ IDName : "staffID", apiName : "skillEntry" });


function createSkillEntryFunction(name){
  return makeAPIFunction(function skillEntryWrapper(inputObject, user, higherTransaction){
    

    return user.canEditStaffMember(inputObject.staffID)
      .tap(allowed => {
        if(!allowed){
          throw new AuthorisationError("You do not have permission to do that", `staff.${name}`);
        }
      })
      .then(() => mutationfunctions[name].call(
        this,
        Object.assign({}, inputObject, { lastUpdateTimestamp : new Date() }),
        user,
        higherTransaction
      ));
  });
}


// Create additional functions to edit own skillEntries
function createMySkillEntryFunction(name){
  return makeAPIFunction(function my_skillEntryWrapper(inputObject, user, higherTransaction){
    if(!user.isLoggedIn()){
      throw new AuthorisationError("You do not have permission to do that", `staff.${name}`);
    }

    // If logged in, take the current users id and pass that to the function
    return mutationfunctions[name].call(
      this,
      Object.assign({}, inputObject, { staffID : user.getID(), lastUpdateTimestamp : new Date() }),
      user,
      higherTransaction
    );
  });
}

for(let f in mutationfunctions){
  if(mutationfunctions.hasOwnProperty(f)){
    skillEntryAPI[f] = createSkillEntryFunction(f);
    skillEntryAPI[`my_${f}`] = createMySkillEntryFunction(f);
  }
}





module.exports = skillEntryAPI;
