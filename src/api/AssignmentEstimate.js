// Assignment Estimate API functions

const APIGenerator = require("./APIGenerator");


const assignmentEstimateAPI = new APIGenerator({
  tableName : "AssignmentEstimates",
  schemaName : "projects",
  columns : [
    [ "assignmentID", "FK" ],
    [ "timestamp", "DateTime" ],
    [ "submittedBy", "FK" ],
    [ "startDatetime", "DateTime" ],
    [ "endDatetime", "DateTime" ],
    [ "approved", "Bit" ],
    [ "percentageOfTimeOnAssignment", "Int" ],
    [ "fedUp", "Bit" ]
  ]
});



module.exports = assignmentEstimateAPI;
