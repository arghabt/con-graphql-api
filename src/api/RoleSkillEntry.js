// Role skill entry functions
const APIGenerator = require("./APIGenerator");
const makeAPIFunction = APIGenerator.makeAPIFunction;

const roleSkillsAPI = new APIGenerator({
  tableName : "Roles_Skills",
  schemaName : "projects",
  columns : [
    [ "roleID", "FK" ],
    [ "skillID", "FK" ],
    [ "skillLevel", "Int" ]
  ]
});

// Add filter to remove skill levels === 0
roleSkillsAPI.addFilter([ "search", "getAll" ], function(results, _, user){
  return results.filter(({ skillLevel }) => skillLevel > 0);
});


const makeFunctions = require("./skillMutationFunctions");
const mutationfunctions = makeFunctions({ IDName : "roleID", apiName : "roleSkill" });

for(let f in mutationfunctions){
  if(mutationfunctions.hasOwnProperty(f)){
    roleSkillsAPI[f] = makeAPIFunction(mutationfunctions[f]);
  }
}




/*
roleSkillsAPI.create = makeAPIFunction(function create({ roleID, skillEntry }, user, higherTransaction){
  return Promise.try(() => {
    if(!roleID){
      throw new APIInputError("No role ID provided", "roleID", "roleSkill.create");
    }

    skillEntry.roleID = roleID;

    let required = [
      "roleID",
      "skillID",
      "skillLevel"
    ];

    let queryFields = [
      "roleID",
      "skillID",
      "skillLevel"
    ];

    let remainingFields = queryFields.filter(field => (skillEntry[field] !== undefined && skillEntry[field] !== ""));
    required.forEach(field => {
      if(!remainingFields.includes(field)){
        throw new APIInputError("Field " + field + " is required", field, "roleSkill.create");
      }
    });

    // Do not create skill if level < 1
    if(skillEntry.skillLevel < 1){
      return { new_id : null };
    }


    let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
    return transactionPromise.then(transaction => {
      // Is this an update or an insert?
      return this.search({ roleID : roleID, skillID : skillEntry.skillID }, user, transaction).then(oldEntries => {
        // Construct query based on entries
        let query = "";
        if(oldEntries.length > 0){
          query = `UPDATE ${this.table} SET
            [skillLevel] = @skillLevel
            WHERE [roleID] = @roleID AND [skillID] = @skillID`;
        }else{
          query = `INSERT INTO ${this.table}
            ([roleID], [skillID], [skillLevel])
            VALUES (@roleID, @skillID, @skillLevel);
            SELECT SCOPE_IDENTITY() AS new_id;`;
        }

        let params = remainingFields.map(field => [
          field,
          undefined,
          skillEntry[field]
        ]);

        return queryDB(query, params, transaction)
          .then(result => {
            let new_id = (oldEntries.length > 0) ? oldEntries[0].ID : result[0].new_id;

            // Don't commit the transaction if it doesn't belong to us
            if(higherTransaction){
              return new_id;
            }else{
              return transaction.commit().return({ ID : new_id });
            }
          })
          .catch((e) => {
            if(higherTransaction){
              throw e;
            }else{
              return transaction.rollback().throw(e);
            }
          });
      });
    });
  });
});



roleSkillsAPI.createMany = makeAPIFunction(function createMany({ roleID, skillEntries }, user, higherTransaction){
  // Get a transaction
  let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
  return transactionPromise.then(transaction => {

    skillEntries = skillEntries.filter((skill) => {
      return skill.skillLevel > 0;
    });

    return Promise.reduce(skillEntries || [], (_, skill) => {
      return this.create({
        roleID : roleID,
        skillEntry : skill
      }, user, transaction);
    }, null)

    .then((ids) => {
      // Don't commit the transaction if it doesn't belong to us
      if(higherTransaction){
        return ids;
      }else{
        return transaction.commit().return(ids);
      }
    })

    .catch((e) => {
      if(higherTransaction){
        throw e;
      }else{
        return transaction.rollback().throw(e);
      }
    });
  });
});



roleSkillsAPI.del = makeAPIFunction(function del({ roleID, skillID, skillEntryID }, user, higherTransaction){
  return Promise.try(() => {
    if(!roleID){
      throw new APIInputError("No role ID provided", "roleID", "roleSkill.delete");
    }
    if(!skillID || !skillEntryID){
      throw new APIInputError("Either skillID or skillEntryID is required", "skillID,skillEntryID", "roleSkill.delete");
    }
    // Get a transaction
    let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
    return transactionPromise.then(transaction => {
      let colName = skillEntryID ? "skillEntryID" : "skillID";
      let query = `DELETE FROM ${this.table}
        WHERE [roleID] = @roleID AND [${colName}] = @${colName}`;

      let params = [
        [ "roleID", "Int", roleID ],
        [ colName, "Int", skillEntryID || skillID ]
      ];

      return queryDB(query, params, transaction)
        .then(() => {
          // Don't commit the transaction if it doesn't belong to us
          if(!higherTransaction){
            return transaction.commit().return();
          }
        })

        .catch((e) => {
          if(higherTransaction){
            throw e;
          }else{
            return transaction.rollback().throw(e);
          }
        });
    });
  });
});


roleSkillsAPI.delMany = makeAPIFunction(function delMany({ roleID, skills }, user, higherTransaction){
  // Get a transaction
  let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
  return transactionPromise.then(transaction => {

    return Promise.reduce(skills || [], (_, skill) => {
      return this.del({
        roleID : roleID,
        skillID : skill.skillID,
        skillEntryID : skill.skillEntryID
      }, user, transaction);
    }, null)

    .then(() => {
      // Don't commit the transaction if it doesn't belong to us
      if(!higherTransaction){
        return transaction.commit().return();
      }
    })

    .catch((e) => {
      if(higherTransaction){
        throw e;
      }else{
        return transaction.rollback().throw(e);
      }
    });
  });
});


roleSkillsAPI.overwriteSkills = makeAPIFunction(function overwriteSkills({ roleID, skillEntries }, user, higherTransaction){
  return Promise.try(() => {
    if(!roleID){
      throw new APIInputError("No role ID provided", "roleID", "roleSkill.edit");
    }

    skillEntries = skillEntries.filter((skill) => {
      return skill.skillLevel > 0;
    });

    // Get a transaction
    let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
    return transactionPromise.then(transaction => {
      // Delete the ones that do not exist in array
      let query = `DELETE FROM ${this.table}
        WHERE [roleID] = @roleID
        AND [skillID] NOT IN (${skillEntries.map((skill, i) => `@skillID_${i}`).join(",")})`;

      let params = skillEntries.map((skill, i) => [
        `skillID_${i}`,
        "Int",
        skill.skillID
      ]);

      params.push([
        "roleID", 
        "Int",
        roleID
      ]);

      return queryDB(query, params, transaction)
        .then(() => {
          // Create the rest..
          return this.createMany({ roleID, skillEntries }, user, transaction);
        })

        .then((ids) => {
          // Don't commit the transaction if it doesn't belong to us
          if(higherTransaction){
            return ids;
          }else{
            return transaction.commit().return(ids);
          }
        })

        .catch((e) => {
          if(higherTransaction){
            throw e;
          }else{
            return transaction.rollback().throw(e);
          }
        });
    });
  });
});
*/

module.exports = roleSkillsAPI;
