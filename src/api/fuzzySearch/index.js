// TODO: Set this to a more appropriate value depending on how "Fuzzy" the search should be.
const FUZZY_THRESHOLD = 1;
const FUZZY_SKILL_THRESHOLD = 6;




exports.performFuzzySearch = function performFuzzySearch(input, unfilteredStaffData, user, inputTypes, fuzzySearchFieldsSupported){
  // Getting fields to run fuzzy search on
  let fuzzySearchFields = fuzzySearchFieldsSupported.filter(field => input[field]);
  let hasAtLeastTheseSkillsIndex = fuzzySearchFields.indexOf("hasAtLeastTheseSkills");
  if(hasAtLeastTheseSkillsIndex > -1){
    fuzzySearchFields.splice(hasAtLeastTheseSkillsIndex, 1);
  }

  // Working out the type of each field
  let fuzzySearchFieldTypes = fuzzySearchFields.map(fuzzySearchField => getTypeOfSearchField(inputTypes, fuzzySearchField));


  let fuzzedStaffMembers = [];
  // Working out the fuzzy rank in the skills field
  unfilteredStaffData.forEach(staffMember => {
    let getUnmatchedFieldsResults = getUnmatchedFields(input, staffMember, fuzzySearchFields, fuzzySearchFieldTypes);
    let { inThreshold : fieldsInThreshold, unmatchedFields, fuzzedFieldsRank } = getUnmatchedFieldsResults;

    // Escape early if this staff member does not match
    if(!fieldsInThreshold){
      return;
    }



    let unmatchedSkills = [];
    let fuzzedSkillsRank = 0;
    let skillsInThreshold = true;
    // If there are skills give, fuzz on them
    if(Array.isArray(input.hasAtLeastTheseSkills) && input.hasAtLeastTheseSkills.length > 0){
      let getUnmatchedSkillsResults = getUnmatchedSkills(input.hasAtLeastTheseSkills, staffMember.skills);
      ({ inThreshold : skillsInThreshold, unmatchedSkills, fuzzedSkillsRank } = getUnmatchedSkillsResults);

      if(!skillsInThreshold){
        // The skills fuzz is not in the threshold, escape here
        return;
      }
    }


    let totalRank = fuzzedFieldsRank + fuzzedSkillsRank;


    fuzzedStaffMembers.push({
      fuzzedFields: {
        fields: unmatchedFields,
        rank: fuzzedFieldsRank
      },
      fuzzedSkills: {
        skills: unmatchedSkills,
        rank: fuzzedSkillsRank
      },
      rank : totalRank,
      staffMember: staffMember
    });
  });


  // Sorting the array of filtered staff data in ascending order by the number of fuzzy fields.
  // Staff with fewer fuzzy fields go at the top
  if(fuzzedStaffMembers.length > 1){
    fuzzedStaffMembers.sort((a, b) => a.rank - b.rank);
  }

  return fuzzedStaffMembers;
};


/*
 * Generates an array of fields names which are "Fuzzy"
 * @param input
 * @param staffMember
 * @param fuzzyFields
 * @param fuzzyFieldsTypes
 */
function getUnmatchedFields(input, staffMember, fuzzyFields, fuzzyFieldsTypes) {
  let unmatchedFields = [];
  let fuzzedFieldsRank = 0;

  for(let i = 0; i < fuzzyFields.length; i++){
    let searchFieldName = fuzzyFields[i];
    let searchFieldType = fuzzyFieldsTypes[i];
    let searchInput = input[searchFieldName];
    let hasMatched = false;

    if(searchFieldType._isArrayType){
      // Array of elements
      if(searchFieldType._originalType === "FK"){
        hasMatched = integerArrayTypeHandler(searchInput, staffMember[searchFieldType._originalName]);
      }
    }else{
      // Non Array Build the parser in here
      if(searchFieldType.type === "FK"){
        hasMatched = integerTypeHandler(searchInput, staffMember[searchFieldName]);
      }
    }

    if(!hasMatched){
      unmatchedFields.push(searchFieldName);
      fuzzedFieldsRank += 1;

      // Check here if the totalRank is above the threshold, as we can end early!
      if(fuzzedFieldsRank > FUZZY_THRESHOLD){
        return {
          inThreshold : false
        };
      }
    }
  }

  return {
    inThreshold : true,
    unmatchedFields,
    fuzzedFieldsRank
  };
}

/*
 * Returns an array of skillIDs for unmatched skills
 * @param skills
 * @param staffSkills
 */
function getUnmatchedSkills(skillsInSearch, staffSkills) {
  let unmatchedSkills = [];
  let fuzzedSkillsRank = 0;

  // Sort the skills by skill ID
  skillsInSearch.sort((a, b) => a.skillID - b.skillID);

  let lastStaffSkillFoundAtIndex = 0;
  for(let i = 0; i < skillsInSearch.length; i++){
    let searchSkill = skillsInSearch[i];
    let skillHasBeenFound = false;

    for(let j = lastStaffSkillFoundAtIndex; j < staffSkills.length; j++){
      let staffSkill = staffSkills[j];

      if(searchSkill.skillID === staffSkill.skillID){
        lastStaffSkillFoundAtIndex = j;
        skillHasBeenFound = true;
        if(searchSkill.skillLevel > staffSkill.skillLevel){
          let rank = searchSkill.skillLevel - staffSkill.skillLevel;
          fuzzedSkillsRank += rank;

          unmatchedSkills.push({
            skillID: searchSkill.skillID,
            rank
          });
        }

        // Skill is found, break the loop here
        break;
      }
    }

    if(!skillHasBeenFound){
      fuzzedSkillsRank += searchSkill.skillLevel;

      unmatchedSkills.push({
        skillID: searchSkill.skillID,
        rank: searchSkill.skillLevel
      });
    }

    // Check here if the totalRank is above the threshold, as we can end early!
    if(fuzzedSkillsRank > FUZZY_SKILL_THRESHOLD){
      return {
        inThreshold : false
      };
    }
  }

  return {
    inThreshold : true,
    unmatchedSkills,
    fuzzedSkillsRank
  };
}

/*
 * Checking the type of a field declared using GraphQL.
 * @param inputTypes
 * @param fieldName
 */
function getTypeOfSearchField(inputTypes, fieldName) {
  return inputTypes.find(t => t.name === fieldName);
}

/*
 * Handler for arrays of FK and other integer variable types apart from skills.
 * @param searchValue
 * @param staffValue
 */
function integerArrayTypeHandler(searchValue, staffValue) {
  return searchValue.some(value => integerTypeHandler(value, staffValue));
}

/*
 * Handler for PK / FK Etc
 * @param searchValue
 * @param staffValue
 */
function integerTypeHandler(searchValue, staffValue) {
  return searchValue === staffValue;
}