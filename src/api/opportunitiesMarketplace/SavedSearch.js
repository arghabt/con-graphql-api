// CTOTheme API functions

const APIGenerator = require("../APIGenerator");
const database = require("../../database/extra");


const savedSearchesAPI = new APIGenerator({
  tableName : "SavedSearches",
  schemaName : "opportunitiesMarketplace",
  database : database,
  columns : [
    [ "name", "Text" ],
    [ "staffID", "FK" ],
    [ "searchJSON", "Text" ]
  ]
});



module.exports = savedSearchesAPI;
