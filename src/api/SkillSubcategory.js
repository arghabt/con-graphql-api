// SkillSubcategories API functions

const APIGenerator = require("./APIGenerator");
const generateSimpleAPIFunctions = require("./simpleAPIGenerator/generateSimpleAPIFunctions");

const SkillSubcategoryAPI = new APIGenerator({
  tableName : "SkillSubcategories",
  schemaName : "skills",
  columns : [
    [ "name", "Text" ],
    [ "professionID", "FK" ],
    [ "subSubjectMatterOwnerID", "FK" ],
    [ "confluenceURL", "Text" ]
  ]
});

generateSimpleAPIFunctions(SkillSubcategoryAPI);



module.exports = SkillSubcategoryAPI;
