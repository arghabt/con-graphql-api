const MentorRelationship = require("./MentorRelationship");

module.exports = {
  hasRelationshipsRemainingSearchOption : function(field){

    return {
      optionName : "hasRelationshipsRemaining",
      type : "Bit",
      queryFragment : function(has){
        if(has){
          return `(SELECT COUNT(1) FROM ${MentorRelationship.table} WHERE
              [status] < 4 AND
              [${field}] = ${this.table}.[ID]
            ) < ${this.table}.[maxRelationships]`;
        }else{
          return `(SELECT COUNT(1) FROM ${MentorRelationship.table} WHERE
              [status] < 4 AND
              [${field}] = ${this.table}.[ID]
            ) >= ${this.table}.[maxRelationships]`;
        }
      }
    };
  }
};
