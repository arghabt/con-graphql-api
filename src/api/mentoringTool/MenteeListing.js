// Mentee listing API functions
const { AuthorisationError, APIInputError } = require("../Errors.js");

const common = require("./common");

const APIGenerator = require("../APIGenerator");
const { queryDB, getOne } = require("../../database");

const MentorRelationship = require("./MentorRelationship");

const menteeListing = new APIGenerator({
  tableName : "MenteeListings",
  schemaName : "mentoringtool",
  columns : [
    [ "staffID", "FK" ],
    [ "skillID", "FK" ],
    [ "timestamp", "DateTime" ],
    [ "maxRelationships", "Int" ]
  ]
});

menteeListing.addSearchOption(common.hasRelationshipsRemainingSearchOption("menteeListingID"));

// Mutations
menteeListing.create = function create({ menteeListing }, user) {
  return user.canCreateMenteeListing()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to create a new mentee listing", "menteeListing.create");
      }
    })
    .then(() => {
      let requiredFields = [
        "staffID",
        "skillID"
      ];

      // Set staffID as the current user
      menteeListing.staffID = user.getID();

      let queryFields = [
        "staffID",
        "skillID",
        "maxRelationships"
      ];

      let remainingFields = queryFields.filter(field => (menteeListing[field] !== undefined));
      requiredFields.forEach(field => {
        if(!remainingFields.includes(field)){
          throw new APIInputError("Field " + field + " is required", field, "menteeListing.create");
        }
      });

      // Defining the query
      let query = `INSERT INTO ${this.table}
        (${remainingFields.join(",")})
        VALUES (${remainingFields.map(field => `@${field}`).join(",")});
        SELECT SCOPE_IDENTITY() AS new_id;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        menteeListing[field]
      ]);

      return getOne(query, params)
        .then(({ new_id }) => ({
          ID : new_id
        }));
    });
};

menteeListing.edit = function edit({ menteeListingID, menteeListing }, user) {
  return user.canEditMenteeListing(menteeListingID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to edit that mentee listing", "menteeListing.edit");
      }
    })
    .then(() => {
      let requiredFields = [
        "maxRelationships"
      ];

      let queryFields = [
        "maxRelationships"
      ];

      let remainingFields = queryFields.filter(field => (menteeListing[field] !== undefined));
      requiredFields.forEach(field => {
        if(!remainingFields.includes(field)){
          throw new APIInputError("Field " + field + " is required", field, "menteeListing.create");
        }
      });


      let query = `UPDATE ${this.table} SET
        ${remainingFields.map(field => {
          return `${field} = @${field}`;
        }).join(",")}
        WHERE [ID] = @menteeListingID`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        menteeListing[field]
      ]);
      // Pushing the mentee listing ID
      params.push([ "menteeListingID", undefined, menteeListingID ]);

      return queryDB(query, params)
        .return({ ID : menteeListingID});
    });
};

menteeListing.del = function del({ menteeListingID }, user) {
  if(!menteeListingID){
    return Promise.reject(new APIInputError("Mentee Listing ID is not provided", "menteeListingID", "menteeListing.delete"));
  }
  return user.canEditMenteeListing(menteeListingID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to delete that mentee listing", "menteeListing.delete");
      }
    })
    .then(() => {
      // Search to see if there are any MentorRelationships with this mentee listing
      return MentorRelationship.search({ menteeListingID }, user);
    })
    .then(data => {
      if(data && data.length > 0){
        throw new APIInputError("One or more existing mentor relationships exist with this mentee listing ID", "menteeListingID", "menteeListing.delete");
      }
    })
    .then(() => {
      let query = `DELETE FROM ${this.table}
        WHERE [ID] = @menteeListingID`;

      let params = [ "menteeListingID", "Int", menteeListingID ];

      return queryDB(query, params);
    });
};

module.exports = menteeListing;