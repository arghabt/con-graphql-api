// Mentor Listing API functions
const { AuthorisationError, APIInputError } = require("../Errors.js");

const common = require("./common");

const APIGenerator = require("../APIGenerator");
const { queryDB, getOne } = require("../../database");

const MentorRelationship = require("./MentorRelationship");

const mentorListing = new APIGenerator({
  tableName : "MentorListings",
  schemaName : "mentoringtool",
  columns : [
    [ "staffID", "FK" ],
    [ "skillID", "FK" ],
    [ "timestamp", "DateTime" ],
    [ "maxRelationships", "Int" ]
  ]
});

mentorListing.addSearchOption(common.hasRelationshipsRemainingSearchOption("mentorListingID"));

// Mutations
mentorListing.create = function create({ mentorListing }, user) {
  return user.canCreateMentorListing()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to create a new mentor listing", "mentorListing.create");
      }
    })
    .then(() => {
      let requiredFields = [
        "staffID",
        "skillID"
      ];

      // Set staffID as the current user
      mentorListing.staffID = user.getID();

      let queryFields = [
        "staffID",
        "skillID",
        "maxRelationships"
      ];

      let remainingFields = queryFields.filter(field => (mentorListing[field] !== undefined));
      requiredFields.forEach(field => {
        if(!remainingFields.includes(field)){
          throw new APIInputError("Field " + field + " is required", field, "mentorListing.create");
        }
      });

      // Defining the query
      let query = `INSERT INTO ${this.table}
        (${remainingFields.join(",")})
        VALUES (${remainingFields.map(field => `@${field}`).join(",")});
        SELECT SCOPE_IDENTITY() AS new_id;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        mentorListing[field]
      ]);

      return getOne(query, params)
        .then(({ new_id }) => ({
          ID : new_id
        }));
    });
};

mentorListing.edit = function edit({ mentorListingID, mentorListing }, user) {
  return user.canEditMentorListing(mentorListingID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to edit that mentor listing", "mentorListing.edit");
      }
    })
    .then(() => {
      let requiredFields = [
        "maxRelationships"
      ];

      let queryFields = [
        "maxRelationships"
      ];

      let remainingFields = queryFields.filter(field => (mentorListing[field] !== undefined));
      requiredFields.forEach(field => {
        if(!remainingFields.includes(field)){
          throw new APIInputError("Field " + field + " is required", field, "mentorListing.create");
        }
      });


      let query = `UPDATE ${this.table} SET
        ${remainingFields.map(field => {
          return `${field} = @${field}`;
        }).join(",")}
        WHERE [ID] = @mentorListingID`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        mentorListing[field]
      ]);
      // Pushing the mentor listing ID
      params.push([ "mentorListingID", undefined, mentorListingID ]);

      return queryDB(query, params)
        .return({ ID : mentorListingID });
    });
};

mentorListing.del = function del({ mentorListingID }, user) {
  if(!mentorListingID){
    return Promise.reject(new APIInputError("Mentor Listing ID is not provided", "mentorListingID", "mentorListing.delete"));
  }
  return user.canEditMentorListing(mentorListingID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to delete that mentor listing", "mentorListing.delete");
      }
    })
    .then(() => {
      // Search to see if there are any MentorRelationships with this mentor listing
      return MentorRelationship.search({ mentorListingID }, user);
    })
    .then((data) => {
      if(data && data.length > 0){
        throw new APIInputError("One or more existing mentor relationships exist with this mentor listing ID", "mentorListingID", "mentorListing.delete");
      }
    })
    .then(() => {
      let query = `DELETE FROM ${this.table}
        WHERE [ID] = @mentorListingID`;

      let params = [ "mentorListingID", "Int", mentorListingID ];

      return queryDB(query, params);
    });
};

module.exports = mentorListing;