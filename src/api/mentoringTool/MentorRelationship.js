// Mentor relationship API functions
const Promise = require("bluebird");
const { AuthorisationError, APIInputError } = require("../Errors.js");

const APIGenerator = require("../APIGenerator");
const { queryDB, getOne } = require("../../database");

const mentorRelationship = new APIGenerator({
  tableName : "MentorRelationships",
  schemaName : "mentoringtool",
  columns : [
    [ "mentorListingID", "FK" ],
    [ "menteeListingID", "FK" ],
    [ "status", "FK" ],
    [ "lastUpdate", "DateTime" ],
    [ "wasSentByMentor", "Bit" ]
  ]
});

// Mutations
mentorRelationship.create = function create({ mentorRelationship }, user) {
  const MentorListing = require("./MentorListing");
  const MenteeListing = require("./MenteeListing");
  // Get the listings so they can be passed into the auth function, and used later
  return Promise.all([
    MenteeListing.getOne({ ID : mentorRelationship.menteeListingID }, user),
    MentorListing.getOne({ ID : mentorRelationship.mentorListingID }, user),
    this.search({ menteeListingID : mentorRelationship.menteeListingID, mentorListingID : mentorRelationship.mentorListingID, operator : "OR" }, user)
  ]).then(([ menteelisting, mentorlisting, currentRelationships ]) => {
    if(!menteelisting){
      throw new APIInputError("Mentee listing does not exist", "menteeListingID", "mentorRelationship.create");
    }
    if(!mentorlisting){
      throw new APIInputError("Mentor listing does not exist", "mentorListingID", "mentorRelationship.create");
    }

    return user.canCreateMentorMenteeRelationship(menteelisting, mentorlisting)
      .tap(allowed => {
        if(!allowed){
          throw new AuthorisationError("You do not have permission to create a that relationship", "mentorRelationship.create");
        }
      })
      .then(() => {
        let requiredFields = [
          "mentorListingID",
          "menteeListingID"
        ];

        let queryFields = [
          "mentorListingID",
          "menteeListingID"
        ];

        let remainingFields = queryFields.filter(field => (mentorRelationship[field] !== undefined));
        requiredFields.forEach(field => {
          if(!remainingFields.includes(field)){
            throw new APIInputError("Field " + field + " is required", field, "mentorRelationship.create");
          }
        });

        // Check if making this relationship will 
        // cause a listing to go over its maximum
        let menteeRelationships = 0;
        let mentorRelationships = 0;
        currentRelationships.forEach(r => {
          if(r.menteeListingID === mentorRelationship.menteeListingID &&
            r.mentorListingID === mentorRelationship.mentorListingID){
            // OH NO, this relationship already exists
            throw new APIInputError("Relationship already exists", "mentorListingID", "mentorRelationship.create");
          }

          if(r.menteeListingID === mentorRelationship.menteeListingID){
            menteeRelationships += 1;
          }
          if(r.mentorListingID === mentorRelationship.mentorListingID){
            mentorRelationships += 1;
          }
        });

        let menteeMaxRelationships = menteelisting.maxRelationships;
        let mentorMaxRelationships = mentorlisting.maxRelationships;

        if(menteeRelationships >= menteeMaxRelationships){
          throw new APIInputError("Exceeding the number of mentee relationship limit", "menteeListingID", "mentorRelationship.create");
        }
        if(mentorRelationships >= mentorMaxRelationships){
          throw new APIInputError("Exceeding the number of mentor relationship limit", "mentorListingID", "mentorRelationship.create");
        }

        // Determine who made the request and set wasSentByMentor
        if(menteelisting.staffID === user.getID()){
          mentorRelationship.wasSentByMentor = false;
        }
        if(mentorlisting.staffID === user.getID()){
          mentorRelationship.wasSentByMentor = true;
        }
        remainingFields.push("wasSentByMentor");


        // Cannot relate to yourself..
        if(menteelisting.staffID === mentorlisting.staffID){
          throw new APIInputError("Cannot create a relationship to yourself!", "mentorListingID", "mentorRelationship.create");
        }
        // Cannot create if the skills are not the same!
        if(menteelisting.skillID !== mentorlisting.skillID){
          throw new APIInputError("Cannot create a relationship between different skills", "mentorListingID", "mentorRelationship.create");
        }


        let query = `INSERT INTO ${this.table}
          (${remainingFields.join(",")})
          VALUES (${remainingFields.map(field => `@${field}`).join(",")});
          SELECT SCOPE_IDENTITY() AS new_id;`;

        let params = remainingFields.map(field => [
          field,
          undefined,
          mentorRelationship[field]
        ]);

        return getOne(query, params)
          .then(({ new_id }) => ({
            ID : new_id
          }));
      });
  });
};



// Acceptable status changes
const stateChanges = [
  [],       // 0 - None
  [ 2 ],    // 1 - Requested
  [ 3, 4 ], // 2 - Seen
  [ 4, 5 ], // 3 - Accepted
  [ 3, 5 ], // 4 - Rejected
  [ 5 ],    // 5 - Finished
];

mentorRelationship.changeRelationshipStatus = function changeRelationshipStatus({ mentorRelationshipID, statusID }, user) {
  return require("./MentorRelationship").getOne({ ID: mentorRelationshipID }).then(mentorRelationship => {
    return user.canEditMentorMenteeRelationship(mentorRelationship)
      .tap(allowed => {
        if(!allowed){
          throw new AuthorisationError("You do not have permission to edit that relationship", "mentorRelationship.changeRelationshipStatus");
        }
      }).then(() => {
        // Check if the state transition is allowed or not
        let currentState = mentorRelationship.status;

        if (!stateChanges[currentState] || !stateChanges[currentState].includes(statusID)){
          throw new APIInputError("Cannot transition to that status", "statusID", "mentorRelationship.changeRelationshipStatus");
        }

        // Check where we are transitioning to
        // if we are going to 2,3,4 (seen,acc,rej) that can only be
        // done by the user that didn't create the relationship
        if(statusID === 2 || statusID === 3 || statusID === 4){
          if(mentorRelationship.wasSentByMentor){
            return user.canEditMenteeListing(mentorRelationship.menteeListingID);
          }else{
            return user.canEditMentorListing(mentorRelationship.mentorListingID);
          }
        }

        // All other states can be transitioned to regardless
        return true;
      }).tap(allowed => {
        if(!allowed){
          throw new AuthorisationError("You do not have permission transition to that status", "mentorRelationship.changeRelationshipStatus");
        }
      })
      .then(() => {
        let query = `UPDATE ${this.table} SET
          [status] = @mentorRelationshipStatus
          WHERE [ID] = @mentorRelationshipID;`;

        let params = [
          [ "mentorRelationshipID", "Int", mentorRelationshipID ],
          [ "mentorRelationshipStatus", "Int", statusID ]
        ];

        return queryDB(query, params)
          .return({ ID : mentorRelationshipID });
      });
  });
};



mentorRelationship.del = function del({ mentorRelationshipID }, user) {
  if (!mentorRelationshipID) {
    return Promise.reject(new APIInputError("Mentor Listing ID is not provided", "mentorRelationshipID", "mentorRelationship.delete"));
  }
  return user.canEditMentorMenteeRelationship(mentorRelationshipID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to delete a that relationship", "mentorRelationship.delete");
      }
    })
    .then(() => {
      let query = `DELETE FROM ${this.table}
            WHERE [ID] = @mentorRelationshipID`;

      let params = [ "mentorRelationshipID", "Int", mentorRelationshipID ];

      return queryDB(query, params);
    });
};

module.exports = mentorRelationship;
