//// Contains the generic functions for editing skill entries
// The skill entry files take this and use it to generate their
// functions, specific to that object
const Promise = require("bluebird");
const { APIInputError } = require("./Errors.js");

const { getTransaction, queryDB } = require("../database");



function makeFunctions(options){
  const { IDName, apiName } = options;

  let skillAPIFunctions = {};


  skillAPIFunctions.create = function create(inputObject, user, higherTransaction){
    return Promise.try(() => {
      let ID = inputObject[IDName];
      let skillEntry = inputObject.skillEntry;

      if(!ID){
        throw new APIInputError(`No ${IDName} provided`, IDName, `${apiName}.create`);
      }

      skillEntry[IDName] = ID;

      let required = [
        IDName,
        "skillID",
        "skillLevel"
      ];

      let queryFields = [
        IDName,
        "skillID",
        "skillLevel"
      ];

      let remainingFields = queryFields.filter(field => (skillEntry[field] !== undefined && skillEntry[field] !== ""));
      required.forEach(field => {
        if(!remainingFields.includes(field)){
          throw new APIInputError(`Field ${field} is required`, field, `${apiName}.create`);
        }
      });

      // Do not create skill if level < 1
      // if(skillEntry.skillLevel < 1){
      //   return { new_id : null };
      // }


      let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
      return transactionPromise.then(transaction => {
        // Is this an update or an insert?
        return this.search({ [IDName] : ID, skillID : skillEntry.skillID, returnZeros : true }, user, transaction).then(oldEntries => {
          // Wrap promise, so returns get sent to the transaction handler
          return Promise.try(() => {
            let lastUpdateTimestamp = inputObject.lastUpdateTimestamp;
            // Construct query based on entries
            let query;
            if(oldEntries.length > 0){
              // Edit if skill level is > 0, UPDATE
              if(skillEntry.skillLevel > 0){
                query = `UPDATE ${this.table} SET
                  [skillLevel] = @skillLevel
                  ${lastUpdateTimestamp ? ", [lastUpdateTimestamp] = @lastUpdateTimestamp" : ""}
                  WHERE [${IDName}] = @${IDName} AND [skillID] = @skillID`;
              }else{
                // If 0, then DELETE the entry
                return skillAPIFunctions.del.call(this, { [IDName] : ID, skillID : skillEntry.skillID }, user, transaction)
                  .return("DELETED");
              }
            }else{
              // Edit if skill level is > 0, CREATE
              if(skillEntry.skillLevel > 0){
                query = `INSERT INTO ${this.table}
                  ([${IDName}], [skillID], [skillLevel] ${lastUpdateTimestamp ? ", [lastUpdateTimestamp]" : ""})
                  VALUES (@${IDName}, @skillID, @skillLevel ${lastUpdateTimestamp ? ", @lastUpdateTimestamp" : ""});
                  SELECT SCOPE_IDENTITY() AS new_id;`;
              }else{
                // If trying to set 0, then IGNORE
                return "DELETED";
              }
            }

            let params = remainingFields.map(field => [
              field,
              undefined,
              skillEntry[field]
            ]);

            if(lastUpdateTimestamp){
              params.push([ "lastUpdateTimestamp", undefined, inputObject.lastUpdateTimestamp ]);
            }

            return queryDB(query, params, transaction);
          })
            .then(result => {
              let new_id = (result === "DELETED") ? null : ((oldEntries.length > 0) ? oldEntries[0].ID : result[0].new_id);

              // Don't commit the transaction if it doesn't belong to us
              if(higherTransaction){
                return new_id;
              }else{
                return transaction.commit().return({ ID : new_id });
              }
            })
            .catch((e) => {
              if(higherTransaction){
                throw e;
              }else{
                return transaction.rollback().throw(e);
              }
            });
        });
      });
    });
  };



  skillAPIFunctions.createMany = function createMany(inputObject, user, higherTransaction){
    let ID = inputObject[IDName];
    let skillEntries = inputObject.skillEntries;

    if(!Array.isArray(skillEntries) || skillEntries.length === 0){
      return Promise.resolve([]);
    }

    // Get a transaction
    let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
    return transactionPromise.then(transaction => {

      return Promise.map(skillEntries || [], (skill) => {
        // Called this way as it guarantees the function exists
        // The name could be changed when the function is loaded into
        // it's API object
        return skillAPIFunctions.create.call(this, {
          [IDName] : ID,
          skillEntry : skill
        }, user, transaction);
      }, { concurrency : 1 })

      .filter(a => a)

      .then((ids) => {
        // Don't commit the transaction if it doesn't belong to us
        if(higherTransaction){
          return ids;
        }else{
          return transaction.commit().return(ids);
        }
      })

      .catch((e) => {
        if(higherTransaction){
          throw e;
        }else{
          return transaction.rollback().throw(e);
        }
      });
    });
  };



  skillAPIFunctions.del = function del(inputObject, user, higherTransaction){
    return Promise.try(() => {
      let ID = inputObject[IDName];
      let { skillID, skillEntryID } = inputObject;

      if(!ID){
        throw new APIInputError(`No ${IDName} provided`, IDName, `${apiName}.delete`);
      }
      if(!skillID && !skillEntryID){
        throw new APIInputError("Either skillID or skillEntryID is required", "skillID,skillEntryID", `${apiName}.delete`);
      }
      // Get a transaction
      let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
      return transactionPromise.then(transaction => {
        let colName = skillEntryID ? "skillEntryID" : "skillID";
        let query = `DELETE FROM ${this.table}
          WHERE [${IDName}] = @ID AND [${colName}] = @${colName}`;

        let params = [
          [ "ID", "Int", ID ],
          [ colName, "Int", skillEntryID || skillID ]
        ];

        return queryDB(query, params, transaction)
          .then(() => {
            // Don't commit the transaction if it doesn't belong to us
            if(!higherTransaction){
              return transaction.commit().return();
            }
          })

          .catch((e) => {
            if(higherTransaction){
              throw e;
            }else{
              return transaction.rollback().throw(e);
            }
          });
      });
    });
  };


  skillAPIFunctions.delMany = function delMany(inputObject, user, higherTransaction){
    let ID = inputObject[IDName];
    let skillIDs = inputObject.skillIDs;

    // Get a transaction
    let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
    return transactionPromise.then(transaction => {

      return Promise.reduce(skillIDs || [], (_, skillID) => {
        return skillAPIFunctions.del.call(this, {
          [IDName] : ID,
          skillID : skillID
        }, user, transaction);
      }, null)

      .then(() => {
        // Don't commit the transaction if it doesn't belong to us
        if(!higherTransaction){
          return transaction.commit().return();
        }
      })

      .catch((e) => {
        if(higherTransaction){
          throw e;
        }else{
          return transaction.rollback().throw(e);
        }
      });
    });
  };


  skillAPIFunctions.overwriteSkills = function overwriteSkills(inputObject, user, higherTransaction){
    return Promise.try(() => {
      let ID = inputObject[IDName];
      let skillEntries = inputObject.skillEntries;

      if(!ID){
        throw new APIInputError(`No ${IDName} provided`, IDName, `${apiName}.overwrite`);
      }

      skillEntries = skillEntries.filter((skill) => {
        return skill.skillLevel > 0;
      });

      // Get a transaction
      let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
      return transactionPromise.then(transaction => {
        // Delete the ones that do not exist in array
        let query = `DELETE FROM ${this.table}
          WHERE [${IDName}] = @ID`;

        if(skillEntries.length > 0){
          query += ` AND [skillID] NOT IN (${skillEntries.map((skill, i) => `@skillID_${i}`).join(",")})`;
        }

        let params = skillEntries.map((skill, i) => [
          `skillID_${i}`,
          "Int",
          skill.skillID
        ]);

        params.push([
          "ID", 
          "Int",
          ID
        ]);

        return queryDB(query, params, transaction)
          .then(() => {
            // Create the rest..
            return skillAPIFunctions.createMany.call(this, { [IDName] : ID, skillEntries }, user, transaction);
          })

          .then((ids) => {
            // Don't commit the transaction if it doesn't belong to us
            if(higherTransaction){
              return ids;
            }else{
              return transaction.commit().return(ids);
            }
          })

          .catch((e) => {
            if(higherTransaction){
              throw e;
            }else{
              return transaction.rollback().throw(e);
            }
          });
      });
    });
  };


  return skillAPIFunctions;
}

module.exports = makeFunctions;
