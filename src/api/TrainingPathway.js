// TrainingPathway API functions

const APIGenerator = require("./APIGenerator");
const generateSimpleAPIFunctions = require("./simpleAPIGenerator/generateSimpleAPIFunctions");

const TrainingPathwayAPI = new APIGenerator({
  tableName : "TrainingPathways",
  schemaName : "skills",
  columns : [
    [ "name", "Text" ],
    [ "capabilityID", "FK" ],
    [ "professionID", "FK" ],
    [ "curatorID", "FK" ],
    [ "lastReviewedDate", "Date" ]
  ]
});

generateSimpleAPIFunctions(TrainingPathwayAPI);


module.exports = TrainingPathwayAPI;
