// Project API functions
const Promise = require("bluebird");
const { APIError, AuthorisationError, APIInputError } = require("./Errors.js");

const APIGenerator = require("./APIGenerator");
const makeAPIFunction = APIGenerator.makeAPIFunction;
const { getTransaction, queryDB, execFunction, getOne } = require("../database");

const { FundingType, ProjectStage } = require("./simple");
const Staff = require("./Staff");
const Roles = require("./Role");
const CTOTheme = require("./CTOTheme");

const projectsAPI = new APIGenerator({
  tableName : "Projects",
  schemaName : "projects",
  columns : [
    [ "name", "Text" ],
    [ "projectManagerID", "FK" ],
    [ "techLeadID", "FK" ],
    [ "description", "Text" ],
    [ "startDate", "Date" ],
    [ "endDate", "Date" ],
    [ "fundingTypeID", "FK" ],
    [ "winProbability", "Int" ],
    [ "projectStageID", "FK" ],
    [ "approved", "Bit" ],
    [ "submittedBy", "FK" ],
    [ "sensitivity", "Int" ],
    [ "capabilityAreaID", "FK" ],
    [ "CTOThemeID", "FK" ]
  ]
});

// Add "active" to search functionaility
// Defined by when the endDate is before now
projectsAPI.addSearchOption({
  optionName : "inactive",
  options : {
    autoFilter : false
  },
  type : "Bit",
  queryFragment : function(inactive){
    return inactive ? `${this.table}.[endDate] < GETDATE()` : `${this.table}.[endDate] >= GETDATE()`;
  }
});

projectsAPI.addSearchOption({
  optionName : "canEdit",
  type : "Bit",
  // Possibly replace this with a filter
  queryFragment : function(_, user){
    if(user.isResourcingManagerRaw()){
      return null;
    }

    return `(${this.table}.[projectManagerID] = @loggedInUser OR ${this.table}.[submittedBy] = @loggedInUser)`;
  }
});

projectsAPI.addSearchOption({
  optionName : "isNonBillable",
  type : "Bit",
  queryFragment : function(is){
    return (is ? "EXISTS" : "NOT EXISTS") + ` (
      SELECT 1 AS A FROM [core].[FundingTypes]
      WHERE ${this.table}.[fundingTypeID] = [core].[FundingTypes].[ID] AND
      ([core].[FundingTypes].[name] = 'Investment' OR [core].[FundingTypes].[name] = 'Overhead')
    )`;
  }
});

projectsAPI.addFilter([ "search", "getAll" ], function(results, args, user){
  return Promise.filter(results, project => user.canSeeProject(project));
});
projectsAPI.addFilter("getOne", function(project, args, user){
  return user.canSeeProject(project).then(allowed => allowed ? project : undefined);
});


projectsAPI.getStakeholdersForProject = makeAPIFunction(function getStakeholdersForProject({ projectID }, user){
  let query = Staff.getSimpleSelect(user) + `
    INNER JOIN [projects].[ProjectStakeholders] PS ON PS.staffID = ${Staff.table}.[ID]
    WHERE PS.[projectID] = @projectID`;

  return queryDB(query, [ "projectID", "Int", projectID ]);
});

projectsAPI.getStaffAssignedToProject = makeAPIFunction(function getStaffAssignedToProject({ projectID }, user){
  return execFunction("[projects].[GetAllAssigneesForProject]", [ "projectID", "Int", projectID ]).map(Staff.addFullName);
});


/* eslint-disable */
projectsAPI.getActiveAssignments = makeAPIFunction(function getActiveAssignments({ projectID }, user){
  // TODO
})
/* eslint-enable */



// Mutations
projectsAPI.create = makeAPIFunction(function create({ project, roles }, user){
  return user.canCreateProjects()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "project.create");
      }
    })
    .then(() => {
      let required = [
        "name",
        "submittedBy"
      ];

      // Set submittedBy as the current user
      project.submittedBy = user.getID();
      // Trim whitespace from project name
      project.name = project.name && project.name.trim();

      let queryFields = [
        "name",
        "projectManagerID",
        "techLeadID",
        "description",
        "startDate",
        "endDate",
        "fundingTypeID",
        "winProbability",
        "projectStageID",
        "approved",
        "submittedBy",
        "sensitivity",
        "CTOThemeID",
        "capabilityAreaID"
      ];

      let remainingFields = queryFields.filter(field => (project[field] !== undefined && project[field] !== ""));
      required.forEach(field => {
        if(!remainingFields.includes(field)){
          throw new APIInputError("Field " + field + " is required", field, "project.create");
        }
      });

      /**** Other checks go here.. ****/

      // Start transaction
      return getTransaction()
        .then(transaction => {
          // Construct query
          let query = `INSERT INTO ${this.table}
              (${remainingFields.join(",")})
              VALUES (${remainingFields.map(field => `@${field}`).join(",")});
              SELECT SCOPE_IDENTITY() AS new_id;`;

          let params = remainingFields.map(field => [
            field,
            undefined,
            project[field]
          ]);

          return getOne(query, params, transaction)
            .then(({ new_id }) => {
              // Make the roles
              return Roles.createMany({ projectID : new_id, roles }, user, transaction)
                // Commit
                .then(() => {
                  return transaction.commit();
                })
                // Get the new project ID
                .return({ ID : new_id });
            })
            .catch((e) => {
              // Transaction rollback
              return transaction.rollback().throw(e);
            });
        });
    });
});


// Other functions that create a project with a subset of features available to the primary function
projectsAPI.createDiscoveryProject = makeAPIFunction(function createDiscoveryProject({ project }, user){
  return user.canCreateDiscoveryProject()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "project.createDiscoveryProject");
      }
    })
    .then(() => Promise.all([
      CTOTheme.getOne({ ID : project.CTOThemeID }),
      FundingType.getByName("Discovery")
    ]))
    .then(([ ctoTheme, fundingType ]) => {
      let required = [
        "name",
        "CTOThemeID",
        "projectManagerID",
        "submittedBy"
      ];

      // Set submittedBy as the current user
      project.submittedBy = user.getID();
      // Set the funding type to discovery
      project.fundingTypeID = fundingType.ID;
      // Set the project manager here
      // It is taken from the CTO theme
      project.projectManagerID = ctoTheme && ctoTheme.themeOwner;

      // Trim whitespace from project name
      project.name = project.name && project.name.trim();


      let queryFields = [
        "name",
        "description",
        "projectManagerID",
        "submittedBy",
        "sensitivity",
        "projectStageID",
        "fundingTypeID",
        "CTOThemeID"
      ];

      let remainingFields = queryFields.filter(field => (project[field] !== undefined && project[field] !== ""));
      required.forEach(field => {
        if(!remainingFields.includes(field)){
          throw new APIInputError("Field " + field + " is required", field, "project.createDiscoveryProject");
        }
      });

      // Other checks go here..


      // Construct query
      let query = `INSERT INTO ${this.table}
          (${remainingFields.join(",")})
          VALUES (${remainingFields.map(field => `@${field}`).join(",")});
          SELECT SCOPE_IDENTITY() AS new_id;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        project[field]
      ]);

      return getOne(query, params)
        .then(({ new_id }) => ({
          ID : new_id
        }));
    });
});

// Other functions that create a project with a subset of features available to the primary function
projectsAPI.createPotentialProject = makeAPIFunction(function createPotentialProject({ project }, user){
  return user.canCreatePotentialProject()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "project.createPotentialProject");
      }
    })
    .then(() => ProjectStage.getByName("Potential"))
    .then(projectStage => {
      let required = [
        "name",
        "submittedBy"
      ];

      // Set submittedBy as the current user
      project.submittedBy = user.getID();
      // Set project stage to 1 (potential)
      project.projectStageID = projectStage.ID;

      let queryFields = [
        "name",
        "description",
        "startDate",
        "winProbability",
        "submittedBy",
        "sensitivity",
        "projectStageID",
        "capabilityAreaID"
      ];

      let remainingFields = queryFields.filter(field => (project[field] !== undefined && project[field] !== ""));
      required.forEach(field => {
        if(!remainingFields.includes(field)){
          throw new APIInputError("Field " + field + " is required", field, "project.createPotentialProject");
        }
      });

      // Other checks go here..


      // Construct query
      let query = `INSERT INTO ${this.table}
          (${remainingFields.join(",")})
          VALUES (${remainingFields.map(field => `@${field}`).join(",")});
          SELECT SCOPE_IDENTITY() AS new_id;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        project[field]
      ]);

      return getOne(query, params)
        .then(({ new_id }) => ({
          ID : new_id
        }));
    });
});


projectsAPI.edit = makeAPIFunction(function edit({ projectID, project = {}, addRoles, editRoles, deleteRoles }, user){
  return user.canModifyProject(projectID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to edit that project", "project.edit");
      }
    })
    .then(() => {
      let queryFields = [
        "name",
        "projectManagerID",
        "techLeadID",
        "description",
        "startDate",
        "endDate",
        "fundingTypeID",
        "winProbability",
        "projectStageID",
        "approved",
        "sensitivity"
      ];

      let remainingFields = queryFields.filter(field => (project[field] !== undefined && project[field] !== ""));

      // Other checks go here..

      // Start transaction
      return getTransaction()
        .then(transaction => {
          let prom;
          if(remainingFields.length > 0){
            // Construct query
            let query = `UPDATE ${this.table} SET
                ${remainingFields.map(field => {
                  return `${field} = @${field}`;
                }).join(",")}
                WHERE [ID] = @projectID;`;


            let params = remainingFields.map(field => [
              field,
              undefined,
              project[field]
            ]);

            // Push on the project ID
            params.push([
              "projectID",
              undefined,
              projectID
            ]);

            prom = queryDB(query, params, transaction);
          }else{
            prom = Promise.resolve();
          }

          return prom
            .then(() => {
              // Make the new roles
              return Array.isArray(addRoles) && (addRoles.length > 0) && Roles.createMany({ projectID, roles : addRoles }, user, transaction);
            })

            .then(() => {
              // Edit the old ones
              return Array.isArray(editRoles) && (editRoles.length > 0) && Roles.editMany({ projectID, roles : editRoles }, user, transaction);
            })

            .then(() => {
              // Delete the old ones
              return Array.isArray(deleteRoles) && (deleteRoles.length > 0) && Roles.delMany({ projectID, roleIDs : deleteRoles }, user, transaction);
            })

            .then(() => {
              // Commit
              return transaction.commit();
            })
            // Get the new project ID
            .return({ ID : projectID })

            .catch((e) => {
              // Transaction rollback
              return transaction.rollback().throw(e);
            });
        });
    });
});

projectsAPI.del = makeAPIFunction(function del({ projectID, force }, user){
  if(!projectID){
    return Promise.reject(new APIInputError("No project ID provided", "projectID", "project.delete"));
  }
  return user.canModifyProject(projectID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to delete that project", "project.delete");
      }
    })
    .then(() => {
      return Roles.search({ projectID }, user);
    })
    .then(roles => {
      // Force flag will allow the project to be deleted even if it has roles
      // However, roles will not be deleted if they are assigned
      // As inforced in Role#delete()
      // There is no way to force an assigned role to be deleted
      if(roles.length > 0 && !force){
        throw new APIError("You cannot delete a project that has roles", "project.delete", 409);
      }

      // Start transaction
      return getTransaction()
        .then(transaction => {
          if(roles.length > 0 && force){
            return Roles.delMany({
              projectID,
              roleIDs : roles.map(({ ID }) => ID)
            }, user, transaction).return(transaction);
          }else{
            return transaction;
          }
        })
        .then(transaction => {
          let query = `DELETE FROM ${this.table}
            WHERE [ID] = @projectID`;

          let params = [
            [ "projectID", "Int", projectID ]
          ];

          return queryDB(query, params, transaction)
            .then(() => {
              return transaction.commit().return(true);
            })
            .catch((e) => {
              // Transaction rollback
              return transaction.rollback().throw(e);
            });
        });
    });
});



module.exports = projectsAPI;
