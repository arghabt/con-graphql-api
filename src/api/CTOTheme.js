// CTOTheme API functions

const APIGenerator = require("./APIGenerator");
const generateSimpleAPIFunctions = require("./simpleAPIGenerator/generateSimpleAPIFunctions");

const CTOTheme = new APIGenerator({
  tableName : "CTOThemes",
  schemaName : "core",
  columns : [
    [ "name", "Text" ],
    [ "themeOwner", "FK" ],
    [ "capabilityAreaID", "FK" ]
  ]
});

generateSimpleAPIFunctions(CTOTheme);




module.exports = CTOTheme;
