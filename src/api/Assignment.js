// Assignment API functions
const Promise = require("bluebird");
const moment = require("moment");

const APIGenerator = require("./APIGenerator");
const makeAPIFunction = APIGenerator.makeAPIFunction;
const { queryDB, getOne, getOneFromFunction, execProcedure, sql : { RequestError }  } = require("../database");
const { APIInputError, AuthorisationError } = require("./Errors.js");

const Staff = require("./Staff");
const AssignmentEstimate = require("./AssignmentEstimate");
const AssignmentRequest = require("./AssignmentRequest");


const assignmentsAPI = new APIGenerator({
  tableName : "Assignments",
  schemaName : "projects",
  columns : [
    [ "roleID", "FK" ],
    [ "staffID", "FK" ],
    [ "approved", "Bit" ],
    [ "submittedBy", "FK" ],
    [ "timestamp", "DateTime" ]
  ]
});


assignmentsAPI.getRoleForAssignment = makeAPIFunction(function getRoleForAssignment({ assignmentID }, user){
  const Role = require("./Role");

  let query = Role.getSimpleSelect(user) + `
    INNER JOIN ${this.table} ON ${this.table}.[roleID] = ${Role.table}.[ID]
    WHERE ${this.table}.[ID] = @assignmentID`;

  return getOne(query, [ "assignmentID", "Int", assignmentID ]);
});

assignmentsAPI.getProjectForAssignment = makeAPIFunction(function getProjectForAssignment({ assignmentID }, user){
  const Project = require("./Project");
  const Role = require("./Role");

  let query = Project.getSimpleSelect(user) + `
    INNER JOIN ${Role.table} ON ${Role.table}.[projectID] = ${Project.table}.[ID]
    INNER JOIN ${this.table} ON ${this.table}.[roleID] = ${Role.table}.[ID]
    WHERE ${this.table}.[ID] = @assignmentID`;

  return getOne(query, [ "assignmentID", "Int", assignmentID ]);
});


assignmentsAPI.getStaffForAssignment = makeAPIFunction(function getStaffForAssignment({ assignmentID }, user){
  let query = Staff.getSimpleSelect(user) + `
    INNER JOIN ${this.table} ON ${this.table}.[staffID] = ${Staff.table}.[ID]
    WHERE ${this.table}.[ID] = @assignmentID`;

  return getOne(query, [ "assignmentID", "Int", assignmentID ]);
});


assignmentsAPI.getAssignmentEstimates = makeAPIFunction(function getAssignmentEstimates({ assignmentID }, user){
  let query = AssignmentEstimate.getSimpleSelect(user) + `
    WHERE ${AssignmentEstimate.table}.[assignmentID] = @assignmentID
    ORDER BY ${AssignmentEstimate.table}.[timestamp] DESC`;

  return queryDB(query, [ "assignmentID", "Int", assignmentID ]);
});

assignmentsAPI.getLatestAssignmentEstimate = makeAPIFunction(function getLatestAssignmentEstimate({ assignmentID }, user){
  let query = `SELECT ${AssignmentEstimate.getSQLSelectFieldsString(user)}
    FROM [projects].[LatestAssignmentEstimates]
    WHERE [assignmentID] = @assignmentID`;

  return getOne(query, [ "assignmentID", "Int", assignmentID ]);
});


assignmentsAPI.getLatestAssigneeEstimate = makeAPIFunction(function getLatestAssigneeEstimate({ assignmentID }, user){
  return getOneFromFunction("[projects].[GetLatestAssigneeEstimate]", [ "assignmentID", "Int", assignmentID ]);
});

assignmentsAPI.getLatestProjectManagerEstimate = makeAPIFunction(function getLatestProjectManagerEstimate({ assignmentID }, user){
  return getOneFromFunction("[projects].[GetLatestProjectManagerEstimate]", [ "assignmentID", "Int", assignmentID ]);
});

assignmentsAPI.getLatestApprovedEstimate = makeAPIFunction(function getLatestApprovedEstimate({ assignmentID }, user){
  let query = `SELECT ${AssignmentEstimate.getSQLSelectFieldsString(user)}
    FROM [projects].[LatestApprovedAssignmentEstimates]
    WHERE [assignmentID] = @assignmentID`;

  return getOne(query, [ "assignmentID", "Int", assignmentID ]);
});





assignmentsAPI.create = makeAPIFunction(function create({ roleID, staffID }, user){
  // Create a new assignment link between a role and a staff member
  // This can only be done by a resourcing manager
  return user.isResourcingManager()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "assignment.create");
      }

      if(!roleID){
        throw new APIInputError("roleID must be supplied", "roleID", "assignment.create");
      }
      if(!staffID){
        throw new APIInputError("staffID must be supplied", "staffID", "assignment.create");
      }
    })
    .then(() => {
      let currentUserID = user.ID;
      // Set approved to false by default
      let approved = false;

      let query = `INSERT INTO ${this.table}
          (roleID, staffID, submittedBy, approved)
          VALUES (@roleID, @staffID, @submittedBy, @approved);
          SELECT SCOPE_IDENTITY() AS new_id;`;

      let params = [
        [ "roleID", "Int", roleID ],
        [ "staffID", "Int", staffID ],
        [ "submittedBy", "Int", currentUserID ],
        [ "approved", "Bit", approved ]
      ];

      return getOne(query, params)
        .then(({ new_id }) => ({
          ID : new_id
        }));
    });
});

assignmentsAPI.del = makeAPIFunction(function del({ assignmentID }, user){
  // Deleting assignments can only be done in two specific circumstances:
  // When the win probability is < 100 for the role or the project
  // OR when the role has not yet started
  return user.isResourcingManager()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "assignment.del");
      }
    })
    .then(() => {
      return Promise.join(
        this.getRoleForAssignment({ assignmentID }, user),
        this.getProjectForAssignment({ assignmentID }, user),
        (role, project) => {
          // Check that the conditions are met
          // Is the win probability at 100?
          if(project.winProbability >= 100 && role.winProbability >= 100){
            throw new APIInputError("The win probability of the project and role is 100", "assignmentID", "assignment.del");
          }
          // That the role has not already started
          if(moment().isSameOrAfter(role.startDate)){
            throw new APIInputError("This role has already started", "assignmentID", "assignment.del");
          }
        }
      );
    })
    .then(() => {
      // We have hit this block, which means we are okay to delete
      let query = `DELETE FROM ${this.table} WHERE [ID] = @assignmentID`;

      return queryDB(query, [
        [ "assignmentID", "Int", assignmentID ]
      ]);
    })
    .return(true);
});


assignmentsAPI.createOrRequestAssignment = makeAPIFunction(function createOrRequestAssignment({ roleID, staffID }, user){
  // Actually two functions, if you are a resourcing manager then create an assignment
  // if not, the make an assignment request
  return user.isResourcingManager()
    .then(isResourcingManager => {
      if(isResourcingManager){
        return this.create({ roleID, staffID }, user).then(obj => {
          obj._assignmentRequest = false;
          return obj;
        });
      }else{
        return AssignmentRequest.create({ roleID, staffID }, user).then(obj => {
          obj._assignmentRequest = true;
          return obj;
        });
      }
    });
});



assignmentsAPI.changeApproval = makeAPIFunction(function changeApproval({ assignmentID, approved }, user){
  // Resourcing managers can approve or reject assignments
  return user.isResourcingManager()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "assignment.changeApproval");
      }
    })
    .then(() => {
      let query = `UPDATE ${this.table} SET
        [approved] = @approved
        WHERE [ID] = @assignmentID`;

      let params = [
        [ "approved", "Bit", approved ],
        [ "assignmentID", "Int", assignmentID ]
      ];

      return queryDB(query, params);
    })
    .return({
      ID : assignmentID
    });
});


// Add a new assignment estimate
assignmentsAPI.addNewAssignmentEstimate = makeAPIFunction(function addNewAssignmentEstimate({ assignmentID, assignmentEstimate }, user){
  return Promise.all([
    this.getOne({ ID : assignmentID }),
    this.getProjectForAssignment({ assignmentID })
  ])
  .then(([ assignment, project ]) => {
    return user.canAddAssignmentEstimate(assignment, project)
      .tap(allowed => {
        if(!allowed){
          throw new AuthorisationError("You do not have permission to do that", "assignment.addNewAssignmentEstimate");
        }
      })
      .then(() => {
        // Set submittedBy as the current user
        assignmentEstimate.submittedBy = user.getID();

        let queryFields = [
          "startDatetime",
          "endDatetime",
          "approved",
          "percentageOfTimeOnAssignment",
          "fedUp"
        ];

        let remainingFields = queryFields.filter(field => (assignmentEstimate[field] !== undefined && assignmentEstimate[field] !== ""));

        // Check if they are the project manager, if they are they can set approved
        if(project.projectManagerID !== user.getID() && remainingFields.includes("approved")){
          throw new APIInputError("Only project managers can set the approved field", "approved", "assignment.addNewAssignmentEstimate");
        }

        // If you are your own project manager, automatically approve
        if(project.projectManagerID === assignment.staffID){
          assignmentEstimate.approved = true;
        }


        let params = remainingFields.map(field => [
          field,
          undefined,
          assignmentEstimate[field]
        ]);

        params.push([ "assignmentID", "Int", assignmentID ]);

        return execProcedure("[projects].[AddNewAssignmentEstimate]", params)
          .then(a => ({
            ID : a.returnValue
          }))
          .catch(RequestError, e => {
            let m = e.message;
            // Check if the error is because startDatetime or endDatetime are not autofilled
            // (ID it's the first estimate)
            if(m.includes("column does not allow nulls") && (m.includes("startDatetime") || m.includes("endDatetime"))){
              throw new APIInputError("The first estimate requires both startDatetime and endDatetime", "startDatetime", "assignment.addNewAssignmentEstimate");
            }else{
              throw e;
            }
          });
      });
  });
});



module.exports = assignmentsAPI;
