// API reconciler

module.exports = Object.assign({

  Staff : require("./Staff"),
  ClearanceApplication : require("./ClearanceApplication"),
  StaffAttachment : require("./StaffAttachment"),
  ECIProjectsReadOnTo : require("./ECIProjectsReadOnTo"),

  Skill : require("./Skill"),
  SkillSubcategory : require("./SkillSubcategory"),
  TrainingPathway : require("./TrainingPathway"),

  SkillEntry : require("./SkillEntry"),
  RoleSkillEntry : require("./RoleSkillEntry"),


  Project : require("./Project"),
  Role : require("./Role"),
  Assignment : require("./Assignment"),
  AssignmentRequest : require("./AssignmentRequest"),
  AssignmentEstimate : require("./AssignmentEstimate"),
  LatestAssignmentEstimate : require("./LatestAssignmentEstimate"),

  CTOTheme : require("./CTOTheme"),

}, require("./simple"),
// opportunitiesMarketplace
require("./opportunitiesMarketplace"),
// Mentoring Tool
require("./mentoringTool")

);
