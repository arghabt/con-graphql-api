const ExError = require("extendable-error-class");

class APIError extends ExError {
  constructor(message, method, statusCode){
    super(message);

    this.method = method;
    this.statusCode = statusCode;
  }
}

class AuthenticationError extends APIError {
  constructor(message, method){
    super(message, method, 401);
  }
}

class AuthorisationError extends APIError {
  constructor(message, method){
    super(message, method, 403);
  }
}

class APIInputError extends APIError {
  constructor(message, field, method){
    super(message, method, 400);

    this.field = field;
  }
}


function convertErrorToGQLJSON(error){
  return {
    errors : [ {
      type : error.constructor.name,
      message : error.message,

      field : error.field,
      method : error.method,
      statusCode : error.statusCode
    } ]
  };
}

module.exports = {
  convertErrorToGQLJSON,

  APIError,
  AuthenticationError,
  AuthorisationError,
  APIInputError
};
