// Skill API functions
const Promise = require("bluebird");

const APIGenerator = require("./APIGenerator");
const { makeAPIFunction, filterFields } = APIGenerator;
const { AuthorisationError, APIInputError } = require("./Errors");

const { queryDB, getOne, getTransaction } = require("../database");

const { SkillTag } = require("./simple");


const skillsAPI = new APIGenerator({
  tableName : "Skills",
  schemaName : "skills",
  columns : [
    [ "skillName", "Text" ],
    [ "subCategoryID", "FK" ],
    [ "inactive", "Bit" ]
  ]
});

// Search for skills with a particular tag
skillsAPI.addSearchOption({
  optionName : "hasTag",
  type : "Int",
  queryFragment : function(){
    return `EXISTS (
      SELECT 1 AS A FROM [skills].[Skills_SkillTags]
        WHERE [skills].[Skills_SkillTags].[skillID] = ${this.table}.[ID]
        AND [skills].[Skills_SkillTags].[skillTagID] = @hasTag
    )`;
  }
});

// Search for skills with particular tags
skillsAPI.addSearchOption({
  optionName : "hasOneOfTags",
  type : "[Int]",
  queryFragment : function(tags){
    return `EXISTS (
      SELECT 1 AS A FROM [skills].[Skills_SkillTags]
        WHERE [skills].[Skills_SkillTags].[skillID] = ${this.table}.[ID]
        AND [skills].[Skills_SkillTags].[skillTagID] IN (${tags.map((_, i) => `@hasOneOfTags_${i}`)})
    )`;
  }
});

// Search for skills with all given tags
skillsAPI.addSearchOption({
  optionName : "hasAllOfTags",
  type : "[Int]",
  queryFragment : function(tags){
    return `(
      SELECT COUNT(1) FROM [skills].[Skills_SkillTags]
        WHERE [skills].[Skills_SkillTags].[skillID] = ${this.table}.[ID]
        AND [skills].[Skills_SkillTags].[skillTagID] IN (${tags.map((_, i) => `@hasAllOfTags_${i}`)})
    ) = ${tags.length}`;
  }
});




// Get the tags for a skill
skillsAPI.getTagsForSkill = makeAPIFunction(function getTagsForSkill({ skillID }, user){
  let query = SkillTag.getSimpleSelect(user) + `
    INNER JOIN [skills].[Skills_SkillTags] SST ON SST.skillTagID = ${SkillTag.table}.[ID]
    WHERE SST.skillID = @skillID`;

  return queryDB(query, [ "skillID", "Int", skillID ]);
});



skillsAPI.create = makeAPIFunction(function create({ skill }, user){
  return user.isAdmin()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", `skills.create`);
      }
    })
    .then(() => {
      let remainingFields = filterFields("skills.create", [
        [ "skillName", true ],
        [ "subCategoryID", true ],
        [ "inactive" ]
      ], skill);

      return getTransaction().then(transaction => {
        let query = `INSERT INTO ${this.table}
            (${remainingFields.join(",")})
            VALUES (${remainingFields.map(field => `@${field}`).join(",")});
            SELECT SCOPE_IDENTITY() AS new_id;`;

        let params = remainingFields.map(field => [
          field,
          undefined,
          skill[field]
        ]);

        return getOne(query, params, transaction)
          .then(({ new_id }) => ({
            ID : new_id
          }))
          // Add tags
          .tap(({ ID }) => Array.isArray(skill.skillTagIDs) && addTagsToSkill(ID, skill.skillTagIDs, transaction))

          .tap(() => transaction.commit())
          .catch(e => transaction.rollback().throw(e));
      });
    });
});


skillsAPI.edit = makeAPIFunction(function edit({ skillID, skill }, user){
  return user.isAdmin()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", `skills.edit`);
      }
    })
    .then(() => {
      let remainingFields = filterFields("skills.edit", [
        [ "skillName" ],
        [ "subCategoryID" ],
        [ "inactive" ]
      ], skill);

      return getTransaction().then(transaction => {
        let query = `UPDATE ${this.table} SET
            ${remainingFields.map(field => {
              return `${field} = @${field}`;
            }).join(",")}
            WHERE [ID] = @skillID;`;

        let params = remainingFields.map(field => [
          field,
          undefined,
          skill[field]
        ]);

        params.push([
          "skillID",
          "Int",
          skillID
        ]);

        return queryDB(query, params, transaction)
          // Add tags
          .then(() => Array.isArray(skill.skillTagIDs) && addTagsToSkill(skillID, skill.skillTagIDs, transaction))

          .then(() => transaction.commit())
          .return({ ID : skillID })
          .catch(e => transaction.rollback().throw(e));
      });
    });
});

skillsAPI.del = makeAPIFunction(function del({ skillID }, user){
  if(!skillID){
    throw new APIInputError("ID of skill to delete is required", "skillID", "skills.del");
  }

  return user.isAdmin()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "skills.del");
      }
    })
    .then(() => {
      let query = `DELETE FROM ${this.table}
        WHERE [ID] = @ID`;

      let params = [
        [ "ID", "Int", skillID ]
      ];

      return queryDB(query, params)
        .return(true);
    });
});







function addTagsToSkill(skillID, skillTagIDs, transaction){
  // Delete all and then remake the list
  let query = `DELETE FROM [skills].[Skills_SkillTags]
    WHERE skillID = @skillID`;

  return queryDB(query, [ "skillID", "Int", skillID ], transaction)
    .then(() => {
      let query = `INSERT INTO [skills].[Skills_SkillTags]
          ([skillTagID], [skillID])
          VALUES (@skillTagID, @skillID)`;

      return Promise.reduce(skillTagIDs || [], (_, skillTagID) => {
        return queryDB(query, [
          [ "skillTagID", "Int", skillTagID ],
          [ "skillID", "Int", skillID ]
        ], transaction);
      }, null);
    });
}

module.exports = skillsAPI;
