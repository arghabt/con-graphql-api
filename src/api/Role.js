// Role API functions
const Promise = require("bluebird");
const { AuthorisationError, APIInputError, APIError } = require("./Errors.js");

const APIGenerator = require("./APIGenerator");
const makeAPIFunction = APIGenerator.makeAPIFunction;
const { getTransaction, queryDB, getOne } = require("../database");

const { CustomerTopUp, CustomerAccount, Location, RoleTag } = require("./simple");
const Assignment = require("./Assignment");
const RoleSkillEntry = require("./RoleSkillEntry");

const rolesAPI = new APIGenerator({
  tableName : "Roles",
  schemaName : "projects",
  columns : [
    [ "projectID", "FK" ],
    [ "description", "Text" ],
    [ "clearanceID", "FK" ],
    [ "startDate", "Date" ],
    [ "endDate", "Date" ],
    [ "gradeID", "FK" ],
    [ "workload", "Int" ],
    [ "briefed", "Bit" ],
    [ "professionID", "FK" ],
    [ "capabilityAreaID", "FK" ],
    [ "winProbability", "Int" ],
    [ "SFIAGradeID", "FK" ],
    [ "otherRequirements", "Text" ],
    [ "advertise", "Bit" ],
    [ "approved", "Bit" ],
    [ "submittedBy", "FK" ]
  ]
});


rolesAPI.addSearchOption({
  optionName : "needsOneOfSkills",
  type : "[Skill]",
  queryFragment : function(skills){
    if(skills.length === 0){
      return null;
    }

    let q = `${this.table}.[ID] IN (SELECT [projects].[Roles_Skills].[roleID] FROM [projects].[Roles_Skills] WHERE
      [projects].[Roles_Skills].[skillLevel] > 0 AND
      (
        ${skills.map((_, i) => `(
          [projects].[Roles_Skills].[skillLevel] >= @needsOneOfSkills_${i}_skillLevel AND
          [projects].[Roles_Skills].[skillID] = @needsOneOfSkills_${i}_skillID
        )`).join(" OR ")}
      )
    )`;

    return q;
  }
});

rolesAPI.addSearchOption({
  optionName : "needsAllOfSkills",
  type : "[Skill]",
  queryFragment : function(skills){
    if(skills.length === 0){
      return null;
    }

    // This is awful and I hope one day I can forgive myself
    let q = `([projects].[Roles].[ID] IN ${skills.map((_, i) => `(
        SELECT [projects].[Roles_Skills].[roleID] FROM [projects].[Roles_Skills] WHERE
          [projects].[Roles_Skills].[skillLevel] > 0 AND
          [projects].[Roles_Skills].[skillID] = @needsAllOfSkills_${i}_skillID AND
          [projects].[Roles_Skills].[skillLevel] >= @needsAllOfSkills_${i}_skillLevel
        GROUP BY [projects].[Roles_Skills].[roleID]
      )`).join(" AND [projects].[Roles].[ID] IN ")})`;


    return q;
  }
});


rolesAPI.addSearchOption({
  optionName : "canBeFufilledWithSkills",
  type : "[Skill]",
  queryFragment : function(skills){
    if(skills.length === 0){
      return null;
    }

    // NEEDS TO GET ALL PROJECTS AN INCLUSIVE SUBSET OF THE GIVEN SKILLS
    // This isn't great but it works
    let q = `(
      ${this.table}.[ID] NOT IN (SELECT [projects].[Roles_Skills].[roleID] FROM projects.Roles_Skills WHERE
        [projects].[Roles_Skills].[skillLevel] > 0 AND
        (
          [projects].[Roles_Skills].[skillID] NOT IN (${skills.map((_, i) => {
            return `@canBeFufilledWithSkills_${i}_skillID`;
          }).join(",")})
        
          OR

          (
            ${skills.map((_, i) => `(
              [projects].[Roles_Skills].[skillID] = @canBeFufilledWithSkills_${i}_skillID AND
              [projects].[Roles_Skills].[skillLevel] > @canBeFufilledWithSkills_${i}_skillLevel
            )`).join(" OR ")}
          )
        )
      ))`;

    /*let q = `(
      ${this.table}.[ID] NOT IN (SELECT [projects].[Roles_Skills].[roleID] FROM projects.Roles_Skills WHERE
        [projects].[Roles_Skills].[skillLevel] > 0 AND
        [projects].[Roles_Skills].[skillID] NOT IN (${skills.map((_, i) => {
          return `@canBeFufilledWithSkills_${i}`;
        }).join(",")}))
      AND
      ${this.table}.[ID] IN (SELECT [projects].[Roles_Skills].[roleID] FROM [projects].[Roles_Skills] WHERE 
        [projects].[Roles_Skills].[skillLevel] > 0 AND
        [projects].[Roles_Skills].[skillID] IN (${skills.map((_, i) => {
          return `@canBeFufilledWithSkills_${i}`;
        }).join(",")}))
    )`;*/


    return q;
  }
});

rolesAPI.addSearchOption({
  optionName : "isInOneOfLocations",
  type : "[FK]",
  queryFragment : function(locations){
    if(locations.length === 0){
      return null;
    }

    let q = `${this.table}.[ID] IN (SELECT [projects].[Roles_Locations].[roleID] FROM [projects].[Roles_Locations] WHERE
      [projects].[Roles_Locations].[locationID] IN (${locations.map((_, i) => {
        return `@isInOneOfLocations_${i}`;
      }).join(",")}))`;

    return q;
  }
});

rolesAPI.addSearchOption({
  optionName : "isAssigned",
  type : "Bit",
  queryFragment : function(is){
    if(is){
      return `EXISTS (SELECT 1 AS E FROM [projects].[Assignments] WHERE
        [projects].[Assignments].[roleID] = ${this.table}.[ID])`;
    }else{
      return `NOT EXISTS (SELECT 1 AS E FROM [projects].[Assignments] WHERE
        [projects].[Assignments].[roleID] = ${this.table}.[ID])`;
    }
  }
});

rolesAPI.addSearchOption({
  optionName : "isAssignedTo",
  type : "FK",
  queryFragment : function(){
    return `EXISTS (SELECT 1 AS E FROM [projects].[Assignments] WHERE
      [projects].[Assignments].[roleID] = ${this.table}.[ID] AND
      [projects].[Assignments].[staffID] = @isAssignedTo)`;
  }
});

rolesAPI.addSearchOption({
  optionName : "hasOneOfTags",
  type : "[FK]",
  queryFragment : function(tags){
    if(tags.length === 0){
      return null;
    }

    let q = `${this.table}.[ID] IN (SELECT [projects].[Roles_RoleTags].[roleID] FROM [projects].[Roles_RoleTags] WHERE
      [projects].[Roles_RoleTags].[roleTagID] IN (${tags.map((_, i) => {
        return `@hasOneOfTags_${i}`;
      }).join(",")}))`;

    return q;
  }
});

rolesAPI.addSearchOption({
  optionName : "isCurrent",
  type : "Bit",
  queryFragment(is){
    if(is){
      return `(
        ${this.table}.[startDate] <= GETUTCDATE() AND
        ${this.table}.[endDate] >= GETUTCDATE()
      )`;
    }else{
      return `(
        ${this.table}.[startDate] > GETUTCDATE() OR
        ${this.table}.[endDate] < GETUTCDATE()
      )`;
    }
  }
});

rolesAPI.addSearchOption({
  optionName : "hasFinished",
  type : "Bit",
  queryFragment(has){
    if(has){
      return `${this.table}.[endDate] < GETUTCDATE()`;
    }else{
      return `${this.table}.[endDate] >= GETUTCDATE()`;
    }
  }
});

rolesAPI.addSearchOption({
  optionName : "hasNotStarted",
  type : "Bit",
  queryFragment(hasNot){
    if(hasNot){
      return `${this.table}.[startDate] > GETUTCDATE()`;
    }else{
      return `${this.table}.[startDate] <= GETUTCDATE()`;
    }
  }
});


rolesAPI.addFilter([ "search", "getAll" ], function(results, args, user){
  return Promise.filter(results, role => user.canSeeRole(role));
});
rolesAPI.addFilter("getOne", function(role, args, user){
  return role && user.canSeeRole(role).then(allowed => allowed ? role : undefined);
});

rolesAPI.getCustomerTopUpsForRole = makeAPIFunction(function getCustomerTopUpsForRole({ roleID }, user){
  let query = CustomerTopUp.getSimpleSelect(user) + `
    INNER JOIN [projects].[Roles_CustomerTopUps] CTU ON CTU.customerTopUpID = ${CustomerTopUp.table}.[ID]
    WHERE CTU.roleID = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ]);
});

rolesAPI.getAccountsForRole = makeAPIFunction(function getAccountsForRole({ roleID }, user){
  let query = CustomerAccount.getSimpleSelect(user) + `
    INNER JOIN [projects].[Roles_Accounts] RA ON RA.accountID = ${CustomerAccount.table}.[ID]
    WHERE RA.roleID = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ]);
});

rolesAPI.getLocationsForRole = makeAPIFunction(function getLocationsForRole({ roleID }, user){
  let query = Location.getSimpleSelect(user) + `
    INNER JOIN [projects].[Roles_Locations] RL ON RL.[locationID] = ${Location.table}.[ID]
    WHERE RL.roleID = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ]);
});

rolesAPI.assignmentExistsForRole = makeAPIFunction(function assignmentExistsForRole({ roleID }, user){
  let query = `SELECT 1 AS A FROM ${Assignment.table}
    WHERE ${Assignment.table}.[roleID] = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ]).then(r => r.length > 0);
});

rolesAPI.getAssignmentForRole = makeAPIFunction(function getAssignmentForRole({ roleID }, user){
  let query = Assignment.getSimpleSelect(user) + `
    WHERE ${Assignment.table}.[roleID] = @roleID`;

  return getOne(query, [ "roleID", "Int", roleID ]);
});

rolesAPI.getProjectForRole = makeAPIFunction(function getProjectForRole({ roleID }, user, transaction){
  const Project = require("./Project");
  let query = Project.getSimpleSelect(user) + `
    INNER JOIN ${this.table} ON ${this.table}.[projectID] = ${Project.table}.[ID]
    WHERE ${this.table}.[ID] = @roleID`;

  return getOne(query, [ "roleID", "Int", roleID ], transaction);
});

rolesAPI.getTagsForRole = makeAPIFunction(function getTagsForRole({ roleID }, user){
  let query = RoleTag.getSimpleSelect(user) + `
    INNER JOIN [projects].[Roles_RoleTags] SST ON SST.roleTagID = ${RoleTag.table}.[ID]
    WHERE SST.roleID = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ]);
});





// Mutations
rolesAPI.create = makeAPIFunction(function create({ projectID, role }, user, higherTransaction){
  if(!projectID){
    return Promise.reject(new AuthorisationError("No project ID provided", "role.create"));
  }
  return user.canModifyProject(projectID).then(allowed => {
    if(!allowed){
      throw new AuthorisationError("You do not have permission to create that role", "role.create");
    }

    let required = [
      "projectID",
      "startDate",
      "endDate",
      "submittedBy"
    ];

    role.submittedBy = user.getID();
    role.projectID = projectID;

    let queryFields = [
      "projectID",
      "description",
      "clearanceID",
      "startDate",
      "endDate",
      "gradeID",
      "workload",
      "briefed",
      "professionID",
      "capabilityAreaID",
      "winProbability",
      "SFIAGradeID",
      "otherRequirements",
      "advertise",
      "approved",
      "submittedBy"
    ];

    let remainingFields = queryFields.filter(field => (role[field] !== undefined && role[field] !== ""));
    required.forEach(field => {
      if(!remainingFields.includes(field)){
        throw new APIInputError("Field " + field + " is required", field, "role.create");
      }
    });

    let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
    return transactionPromise.then(transaction => {
      // Construct query
      let query = `INSERT INTO ${this.table}
          (${remainingFields.join(",")})
          VALUES (${remainingFields.map(field => `@${field}`).join(",")});
          SELECT SCOPE_IDENTITY() AS new_id;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        role[field]
      ]);

      return getOne(query, params, transaction)
        .then(({ new_id }) => {
          // Deal with FK tables
          // Locations
          // Don't run if there are no locations to add
          return Promise.resolve(Array.isArray(role.locationIDs) &&
              (role.locationIDs.length > 0) &&
              addLocationsToRole(new_id, role.locationIDs, transaction))

            // Accounts
            .then(() => {
              return Array.isArray(role.accountIDs) && (role.accountIDs.length > 0) &&  addAccountsToRole(new_id, role.accountIDs, transaction);
            })

            // TopUps
            .then(() => {
              return Array.isArray(role.customerTopUpIDs) && (role.customerTopUpIDs.length > 0) && addTopUpsToRole(new_id, role.customerTopUpIDs, transaction);
            })

            // Tags
            .then(() => {
              return Array.isArray(role.roleTagIDs) && (role.roleTagIDs.length > 0) &&  addRoleTagsToRole(new_id, role.roleTagIDs, transaction);
            })

            // Skills
            .then(() => {
              return Array.isArray(role.skillEntries) && (role.skillEntries.length > 0) && RoleSkillEntry.createMany({ roleID : new_id, skillEntries : role.skillEntries }, user, transaction);
            })


            // Commit transaction/return
            .then(() => {
              // Don't commit the transaction if it doesn't belong to us
              if(higherTransaction){
                return new_id;
              }else{
                return transaction.commit().return({ ID : new_id });
              }
            });
        })
        .catch((e) => {
          if(higherTransaction){
            throw e;
          }else{
            return transaction.rollback().throw(e);
          }
        });
    });
  });
});

rolesAPI.createMany = makeAPIFunction(function createMany({ projectID, roles }, user, higherTransaction){
  // Get a transaction
  let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
  return transactionPromise.then(transaction => {

    return Promise.reduce(roles || [], (_, role) => {
      return this.create({ projectID, role }, user, transaction);
    }, null)

    .then((ids) => {
      // Don't commit the transaction if it doesn't belong to us
      if(higherTransaction){
        return ids;
      }else{
        return transaction.commit().return(ids);
      }
    })

    .catch((e) => {
      if(higherTransaction){
        throw e;
      }else{
        return transaction.rollback().throw(e);
      }
    });
  });
});


rolesAPI.edit = makeAPIFunction(function edit({ projectID, role:roleObj }, user, higherTransaction){
  if(!projectID){
    return Promise.reject(new AuthorisationError("No project ID provided", "role.edit"));
  }
  return user.canModifyProject(projectID).then(allowed => {
    if(!allowed){
      throw new AuthorisationError("You do not have permission to edit that role", "role.edit");
    }

    let queryFields = [
      "description",
      "clearanceID",
      "startDate",
      "endDate",
      "gradeID",
      "workload",
      "briefed",
      "professionID",
      "capabilityAreaID",
      "winProbability",
      "SFIAGradeID",
      "otherRequirements",
      "advertise",
      "approved"
    ];

    let { roleID, role } = roleObj;

    let remainingFields = queryFields.filter(field => (role[field] !== undefined && role[field] !== ""));

    // Additional checks..
/*    if(Array.isArray(role.addSkills)){
      // Filters any skills added to "add" that have a level of 0
      // and adds them to "delete"
      role.addSkills.filter((skill) => {
        if(skill.skillLevel < 1){
          role.deleteSkills.push(skill);
          return false
        }else{
          return true;
        }
      })
    }*/


    // Get a transaction
    let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
    return transactionPromise.then(transaction => {
      let prom;
      if(remainingFields.length > 0){
        // Construct query
        let query = `UPDATE ${this.table} SET
            ${remainingFields.map(field => {
              return `${field} = @${field}`;
            }).join(",")}
            WHERE [ID] = @roleID AND [projectID] = @projectID;`;

        let params = remainingFields.map(field => [
          field,
          undefined,
          role[field]
        ]);

        // Push on the project & role ID
        params.push([
          "projectID",
          undefined,
          projectID
        ], [
          "roleID",
          undefined,
          roleID
        ]);

        prom = queryDB(query, params, transaction);
      }else{
        prom = Promise.resolve();
      }

      return prom
        // Do locations, accounts, tags and skills
        .then(() => {
          return Array.isArray(role.locationIDs) && addLocationsToRole(roleID, role.locationIDs, transaction);
        })

        // Accounts
        .then(() => {
          return Array.isArray(role.accountIDs) && addAccountsToRole(roleID, role.accountIDs, transaction);
        })

        // TopUps
        .then(() => {
          return Array.isArray(role.customerTopUpIDs) && addTopUpsToRole(roleID, role.customerTopUpIDs, transaction);
        })

        // Tags
        .then(() => {
          return Array.isArray(role.roleTagIDs) && addRoleTagsToRole(roleID, role.roleTagIDs, transaction);
        })


        /*.then(() => {
          // TODO: Delete the skill entries
          return Array.isArray(role.deleteSkills) && (role.deleteSkills.length > 0) && RoleSkillEntry.delMany({ roleID, role.deleteSkills }, user, transaction);
        })

        .then(() => {
          // Create: this will also modify if an entry already exists
          return Array.isArray(role.addSkills) && (role.addSkills.length > 0) && RoleSkillEntry.createMany({ roleID, role.addSkills }, user, transaction);
        })*/

        .then(() => {
          // Use overwrite skills
          return Array.isArray(role.skillEntries) && RoleSkillEntry.overwriteSkills({ roleID, skillEntries : role.skillEntries }, user, transaction);
        })


        .then(() => {
          // Don't commit the transaction if it doesn't belong to us
          if(higherTransaction){
            return roleID;
          }else{
            return transaction.commit().return(roleID);
          }
        })

        .catch((e) => {
          if(higherTransaction){
            throw e;
          }else{
            return transaction.rollback().throw(e);
          }
        });
    });
  });
});


rolesAPI.editMany = makeAPIFunction(function editMany({ projectID, roles }, user, higherTransaction){
  // Get a transaction
  let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
  return transactionPromise.then(transaction => {

    return Promise.reduce(roles || [], (_, role) => {
      return this.edit({ projectID, role }, user, transaction);
    }, null)

    .then((ids) => {
      // Don't commit the transaction if it doesn't belong to us
      if(higherTransaction){
        return ids;
      }else{
        return transaction.commit().return(ids);
      }
    })

    .catch((e) => {
      if(higherTransaction){
        throw e;
      }else{
        return transaction.rollback().throw(e);
      }
    });
  });
});



rolesAPI.del = makeAPIFunction(function del({ projectID, roleID }, user, higherTransaction){
  if(!projectID){
    return Promise.reject(new APIInputError("No project ID provided", "projectID", "role.delete"));
  }
  if(!roleID){
    return Promise.reject(new APIInputError("No role ID provided", "roleID", "role.delete"));
  }
  return user.canModifyProject(projectID).then(allowed => {
    if(!allowed){
      throw new AuthorisationError("You do not have permission to delete that role", "role.delete");
    }

    // Get a transaction
    let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
    return transactionPromise.then(transaction => {
      // Check if there are assignments
      return Assignment.search({ roleID }, user).then(results => {
        if(results && results.length > 0){
          throw new APIError("Role cannot be deleted, it has been assigned", "role.delete", 409);
        }
      })
      .then(() => {
        let query = `DELETE FROM ${this.table}
          WHERE [projectID] = @projectID AND [ID] = @roleID`;

        let params = [
          [ "projectID", "Int", projectID ],
          [ "roleID", "Int", roleID ]
        ];

        return queryDB(query, params, transaction);
      })
      .then(() => {
        // Don't commit the transaction if it doesn't belong to us
        if(!higherTransaction){
          return transaction.commit().return();
        }
      })

      .catch((e) => {
        if(higherTransaction){
          throw e;
        }else{
          return transaction.rollback().throw(e);
        }
      });
    });
  });
});

rolesAPI.delMany = makeAPIFunction(function delMany({ projectID, roleIDs }, user, higherTransaction){
  // Get a transaction
  let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
  return transactionPromise.then(transaction => {

    return Promise.reduce(roleIDs || [], (_, roleID) => {
      return this.del({
        projectID : projectID,
        roleID : roleID
      }, user, transaction);
    }, null)

    .then(() => {
      // Don't commit the transaction if it doesn't belong to us
      if(!higherTransaction){
        return transaction.commit().return();
      }
    })

    .catch((e) => {
      if(higherTransaction){
        throw e;
      }else{
        return transaction.rollback().throw(e);
      }
    });
  });
});



rolesAPI.duplicate = makeAPIFunction(function duplicate({ roleID, changes }, user, higherTransaction){
  if(!roleID){
    throw new APIInputError("No role ID provided", "roleID", "role.duplicate");
  }

  let transactionPromise = (higherTransaction) ? Promise.resolve(higherTransaction) : getTransaction();
  return transactionPromise.then(transaction => {
    return this.getProjectForRole({ roleID }, user, transaction)
      .then(project => {
        // If there is no project, there is no role
        if(!project){
          throw new APIInputError("Role does not exist", "roleID", "role.duplicate");
        }

        // Firstly a query is run to copy the basic information over from the current roleID's role
        // See https://technet.microsoft.com/en-us/library/ms189872(v=sql.105).aspx
        let colNamesWithoutID = this.getColumnNames();
        let index = colNamesWithoutID.indexOf("ID");
        if(index > -1){
          colNamesWithoutID.splice(index, 1);
        }

        let query = `
          INSERT INTO ${this.table} (${colNamesWithoutID.join(",")})
            SELECT ${colNamesWithoutID.join(",")} FROM ${this.table} WHERE [ID] = @roleID;
          SELECT SCOPE_IDENTITY() AS new_id;
        `;

        let params = [
          [ "roleID", "Int", roleID ]
        ];

        return getOne(query, params, transaction)
          .tap(({ new_id }) => {
            // Next we copy over the auxillary tables
            let query = `
              INSERT INTO [projects].[Roles_Locations]
                SELECT [locationID], @new_id as [roleID] FROM [projects].[Roles_Locations]
                  WHERE [roleID] = @roleID;

              INSERT INTO [projects].[Roles_Accounts]
                SELECT [accountID], @new_id as [roleID] FROM [projects].[Roles_Accounts]
                  WHERE [roleID] = @roleID;

              INSERT INTO [projects].[Roles_CustomerTopUps]
                SELECT [customerTopUpID], @new_id as [roleID] FROM [projects].[Roles_CustomerTopUps]
                  WHERE [roleID] = @roleID;

              INSERT INTO [projects].[Roles_RoleTags]
                SELECT [roleTagID], @new_id as [roleID] FROM [projects].[Roles_RoleTags]
                  WHERE [roleID] = @roleID;
            `;

            let params = [
              [ "roleID", "Int", roleID ],
              [ "new_id", "Int", new_id ]
            ];

            return queryDB(query, params, transaction);
          })
          .tap(({ new_id }) => {
            // Edit the submitted by
            let query = `
              UPDATE ${this.table} SET submittedBy = @submittedBy
                WHERE [ID] = @new_id
            `;

            let params = [
              [ "new_id", "Int", new_id ],
              [ "submittedBy", "Int", user.getID() ]
            ];

            return queryDB(query, params, transaction);
          })
          .tap(({ new_id }) => {
            if(changes){
              return this.edit({
                projectID : project.ID,
                role : {
                  roleID : new_id,
                  role : changes
                }
              }, user, transaction);
            }
          })
          .then(({ new_id }) => {
            return { ID : new_id };
          })


          .tap(() => {
            // Don't commit the transaction if it doesn't belong to us
            if(!higherTransaction){
              return transaction.commit();
            } 
          })
          .catch((e) => {
            if(higherTransaction){
              throw e;
            }else{
              return transaction.rollback().throw(e);
            }
          });
      });
  });
});





/* Util functions for create & edit */
function addLocationsToRole(roleID, locationIDs, transaction){
  // Delete all and then remake the list
  let query = `DELETE FROM [projects].[Roles_Locations]
    WHERE roleID = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ], transaction)
    .then(() => {
      let query = `INSERT INTO [projects].[Roles_Locations]
          ([locationID], [roleID])
          VALUES (@locationID, @roleID)`;

      return Promise.reduce(locationIDs || [], (_, locationID) => {
        return queryDB(query, [
          [ "locationID", "Int", locationID ],
          [ "roleID", "Int", roleID ]
        ], transaction);
      }, null);
    });
}

function addAccountsToRole(roleID, accountIDs, transaction){
  // Delete all and then remake the list
  let query = `DELETE FROM [projects].[Roles_Accounts]
    WHERE roleID = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ], transaction)
    .then(() => {
      let query = `INSERT INTO [projects].[Roles_Accounts]
          ([accountID], [roleID])
          VALUES (@accountID, @roleID)`;

      return Promise.reduce(accountIDs || [], (_, accountID) => {
        return queryDB(query, [
          [ "accountID", "Int", accountID ],
          [ "roleID", "Int", roleID ]
        ], transaction);
      }, null);
    });
}

function addTopUpsToRole(roleID, customerTopUpIDs, transaction){
  // Delete all and then remake the list
  let query = `DELETE FROM [projects].[Roles_CustomerTopUps]
    WHERE roleID = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ], transaction)
    .then(() => {
      let query = `INSERT INTO [projects].[Roles_CustomerTopUps]
          ([customerTopUpID], [roleID])
          VALUES (@customerTopUpID, @roleID)`;

      return Promise.reduce(customerTopUpIDs || [], (_, customerTopUpID) => {
        return queryDB(query, [
          [ "customerTopUpID", "Int", customerTopUpID ],
          [ "roleID", "Int", roleID ]
        ], transaction);
      }, null);
    });
}


function addRoleTagsToRole(roleID, roleTagIDs, transaction){
  // Delete all and then remake the list
  let query = `DELETE FROM [projects].[Roles_RoleTags]
    WHERE roleID = @roleID`;

  return queryDB(query, [ "roleID", "Int", roleID ], transaction)
    .then(() => {
      let query = `INSERT INTO [projects].[Roles_RoleTags]
          ([roleTagID], [roleID])
          VALUES (@roleTagID, @roleID)`;

      return Promise.reduce(roleTagIDs || [], (_, roleTagID) => {
        return queryDB(query, [
          [ "roleTagID", "Int", roleTagID ],
          [ "roleID", "Int", roleID ]
        ], transaction);
      }, null);
    });
}


module.exports = rolesAPI;
