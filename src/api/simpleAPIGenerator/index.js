const generateSimpleAPIFunctions = require("./generateSimpleAPIFunctions");
const APIGenerator = require("../APIGenerator");

module.exports = function simpleAPIGenerator(tableName, schemaName = "core", columns = [ [ "name", "Text" ] ]){
  let generator = new APIGenerator({ tableName, schemaName, columns });
  return generateSimpleAPIFunctions(generator);
};

