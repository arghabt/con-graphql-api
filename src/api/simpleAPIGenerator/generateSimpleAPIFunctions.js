// Generator for a simple data structures (name and IDs)
const Promise = require("bluebird");

const APIGenerator = require("../APIGenerator");
const makeAPIFunction = APIGenerator.makeAPIFunction;

const moment = require("moment");

const { AuthorisationError } = require("../Errors");

const simpleFunctions = require("./simpleFunctions");


function refreshCache(generator, args){
  return APIGenerator.prototype.getAll.apply(generator, args) // Devolve to the original getAll
    .tap(data => {
      this.data = data;
      this.valid = true;
      this.createTime = moment();
    });
}


function createCache(){
  return {
    valid : false,
    data : [],
    createTime : moment(),
    get invalidated(){
      return !this.valid || !Array.isArray(this.data) || this.data.length === 0 || this.createTime.isBefore(Date.now() - 1800000); // 1800000 = 30 minutes
    },
    refreshCache
  };
}


module.exports = function generateSimpleAPIFunctions(generator){
  // Create a cache system
  let cache = createCache();

  // Assumes that simples have no sql manipulators or filters
  generator.getAll = function getAll(){
    return Promise.try(() => {
      if(cache.invalidated){
        return cache.refreshCache(this, arguments);
      }

      // Use the cache
      return cache.data.slice();
    });
  };

  generator.getOne = function getOne(args){
    return Promise.try(() => {
      if(!args.ID && args.ID !== 0){
        return null;
      }

      if(cache.invalidated){
        return cache.refreshCache(this, arguments).then(r => {
          return r.find(i => i.ID === args.ID);
        });
      }

      let item = cache.data.find(i => i.ID === args.ID);
      if(item){
        return item;
      }

      return APIGenerator.prototype.getOne.apply(this, arguments)
        .tap(r => {
          if(r){
            cache.data.push(r);
          }
        });
    });
  };


  // TODO: Implement search
  

  // Internal function
  generator.getByName = function getByName(name){
    return Promise.try(() => {
      if(cache.invalidated){
        return cache.refreshCache(this, arguments).then(r => {
          return r.find(i => i.name === name || i.codename === name);
        });
      }

      let item = cache.data.find(i => i.name === name || i.codename === name);
      if(item){
        return item;
      }

      return cache.refreshCache(this, arguments).then(r => {
        return r.find(i => i.name === name || i.codename === name);
      });
    });
  };



  // Add add/edit/delete functions!
  generator.create = makeAPIFunction(function create(obj, user){
    return user.isAdmin()
      .tap(allowed => {
        if(!allowed){
          throw new AuthorisationError("You do not have permission to do that", `${generator.getTableName()}.create`);
        }
      })
      .then(() => simpleFunctions.create.call(this, generator, obj, user))
      .tap(() => {
        cache.valid = false;
      });
  });


  generator.edit = makeAPIFunction(function edit(obj, user){
    return user.isAdmin()
      .tap(allowed => {
        if(!allowed){
          throw new AuthorisationError("You do not have permission to do that", `${generator.getTableName()}.edit`);
        }
      })
      .then(() => simpleFunctions.edit.call(this, generator, obj, user))
      .tap(() => {
        cache.valid = false;
      });
  });

  generator.del = makeAPIFunction(function del(obj, user){
    return user.isAdmin()
      .tap(allowed => {
        if(!allowed){
          throw new AuthorisationError("You do not have permission to do that", `${generator.getTableName()}.del`);
        }
      })
      .then(() => simpleFunctions.del.call(this, generator, obj, user))
      .tap(() => {
        cache.valid = false;
      });
  });




  return generator;
};

