// Generator for a simple data structures (name and IDs)
const { queryDB, getOne } = require("../../database");
const { APIInputError } = require("../Errors");

module.exports = {
  // Add add/edit/delete functions!
  create(generator, obj, user){
    let colNames = generator.getColumnNames();
    colNames.shift(); // Remove ID

    let inputObj = obj.input;


    let remainingFields = colNames.filter(field => (inputObj[field] !== undefined && inputObj[field] !== ""));

    if(remainingFields.length > 0){
      let query = `INSERT INTO ${this.table}
        (${remainingFields.join(",")})
        VALUES (${remainingFields.map(field => `@${field}`).join(",")});
        SELECT SCOPE_IDENTITY() AS new_id;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        inputObj[field]
      ]);

      return getOne(query, params)
        .then(({ new_id }) => ({
          ID : new_id
        }));
    }else{
      throw new APIInputError("At least one field is required", undefined, `${generator.getTableName()}.create`);
    }
  },


  edit(generator, obj, user){
    let colNames = generator.getColumnNames();
    colNames.shift(); // Remove ID

    let ID = obj.ID;
    let inputObj = obj.input;


    let remainingFields = colNames.filter(field => (inputObj[field] !== undefined && inputObj[field] !== ""));

    if(remainingFields.length > 0){
      // Construct query
      let query = `UPDATE ${this.table} SET
        ${remainingFields.map(field => {
          return `${field} = @${field}`;
        }).join(",")}
        WHERE [ID] = @ID;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        inputObj[field]
      ]);

      // Push on the ID
      params.push([
        "ID",
        undefined,
        ID
      ]);

      return queryDB(query, params)
        .return({ ID });
    }else{
      return { ID };
    }
  },

  del(generator, { ID }, user){
    let query = `DELETE FROM ${this.table}
      WHERE [ID] = @ID`;

    let params = [
      [ "ID", "Int", ID ]
    ];

    return queryDB(query, params)
      .return(true);
  }
};

