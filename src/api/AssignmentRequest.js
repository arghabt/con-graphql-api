// Assignment requests - Extra data
const Promise = require("bluebird");

const APIGenerator = require("./APIGenerator");
const { makeAPIFunction } = APIGenerator;
const { APIInputError, AuthorisationError } = require("./Errors.js");

const database = require("../database/extra");
const mainDatabase = require("../database/main");


const assignmentRequestAPI = new APIGenerator({
  tableName : "AssignmentRequests",
  schemaName : "projects",
  includeID : false,
  database : database,
  columns : [
    [ "roleID", "FK" ],
    [ "staffID", "FK" ],
    [ "submittedBy", "FK" ],
    [ "timestamp", "DateTime" ]
  ]
});


assignmentRequestAPI.addSearchOption({
  optionName : "roleHasAssignment",
  type : "Bit",
  queryFragment : function(has){
    let q = "";
    if(!has){
      q = `NOT `;
    }

    const Assignment = require("./Assignment");
    let assignmentTableName = `[${mainDatabase.databaseName}].${Assignment.table}`;

    q += `EXISTS (SELECT 1 AS A FROM ${assignmentTableName}
      WHERE ${this.table}.[roleID] = ${assignmentTableName}.[roleID])`;

    return q;
  }
});


assignmentRequestAPI.get = makeAPIFunction(function get({ roleID, staffID }, user){
  return this.search({ roleID, staffID }, user).then(result => result[0]);
});


assignmentRequestAPI.create = makeAPIFunction(function create({ roleID, staffID }, user){
  let currentUserID = user.getID();
  if(!currentUserID){
    throw new AuthorisationError("You do not have permission to do that", "assignmentRequest.create");
  }
  if(!roleID){
    throw new APIInputError("roleID must be supplied", "roleID", "assignmentRequest.create");
  }
  if(!staffID){
    throw new APIInputError("staffID must be supplied", "staffID", "assignmentRequest.create");
  }

  const Role = require("./Role");
  const Staff = require("./Staff");
  const Assignment = require("./Assignment");

  return Promise.join(
    Role.getOne({ ID : roleID }, user),
    Staff.getOne({ ID : staffID }, user),
    Assignment.search({ roleID }, user),
    (role, staff, assignment) => {
      if(!role){
        throw new APIInputError("Role does not exist", "roleID", "assignmentRequest.create");
      }
      if(!staff){
        throw new APIInputError("Staff member does not exist", "staffID", "assignmentRequest.create");
      }

      if(assignment && assignment[0]){
        throw new APIInputError("That role has already been assigned", "roleID", "assignmentRequest.create");
      }

      let query = `INSERT INTO ${this.table}
          (roleID, staffID, submittedBy)
          VALUES (@roleID, @staffID, @submittedBy)`;

      let params = [
        [ "roleID", "Int", roleID ],
        [ "staffID", "Int", staffID ],
        [ "submittedBy", "Int", currentUserID ]
      ];

      return database.queryDB(query, params)
        .return({ roleID, staffID });
    });
});


assignmentRequestAPI.del = makeAPIFunction(function del({ roleID, staffID }, user){
  if(!roleID){
    throw new APIInputError("roleID must be supplied", "roleID", "assignmentRequest.create");
  }
  if(!staffID){
    throw new APIInputError("staffID must be supplied", "staffID", "assignmentRequest.create");
  }

  return Promise.join(
    this.search({ roleID, staffID }, user),
    user.isResourcingManager(),
    (request, isResourcingManager) => {
      let authError = new AuthorisationError("You do not have permission to do that", "assignmentRequest.del");

      // Ignores if there is no current request:
      if(!request || !request[0]){
        // Throw an error if they are not a resourcing manager
        if(!isResourcingManager){
          throw authError;
        }else{
          // There is no request, so nothing to delete, return
          return true;
        }
      }
      if(!isResourcingManager && request[0].submittedBy !== user.getID()){
        throw authError;
      }


      let query = `DELETE FROM ${this.table}
        WHERE roleID = @roleID AND staffID = @staffID`;

      let params = [
        [ "roleID", "Int", roleID ],
        [ "staffID", "Int", staffID ]
      ];

      return database.queryDB(query, params)
        .return(true);
    });
});


assignmentRequestAPI.convertToAssignment = makeAPIFunction(function convertToAssignment(inp, user){
  const Assignment = require("./Assignment");
  // Actually devolves the request to the two different API functions
  // First create the new assignment
  return Assignment.create(inp, user)
    .tap(() => {
      // Delete the assignment request
      // If they are able to create an assignment
      // they will be able to delete a request
      return this.del(inp, user);
    });
});





module.exports = assignmentRequestAPI;
