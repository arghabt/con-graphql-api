// API functions that will be created using the simpleAPIGenerator
// To create new API functions for a simple data table insert an entry into this file

const simpleAPIGenerator = require("./simpleAPIGenerator");

module.exports = {
  AdminRole : simpleAPIGenerator("AdminRoles", "staff"),

  CapabilityArea : simpleAPIGenerator("CapabilityAreas", "core", [
    [ "name", "Text" ],
    [ "technicalCapabilitySpecialistID", "FK" ],
    [ "description", "Text" ]
  ]),

  Profession : simpleAPIGenerator("Professions", "core", [
    [ "name", "Text" ],
    [ "subjectMatterOwnerID", "FK" ],
    [ "confluenceURL", "Text" ]
  ]),

  Clearance : simpleAPIGenerator("Clearance"),
  Customer : simpleAPIGenerator("Customers"),
  FundingType : simpleAPIGenerator("FundingTypes"),
  PracticeArea : simpleAPIGenerator("PracticeAreas"),
  SFIAGrade : simpleAPIGenerator("SFIAGrades"),
  Location : simpleAPIGenerator("Locations", "core", [
    [ "codename", "Text" ],
    [ "realname", "Text" ]
  ]),

  Grade : simpleAPIGenerator("Grades", "core", [
    [ "code", "Text" ],
    [ "name", "Text" ]
  ]),

  SkillTag : simpleAPIGenerator("SkillTags", "skills"),
  RoleTag : simpleAPIGenerator("RoleTags", "projects"),

  ProjectStage : simpleAPIGenerator("ProjectStage", "projects", [
    [ "name", "Text" ],
    [ "position", "Int" ]
  ]),

  CustomerAccount : simpleAPIGenerator("CustomerAccounts", "core", [
    [ "name", "Text" ],
    [ "customerID", "FK" ]
  ]),

  ClearanceHolder : simpleAPIGenerator("ClearanceHolders", "core", [
    [ "name", "Text" ],
    [ "customerName", "Text" ]
  ]),

  ClearanceSponsor: simpleAPIGenerator("ClearanceSponsors", "core", [
    [ "name", "Text" ],
    [ "customerID", "FK" ]
  ]),

  CustomerTopUp : simpleAPIGenerator("CustomerTopUps", "core", [
    [ "name", "Text" ],
    [ "customerID", "FK" ]
  ]),

  ECIProject : simpleAPIGenerator("ECIProjects"),

  MentorRelationshipStatus : simpleAPIGenerator("MentorRelationshipStatus", "mentoringtool", [
    [ "position", "Int" ],
    [ "name", "Text" ],
  ]),
};
