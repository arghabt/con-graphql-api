// Staff Attachment functions

const APIGenerator = require("./APIGenerator");
const makeAPIFunction = APIGenerator.makeAPIFunction;
const { AuthorisationError, APIError } = require("./Errors.js");

const { getOne, queryDB } = require("../database");
const endpoint = require("config").get("server.fileEndpoint");

const staffAttachmentsAPI = new APIGenerator({
  tableName : "StaffAttachments",
  schemaName : "staff",
  columns : [
    [ "staffID", "FK" ],
    [ "filename", "NVarChar" ]
  ]
});


function generateRelativeUrl(result){
  return `${endpoint}/${result.staffID}/${result.ID}/${result.filename}`;
}

/* TODO
function generateAbsoluteUrl(result){
  return `${generateRelativeUrl(result)}`;
}*/

staffAttachmentsAPI.addFilter([ "get", "getAll", "search" ], function(results, _, user){
  if(Array.isArray(results)){
    return results.map(result => {
      result.url = generateRelativeUrl(result);
      return result;
    });
  }else{
    results.url = generateRelativeUrl(results);
    return results;
  }
});

staffAttachmentsAPI.getFile = makeAPIFunction(function getFile({ staffID, attachmentID, filename }, user){
  return user.canGetAttachment(attachmentID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission get that file", "staffAttachment.getFile");
      }
    })
    .then(() => {
      let query = `SELECT ${this.table}.[CV] FROM ${this.table}
        WHERE ${this.table}.[ID] = @ID AND
        ${this.table}.[staffID] = @staffID AND
        ${this.table}.[filename] = @filename`;

      let params = [
        [ "ID", "Int", attachmentID ],
        [ "staffID", "Int", staffID ],
        [ "filename", "NVarChar", filename ]
      ];

      return getOne(query, params).then(r => {
        if(!r){
          throw new APIError("404: Not found", "staffAttachment.getFile", 404);
        }
        return r.CV;
      });
    });
});

staffAttachmentsAPI.saveFile = makeAPIFunction(function saveFile({ staffID, filename, attachment }, user){
  return user.canUploadAttachmentForStaffMember(staffID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission save files for that user", "staffAttachment.saveFile");
      }
    })
    .then(() => {
      let query = `INSERT INTO ${this.table}
        ([staffID], [filename], [CV])
        VALUES (@staffID, @filename, @attachment)`;

      let params = [
        [ "staffID", "Int", staffID ],
        [ "filename", "NVarChar", filename ],
        [ "attachment", "VarBinary", attachment ]
      ];

      return queryDB(query, params);
    });
});


staffAttachmentsAPI.deleteFile = makeAPIFunction(function deleteFile({ attachmentID }, user){
  return this.getOne({ ID : attachmentID }, user)
    .then(({ staffID }) => user.canUploadAttachmentForStaffMember(staffID))
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission delete files for that user", "staffAttachment.deleteFile");
      }
    })
    .then(() => {
      let query = `DELETE FROM ${this.table}
        WHERE [ID] = @attachmentID`;

      let params = [
        [ "attachmentID", "Int", attachmentID ]
      ];

      return queryDB(query, params).return(true);
    });
});


module.exports = staffAttachmentsAPI;
