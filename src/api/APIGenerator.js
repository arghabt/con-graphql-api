// Generator for a general data type api functions
const Promise = require("bluebird");

const { databases, sql : { RequestError } } = require("../database");
const { APIInputError } = require("./Errors");


class APIGenerator {

  constructor({ tableName, schemaName, columns, database, includeID = true }){
    if(!tableName){
      throw new Error("tableName is required");
    }
    if(!schemaName){
      throw new Error("schemaName is required");
    }
    if(!columns){
      throw new Error("columns is required");
    }

    // Resolve the database
    if(!database){
      this.database = databases.main;
    }else{
      if(typeof database === "string"){
        this.database = databases[database];
      }else{
        this.database = database;
      }
    }

    if(!this.database || !this.database.getDatabaseVersion){
      throw new Error("Database is not provided or invalid");
    }

    this.tableName = tableName;
    this.table = `[${schemaName}].[${tableName}]`;

    this.inputChangers = {
      getAll : null,
      getOne : null,
      search : null
    };
    this.sqlManipulators = {
      getAll : null,
      getOne : null,
      search : null
    };
    this.filters = {
      getAll : null,
      getOne : null,
      search : null
    };



    // Convert columns to objects
    this.columns = columns
      .map(([ name, type, authorisationFunction, columnQueryFragment ]) => ({
          name,
          type,
          authorisationFunction,
          columnQueryFragment,
        }));
    // Automatically add ID
    if(includeID){
      this.columns.unshift({ name : "ID", type : "PK" });
    }


    this.complexCols = this.columns
      .filter(({ columnQueryFragment }) => columnQueryFragment);
    this.simpleCols = this.columns
      .filter(({ columnQueryFragment }) => !columnQueryFragment);
    this.simpleColNames = this.simpleCols.map(({ name }) => name).filter(n => n);

    // Generate the search options
    this.searchOptions = APIGenerator.generateSearchOptions(this.columns);

    // Add the inactive field
    if(this.searchOptions.find(({ name }) => name === "inactive")){
      this.createIncludeActiveField();
      this._hasInactiveField = true;
    }else{
      this._hasInactiveField = false;
    }
  }

  // THIS IS NOT YET COMPATIBLE WITH PROMISIFIED AUTHUSER FUNCTIONS
  applyAuthorisationFunction(col, user){
    if(!col.authorisationFunction){
      return true;
    }
    if(col.authorisationFunction.call){
      return col.authorisationFunction.call(user, col);
    }
    const AuthUser = require("../auth/AuthUser");
    let af = AuthUser.prototype[col.authorisationFunction];
    if(af){
      return af.call(user, col);
    }
    throw new Error(`Authorisation Function (${col.authorisationFunction}) is invalid`);
  }

  getColumns(){
    return this.columns.map(col => Object.assign({}, col));
  }
  getSimpleColumns(){
    return this.simpleCols.map(col => Object.assign({}, col));
  }
  getComplexColumns(){
    return this.complexCols.map(col => Object.assign({}, col));
  }
  getColumnNames(){
    return this.simpleColNames.slice();
  }
  getSQLSelectFieldsString(user){
    let simpleSQLCols = this.simpleCols
      .filter(col => this.applyAuthorisationFunction(col, user))
      .map(col => `[${col.name}]`);

    let complexSQLCols = this.complexCols
      .filter(col => this.applyAuthorisationFunction(col, user))
      .map(col => `(${col.columnQueryFragment}) AS ${col.name}`);

    return `
      ${simpleSQLCols.join(",")}
      ${this.complexCols.length > 0 ? "," + complexSQLCols.join(",") : ""}
    `;
  }
  getSimpleSelect(user){
    // Generate simple select
    let simpleSelect = `SELECT ${this.getSQLSelectFieldsString(user)} FROM ${this.table}`;
    return simpleSelect;
  }
  getTableName(){
    return this.tableName;
  }
  getTable(){
    return this.table;
  }
  getSearchOptions(){
    return this.searchOptions.map(col => Object.assign({}, col));
  }

  hasInactiveField(){
    return this._hasInactiveField;
  }

  addSearchOption({ optionName, type, queryFragment, filter, description, options = {} }){
    if(!optionName){
      throw new Error("optionName is required");
    }
    if(!type){
      throw new Error("type is required");
    }
    if(this.searchOptions.find(({ name }) => name === optionName)){
      throw new Error(`Option ${optionName} already exists`);
    }
    if(optionName === "inactive" && options.autoFilter !== false){
      this.createIncludeActiveField();
      this._hasInactiveField = true;
    }

    let searchOptionObject = {
      name : optionName,
      type,
      queryFragment,
      filter,
      description,
      options
    };

    // Determine the search option type to create
    if(APIGenerator.isArrayType(type)){
      // Extract the original type from the array type
      let match = type.match(/^\[(.+)\]$/);
      if(!Array.isArray(match) || match.length < 2){
        throw new Error(`Could not determine the original type of ${optionName} (type given: ${type})`);
      }
      let originalType = match[1];
      searchOptionObject.type = originalType;

      return this.searchOptions.push(
        APIGenerator.generateArrayType(searchOptionObject)
      );
    }

    if(APIGenerator.isNumberComparatorType(type)){
      return this.searchOptions.push(
        APIGenerator.generateNumberComparatorType(searchOptionObject)
      );
    }

    if(APIGenerator.isDateComparatorType(type)){
      return this.searchOptions.push(
        APIGenerator.generateDateComparatorType(searchOptionObject)
      );
    }

    if(APIGenerator.isCustomSearchType(type)){
      return this.searchOptions.push(
        APIGenerator.generateCustomSearchType(searchOptionObject)
      );
    }

    // Default, generate normal type
    return this.searchOptions.push(
      APIGenerator.generateNormalType(searchOptionObject)
    );
  }

  createIncludeActiveField(){
    // Push the override option to the list
    this.searchOptions.push(
      APIGenerator.generateNormalType({
        name : "includeInactive",
        type : "Bit",
        description : "If set to true, objects marked as inactive will be included the result set"
      })
    );
  }

  addInputChanger(forEndpoint, func){
    if(!Array.isArray(forEndpoint)){
      forEndpoint = [ forEndpoint ];
    }

    forEndpoint.forEach((endpoint) => {
      this.inputChangers[endpoint] = func;
    });
  }

  addSQLManipulator(forEndpoint, func){
    if(!Array.isArray(forEndpoint)){
      forEndpoint = [ forEndpoint ];
    }

    forEndpoint.forEach((endpoint) => {
      this.sqlManipulators[endpoint] = func;
    });
  }

  addFilter(forEndpoint, func){
    if(!Array.isArray(forEndpoint)){
      forEndpoint = [ forEndpoint ];
    }

    forEndpoint.forEach((endpoint) => {
      this.filters[endpoint] = func;
    });
  }


  applyInputChanger(type, args, user){
    let func = this.inputChangers[type];
    if(func && func.call){
      return Promise.resolve(func.call(this, args, user));
    }else{
      return Promise.resolve(args);
    }
  }

  applySQLManipulator(type, query, args, user){
    let func = this.sqlManipulators[type];
    if(func && func.call){
      return Promise.resolve(func.call(this, query, args, user));
    }else{
      return Promise.resolve(query);
    }
  }

  applyFilter(type, results, args, user){
    let func = this.filters[type];
    if(func && func.call){
      return Promise.resolve(func.call(this, results, args, user));
    }else{
      return Promise.resolve(results);
    }
  }






  getAll(args, user, transaction){
    return this.applyInputChanger("getAll", Object.assign({}, args), user)
      .then(args => {
        // If args is falsy, then we do not continue (return null)
        // an empty object will continue
        if(!args){
          return null;
        }

        if(this.hasInactiveField() && !args.includeInactive){
          // If inactive is passed in, devolve request to this.search
          if(args.inactive === undefined){
            args.inactive = false;
          }

          return this.search({ inactive : args.inactive, orderBy : args.orderBy }, user);
        }

        // WHERE 1 = 1 means that SQL manipulators do not have to worry about adding WHERE
        let query = this.getSimpleSelect(user) + " WHERE 1 = 1";

        return this.applySQLManipulator("getAll", query, args, user)
          .then(q => {
            // Apply orderBy if required
            let orderBy = args.orderBy;
            if(orderBy){
              orderBy = this.checkOrderBy(orderBy);
              return `${q} ${this.applyOrderBy(orderBy)}`;
            }else{
              return q;
            }
          })
          .then(q => this.database.queryDB(q, undefined, transaction))
          .then(results => this.applyFilter("getAll", results, args, user));
      });
  }

  getOne(args, user, transaction){
    return this.applyInputChanger("getOne", args, user)
      .then(args => {
        if(!args || args.ID === null || args.ID === undefined){
          return null;
        }

        let query = this.getSimpleSelect(user) + ` WHERE ${this.table}.[ID] = @ID`;


        return this.applySQLManipulator("getOne", query, args, user)
          .then(q => this.database.getOneByID(q, args.ID, transaction))
          .then(result => this.applyFilter("getOne", result, args, user));
      });
  }

  search(rawArgs, user, transaction){
    return this.applyInputChanger("search", Object.assign({}, rawArgs), user)
      .then(args => {
        // If args is falsy, then we do not continue (return null)
        // an empty object will continue
        if(!args){
          return null;
        }

        // Operator and orderBy are not used in the query directly, so delete it from args
        let operator = args.operator;
        let orderBy = args.orderBy;
        let includeInactive = args.includeInactive;
        delete args.operator;
        delete args.orderBy;
        delete args.includeInactive;

        // Add the inactive as a search option (set to false) by default
        if(this.hasInactiveField() && args.inactive === undefined && !includeInactive){
          args.inactive = false;
        }


        // Get the searchable columns
        let searchableCols = this.searchOptions.filter(({ name }) => (args[name] !== undefined/* && args[name] !== null*/));


        if(searchableCols.length === 0){
          return this.getAll(rawArgs, user); // Nothing to search, return all
        }

        // Filter out invalid array elements
        // - proto of args is set to null by gql, so hasOwnProperty is not required
        for(let arg in args){ // eslint-disable-line guard-for-in
          let a = args[arg];
          if(Array.isArray(a)){
            args[arg] = a.filter(b => b !== undefined && b !== null);
          }
        }

        // Check operator or orderBy are valid
        operator = this.checkOperator(operator);
        orderBy = this.checkOrderBy(orderBy);

        // Generate the select statement
        let fragments = this.getSearchFragments(searchableCols, args, user).filter(o => o);

        if(fragments.length === 0){
          return this.getAll(rawArgs, user); // Nothing to search, return all
        }

        let query = this.getSimpleSelect(user) + " WHERE " + this.applyOperator(fragments, operator);
        let params = this.convertArgsToInputArrays(searchableCols, args);
        // Push extra param for user id
        params.push([ "loggedInUser", "Int", user.getID() ]);


        return this.applySQLManipulator("search", query, args, user)
          // Apply the order by after the SQL manipulator, so that a manipulator
          // can always add things to the where clause easily
          .then(q => q + this.applyOrderBy(orderBy))
          .then(q => this.database.queryDB(q, params, transaction))
          .then(results => this.applySearchFilters(searchableCols, results, args, user))
          .then(results => this.applyFilter("search", results, args, user));
      });
  }


  checkOperator(operator){
    if(!operator){
      operator = "AND";
    }else{
      operator = operator.toUpperCase();
      if(operator !== "OR" && operator !== "AND"){
        throw new APIInputError("operator not recognised. Must be 'AND' or 'OR'");
      }
    }
    return operator;
  }

  checkOrderBy(orderBy){
    if(!orderBy || !orderBy.field){
      return null;
    }

    // Default direction = ASC
    if(!orderBy.direction){
      orderBy.direction = "ASC";
    }

    // Check for invalid direction
    if(orderBy.direction !== "ASC" && orderBy.direction !== "DESC"){
      throw new APIInputError("orderBy.direction notnot recognised. Must be 'ASC' or 'DESC'");
    }

    if(this.getColumnNames().includes(orderBy.field)){
      return orderBy;
    }else{
      throw new APIInputError(`orderBy.field not recognised. Must one of ${this.getColumnNames().join(", ")}.`); 
    }
  }


  applyOperator(fragments, operator){
    return fragments.join(` ${operator} `);
  }

  applyOrderBy(orderBy){
    if(!orderBy){
      return "";
    }else{
      return ` ORDER BY ${this.table}.[${orderBy.field}] ${orderBy.direction}`;
    }
  }


  convertArgsToInputArrays(searchableCols, args){
    let params = [];

    // Convert args into input param arrays
    searchableCols.forEach(({ name, type }) => {
      let arg = args[name];
      if(arg !== null && arg !== undefined){

        // Map comparator types into multiple inputs
        if(APIGenerator.isComparator(type)){
          return this.convertComparatorArgToInputArray(name, type, arg)
            .forEach(i => params.push(i));
        }

        // Map the FK array to multiple inputs
        if(APIGenerator.isArrayType(type)){
          return this.convertArrayArgToInputArray(name, arg)
            .forEach(i => params.push(i));
        }

        // Map objects to seperate items
        if(typeof arg === "object" && !arg.getTime){
          return this.convertObjectArgToInputArray(name, arg)
            .forEach(i => params.push(i));
        }


        // Default, normal push
        params.push([ name, undefined, arg ]);

      }
    });

    return params;
  }

  convertComparatorArgToInputArray(name, type, arg){
    let params = [];
    let mapping = APIGenerator.makeComparatorMapping(type, arg);

    for(let i in mapping){
      if(mapping.hasOwnProperty(i) && (mapping[i] || mapping[i] === 0)){
        params.push([ `${name}_${i}`, undefined, mapping[i] ]);
      }
    }

    return params;
  }

  convertArrayArgToInputArray(name, arg){
    let params = [];

    arg.forEach((a, i) => {
      let n = `${name}_${i}`;

      if(typeof a === "object"){
        return this.convertObjectArgToInputArray(n, a)
          .forEach(i => params.push(i));
      }else{
        params.push([ n, undefined, a ]);
      }
    });

    return params;
  }

  convertObjectArgToInputArray(name, arg){
    let params = [];

    for(let i in arg){
      if(arg[i] || arg[i] === 0){
        params.push([ `${name}_${i}`, undefined, arg[i] ]);
      }
    }

    return params;
  }

  getLeftSideOfSearchComparison(col){
    if(col.columnQueryFragment){
      return col.columnQueryFragment;
    }

    let tableName = this.table ? `${this.table}.` : "";
    if(col._originalName){
      return `${tableName}${col._originalName}`;
    }
    return `${tableName}${col.name}`;
  }

  getSearchFragments(searchableCols, args, user){
    return searchableCols.map((col) => {
      let { name, type, queryFragment } = col;
      let arg = args[name];

      // Supplied QF
      if(queryFragment !== undefined){
        // Skip construction if query fragment is supplied - not recommended for comparator types
        if(queryFragment && queryFragment.call){
          return queryFragment.call(this, arg, user); // Run queryFragment if function
        }else{
          return queryFragment;
        }
      }

      // For comparators
      if(APIGenerator.isComparator(type)){
        return this.getComparatorQueryFragment(col, arg);
      }

      // For array types
      if(APIGenerator.isArrayType(type)){
        return this.getArrayQueryFragment(col, arg);
      }

      if(APIGenerator.isCustomSearchType(type)){
        throw new Error(`No function defined, cannot deal with custom type ${type} (argument name ${name}).`);
      }

      // Default
      return this.getSearchQueryFragment(col, arg, user);
    });
  }


  getComparatorQueryFragment(col, arg){
    let { name, type } = col;
    let mapping = APIGenerator.makeComparatorMapping(type, arg);
    let leftSide = this.getLeftSideOfSearchComparison(col);
    let q = null;

    // 0 == fase :(
    if(
      (mapping.eq || mapping.eq === 0) ||
      (mapping.lt || mapping.lt === 0) ||
      (mapping.gt || mapping.gt === 0) ||
      (mapping.lte || mapping.lte === 0) ||
      (mapping.gte || mapping.gte === 0)
    ){
      q = "(";

      if(mapping.eq || mapping.eq === 0){
        q += `${name} = @${name}_eq`;
        if(
          (mapping.lt || mapping.lt === 0) ||
          (mapping.gt || mapping.gt === 0) ||
          (mapping.lte || mapping.lte === 0) ||
          (mapping.gte || mapping.gte === 0)
        ){
          q += " OR (";
        }
      }

      // LT
      if(mapping.lt || mapping.lt === 0){
        q += `${leftSide} < @${name}_lt`;
        if(
          (mapping.gt || mapping.gt === 0) ||
          (mapping.lte || mapping.lte === 0) ||
          (mapping.gte || mapping.gte === 0)
        ){
          q += " AND ";
        }
      }

      // GT
      if(mapping.gt || mapping.gt === 0){
        q += `${leftSide} > @${name}_gt`;
        if(
          (mapping.lte || mapping.lte === 0) ||
          (mapping.gte || mapping.gte === 0)
        ){
          q += " AND ";
        }
      }

      // LT/E
      if(mapping.lte || mapping.lte === 0){
        q += `${leftSide} <= @${name}_lte`;
        if(mapping.gte || mapping.gte === 0){
          q += " AND ";
        }
      }

      // GT/E
      if(mapping.gte || mapping.gte === 0){
        q += `${leftSide} > @${name}_gte`;
      }

      q += ")";
    }

    return q;
  }


  getArrayQueryFragment(col, arg){
    // Nothing here, return early
    if(arg.length === 0){
      return null;
    }

    let leftSide = this.getLeftSideOfSearchComparison(col);
    let q = `${leftSide} IN (${arg.map((_, i) => `@${col.name}_${i}`).join(",")})`;

    return q;
  }


  getSearchQueryFragment(col, arg, user){
    let { name, type } = col;
    let leftSide = this.getLeftSideOfSearchComparison(col);

    let q = `${leftSide} `;

    // if the arg is NULL change the operator
    // to IS NULL
    if(arg !== null){
      switch(type){
        case "Text" : 
          q += `LIKE ('%' + @${name} + '%')`;
          break;

        case "Int" :
        case "Float" :
        case "Date" :
        case "DateTime" :
        case "FK" :
        case "PK" :
        default :
          q += `= @${name}`;
      }
    }else{
      q += `IS NULL`;
    }

    return q;
  }


  applySearchFilters(searchableCols, results, args, user){
    // Extract the filter
    searchableCols
      .forEach(({ name, filter }) => {
        if(filter){
          results = filter.call(this, results, args[name], user);
        }
      });

    return results;
  }


}



/*
 * Functions that generate the search options from the inital input
 * Additionally, some of the type specific functions are called
 * when a new search option is added
 */
APIGenerator.generateSearchOptions = function generateSearchOptions(searchOptions){
  // Expand out int and date items to have a lessthan and greaterthan operators
  let newOptions = searchOptions.map(o => {
    // This is dereferenced as an array,
    // as this is how original db entries are defined
    let { type, queryFragment } = o;
    if(!queryFragment){
      // Ints
      if(APIGenerator.isNumberType(type)){
        return APIGenerator.generateNumberComparatorType(o);
      }

      if(APIGenerator.isDateType(type)){
        return APIGenerator.generateDateComparatorType(o);
      }

      if(APIGenerator.isCustomSearchType(type)){
        return APIGenerator.generateCustomSearchType(o);
      }

    }

    return APIGenerator.generateNormalType(o);
  });

  // Add an extra option to newOptions for every FK and PK (ID), which makes it searchable with an
  // array of Ints as well as an individual int
  searchOptions
    .forEach(({ name, type, queryFragment }) => {
      if(!queryFragment && (type === "FK" || type === "PK")){
        newOptions.push(
          APIGenerator.generateArrayType({
            name : APIGenerator.generateArrayTypeName(name),
            type,
            queryFragment,
            _originalName : name,
          })
        );
      }
    });


  // No point in adding operator if there is only one option..
  if(newOptions.length > 1){
    newOptions.push({ name : "operator", type : "Text" });
  }
  newOptions.push({ name : "orderBy", type : "OrderBy" });

  return newOptions;
};


APIGenerator.generateNormalType = function generateNormalType(inp){
  return Object.assign({}, inp);
};


APIGenerator.generateNumberComparatorType = function generateNumberComparatorType(inp){
  return Object.assign({}, inp, {
    type : "NumberComparator",
    _isComparatorType : true,
    _originalType : inp.type || "Int",
  });
};

APIGenerator.generateDateComparatorType = function generateDateComparatorType(inp){
  return Object.assign({}, inp, {
    type : "DateComparator",
    _isComparatorType : true,
    _originalType : inp.type || "DateTime",
  });
};

APIGenerator.generateArrayType = function generateArrayType(inp){
  return Object.assign({
    _originalName : inp.name
  }, inp, {
    type : `[${inp.type}]`,
    _isArrayType : true,
    _originalType : inp.type,
  });
};

APIGenerator.generateArrayTypeName = function generateArrayTypeName(originalName){
  return `${originalName}s`;
};

APIGenerator.generateCustomSearchType = function generateCustomSearchType(inp){
  return Object.assign({
    queryFragment : customSearchTypes[inp.type],
    _isCustomSearchType : true
  }, inp);
};



APIGenerator.makeComparatorMapping = function makeComparatorMapping(type, arg){
  switch(type){
    case "DateComparator" : 
      return {
        eq : arg.equals,
        lt : arg.isBefore,
        gt : arg.isAfter,
        lte : arg.isSameOrBefore,
        gte : arg.isSameOrAfter,
      };

    case "NumberComparator" :
    default :
      return {
        eq : arg.equals,
        lt : arg.isLessThan,
        gt : arg.isGreaterThan,
        lte : arg.isEqualOrLessThan,
        gte : arg.isEqualOrGreaterThan,
      };
  }
};

APIGenerator.isNumberType = function isNumberType(type){
  return type === "Int" || type === "Float";
};

APIGenerator.isDateType = function isDateType(type){
  return type === "Date" || type === "DateTime" || type === "DateTime2";
};

APIGenerator.isComparator = function isComparator(type){
  return APIGenerator.isDateComparatorType(type) || APIGenerator.isNumberComparatorType(type);
};
APIGenerator.isDateComparatorType = function isDateComparatorType(type){
  return type === "DateComparator";
};
APIGenerator.isNumberComparatorType = function isNumberComparatorType(type){
  return type === "NumberComparator";
};

APIGenerator.isArrayType = function isArrayType(type){
  return /^\[.+\]$/.test(type);
};

// Check against the object of custom search types (at the bottom of the file)
APIGenerator.isCustomSearchType = function isCustomSearchType(type){
  return customSearchTypes[type];
};


APIGenerator.filterFields = function filterFields(name, possible, actual){
  return possible.filter(field => {
    let fieldName, required;
    if(Array.isArray(field)){
      [ fieldName, required ] = field;
    }else{
      fieldName = field;
    }

    let exists = (actual[fieldName] !== undefined && actual[fieldName] !== "");

    if(required && !exists){
      throw new APIInputError(`Field ${fieldName} is required`, fieldName, name);
    }else{
      return exists;
    }
  }).map(f => Array.isArray(f) ? f[0] : f);
};


APIGenerator.makeAPIFunction = function makeAPIFunction(func){
  let funcName = func.name;
  return function apiProxy(){
    return Promise.try(() => {
      return func.apply(this, arguments);
    })
    .catch(RequestError, e => {
      // catch request errors and handle here
      let m = e.message;
      let f = `${this.tableName.toLowerCase()}.${funcName}`;

      if(m.includes("FOREIGN KEY")){
        // We know it's an FK constraint issue
        let tn = m.split("table \"")[1].split("\",")[0].split(".")[1]; // LOL
        throw new APIInputError(`Invalid option selected for '${tn}'`, tn, f);
      }

      if(m.includes("UNIQUE KEY")){
        // It's a uniq constraint
        let n = m.split("IX_")[1].split("'.")[0].split("_")[1];
        let v = /\((.*)\)/.exec(m.split("The duplicate key value is")[1])[1];
        throw new APIInputError(`Duplicate entry added for '${n}', value '${v}'`, n, f);
      }

      if(m.includes("Validation failed for parameter")){
        // Failed validation, probably in a search
        let [ , n, v ] = /'(.*)'\. (.*)\./.exec(m) || [];
        n = comparatorReplacer(n);
        throw new APIInputError(`Invalid parameter '${n}', value '${v}'`, n, f);
      }

      // Default throw
      // This will be stripped out into
      // "internal server error"s in production
      throw e;
    });
  };
};

// Finds the user argument, from an arguments array
APIGenerator.findUserObject = function findUserObject(args){
  const AuthUser = require("../auth/AuthUser");
  return Array.prototype.find.call(args, arg => {
    return arg instanceof AuthUser;
  });
};




function comparatorReplacer(s){
  return s.replace("_gt", " (greater than)")
    .replace("_lt", " (less than)")
    .replace("_eq", " (equals)");
}



// Assign proxy to the APIGen builtins
APIGenerator.prototype.getAll = APIGenerator.makeAPIFunction(APIGenerator.prototype.getAll);
APIGenerator.prototype.getOne = APIGenerator.makeAPIFunction(APIGenerator.prototype.getOne);
APIGenerator.prototype.search = APIGenerator.makeAPIFunction(APIGenerator.prototype.search);


/***
 * Custom search types
 *
 */
const customSearchTypes = {
  Skill : null,
  PercentFreeAt : null
};


module.exports = APIGenerator;
