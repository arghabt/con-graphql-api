// Staff API functions
const APIGenerator = require("./APIGenerator");
const { makeAPIFunction, filterFields } = APIGenerator;
const { getTransaction, queryDB, getOne } = require("../database");
const { AuthorisationError, APIInputError } = require("./Errors.js");

const Promise = require("bluebird");

const { CustomerAccount, ECIProject } = require("./simple");
const Role = require("./Role");
const ECIProjectsReadOnTo = require("./ECIProjectsReadOnTo");
const SkillEntry = require("./SkillEntry");

const fuzzySearchFunctions = require("./fuzzySearch");

const staffAPI = new APIGenerator({
  tableName : "Staff",
  schemaName : "staff",
  columns : [
    [ "surname", "Text" ],
    [ "forename", "Text" ],
    [ "name", "Text", undefined, "CONCAT([forename], ' ', [surname])" ],
    [ "email", "Text" ],
    [ "EIN", "Int" ],
    [ "bio", "Text" ],
    [ "notes", "Text", "isResourcingManagerRaw" ],
    [ "basedAt", "FK" ],
    [ "workingAt", "FK" ],
    [ "clearanceLevel", "FK", "isResourcingManagerRaw" ],
    [ "customerTopUp", "FK", "isResourcingManagerRaw" ],
    [ "startDate", "Date" ],
    [ "endDate", "Date" ],
    [ "yearsExperiencePriorToStarting", "Int" ],
    [ "loginID", "Text" ],
    [ "startConfirmed", "Bit" ],
    [ "workingHours", "Int" ],
    [ "LV1A", "DateTime" ],
    [ "LV2A", "DateTime" ],
    [ "LV1C", "DateTime" ],
    [ "LV2C", "DateTime" ],
    [ "gradeID", "FK", "isResourcingManagerRaw" ],
    [ "SFIAGradeID", "FK", "isResourcingManagerRaw" ],
    [ "profession", "FK" ],
    [ "capability", "FK" ],
    [ "practiceArea", "FK" ],
    [ "clearanceHolderID", "FK" ],
    [ "clearanceSponsorID", "FK" ],
    [ "delegatedStaffMemberID", "FK" ]
  ]
});



let inPostSearchOptions = {
  optionName : "isInPost",
  description : "Whether a user is currently in their post",
  type : "Bit",
  queryFragment : function(is){
    if(is){
      return `(
          (${this.table}.[startDate] <= GETDATE() OR ${this.table}.[startDate] IS NULL)
        AND
          (${this.table}.[endDate] >= GETDATE() OR ${this.table}.[endDate] IS NULL)
      )`;
    }else{
      return `(${this.table}.[startDate] > GETDATE() OR ${this.table}.[endDate] < GETDATE())`;
    }
  }
};

staffAPI.addSearchOption(inPostSearchOptions);

// Alias to inactive
staffAPI.addSearchOption({
  optionName : "inactive",
  description : "Whether a user is inactive (the opposite of isInPost)",
  type : "Bit",
  queryFragment : function(is){
    return inPostSearchOptions.queryFragment.call(this, !is);
  }
});


staffAPI.addSearchOption({
  optionName : "hasLeftPost",
  description : "Whether a user has left their post",
  type : "Bit",
  queryFragment : function(has){
    if(has){
      return `(${this.table}.[endDate] IS NOT NULL AND ${this.table}.[endDate] < GETDATE())`;
    }else{
      return `(${this.table}.[endDate] IS NULL OR ${this.table}.[endDate] >= GETDATE())`;
    }
  }
});

staffAPI.addSearchOption({
  optionName : "hasNotStarted",
  description : "Whether a user has not started their role",
  type : "Bit",
  queryFragment : function(hasNot){
    if(hasNot){
      return `(${this.table}.[startDate] IS NOT NULL AND ${this.table}.[startDate] > GETDATE())`;
    }else{
      return `(${this.table}.[startDate] IS NULL OR ${this.table}.[startDate] <= GETDATE())`;
    }
  }
});

staffAPI.addSearchOption({
  optionName : "isInPostOn",
  description : "Whether a user is in their post on a given date",
  type : "DateTime",
  queryFragment : function(is){
    if(is){
      return `(
          (${this.table}.[startDate] <= @isInPostOn OR ${this.table}.[startDate] IS NULL)
        AND
          (${this.table}.[endDate] >= @isInPostOn OR ${this.table}.[endDate] IS NULL)
      )`;
    }else{
      return `(${this.table}.[startDate] > @isInPostOn OR ${this.table}.[endDate] < @isInPostOn)`;
    }
  }
});


staffAPI.addSearchOption({
  optionName : "isCurrentlyAssigned",
  description : "Whether a user id currently assigned to a role",
  type : "Bit",
  queryFragment : function(is){
    if(is){
      return `EXISTS (SELECT 1 AS E FROM [projects].[Assignments]
        INNER JOIN [projects].[Roles] ON [projects].[Assignments].[roleID] = [projects].[Roles].[ID]
        WHERE
          [projects].[Assignments].[staffID] = ${this.table}.[ID] AND
          [projects].[Roles].[startDate] <= GETUTCDATE() AND
          [projects].[Roles].[endDate] >= GETUTCDATE()
        )`;
    }else{
      return `NOT EXISTS (SELECT 1 AS E FROM [projects].[Assignments]
        INNER JOIN [projects].[Roles] ON [projects].[Assignments].[roleID] = [projects].[Roles].[ID]
        WHERE
          [projects].[Assignments].[staffID] = ${this.table}.[ID] AND
          [projects].[Roles].[startDate] <= GETUTCDATE() AND
          [projects].[Roles].[endDate] >= GETUTCDATE()
        )`;
    }
  }
});

staffAPI.addSearchOption({
  optionName : "hasBeenAssigned",
  description : "Whether a user has been assigned a role at any point",
  type : "Bit",
  queryFragment : function(has){
    if(has){
      return `EXISTS (SELECT 1 AS E FROM [projects].[Assignments]
        WHERE [projects].[Assignments].[staffID] = ${this.table}.[ID])`;
    }else{
      return `NOT EXISTS (SELECT 1 AS E FROM [projects].[Assignments]
        WHERE [projects].[Assignments].[staffID] = ${this.table}.[ID])`;
    }
  }
});


staffAPI.addSearchOption({
  optionName : "isOnTheBenchAt",
  description : "Whether a user is on the bench (is not assigned to unpaid work) at a given DateTime",
  type : "DateTime",
  queryFragment : function(){
    return `NOT EXISTS (SELECT 1 AS E FROM [projects].[Assignments]
      INNER JOIN [projects].[Roles] ON [projects].[Assignments].[roleID] = [projects].[Roles].[ID]
      INNER JOIN [projects].[Projects] ON [projects].[Roles].[projectID] = [projects].[Projects].[ID]
      LEFT JOIN [core].[FundingTypes] ON [projects].[Projects].[fundingTypeID] = [core].[FundingTypes].[ID]
      WHERE
        [projects].[Assignments].[staffID] = ${this.table}.[ID]
      AND
        -- Get the right time
        ([projects].[Roles].[startDate] <= @isOnTheBenchAt AND [projects].[Roles].[endDate] >= @isOnTheBenchAt)
      AND
        -- Filter out unpaid work
        (
          ([core].[FundingTypes].[name] != 'Investment' AND [core].[FundingTypes].[name] != 'Overhead')
            OR
          [projects].[Projects].[fundingTypeID] IS NULL
        )
      HAVING SUM([projects].[Roles].[workload]) >= 100
    )
    -- Filter staff members that have not started or have finished
    AND (${this.table}.startDate IS NULL OR ${this.table}.startDate <= @isOnTheBenchAt)
    AND (${this.table}.endDate IS NULL OR ${this.table}.endDate > @isOnTheBenchAt)`;
  }
});


staffAPI.addSearchOption({
  optionName : "hasPercentFreeAt",
  description : "Whether a user a percentage of their time free at a given DateTime",
  type : "PercentFreeAt",
  queryFragment : function(){
    return `NOT EXISTS (SELECT 1 AS E FROM [projects].[Assignments]
      INNER JOIN [projects].[Roles] ON [projects].[Assignments].[roleID] = [projects].[Roles].[ID]
      INNER JOIN [projects].[Projects] ON [projects].[Roles].[projectID] = [projects].[Projects].[ID]
      LEFT JOIN [core].[FundingTypes] ON [projects].[Projects].[fundingTypeID] = [core].[FundingTypes].[ID]
      WHERE
        [projects].[Assignments].[staffID] = ${this.table}.[ID]
      AND
        -- Get the right time
        ([projects].[Roles].[startDate] <= @hasPercentFreeAt_at AND [projects].[Roles].[endDate] >= @hasPercentFreeAt_at)
      AND
        -- Filter out unpaid work
        (
          ([core].[FundingTypes].[name] != 'Investment' AND [core].[FundingTypes].[name] != 'Overhead')
            OR
          [projects].[Projects].[fundingTypeID] IS NULL
        )
      -- Check the workload reaches the threshold
      HAVING SUM([projects].[Roles].[workload]) > (100 - @hasPercentFreeAt_percentFree)
    )
    -- Filter staff members that have not started or have finished
    AND (${this.table}.startDate IS NULL OR ${this.table}.startDate <= @hasPercentFreeAt_at)
    AND (${this.table}.endDate IS NULL OR ${this.table}.endDate > @hasPercentFreeAt_at)`;
  }
});


staffAPI.addSearchOption({
  optionName : "hasAtLeastTheseSkills",
  description : "Whether a user has at least the given skills",
  type : "[Skill]",
  queryFragment : function(skills){
    if(skills.length === 0){
      return null;
    }

    return `(
      SELECT COUNT(1) FROM [Skills].[SkillEntries]
        WHERE [Skills].[SkillEntries].[skillLevel] > 0
        AND [Skills].[SkillEntries].[staffID] = ${this.table}.[ID]
        AND (
          ${skills.map((_, i) => `(
            [Skills].[SkillEntries].[skillID] = @hasAtLeastTheseSkills_${i}_skillID AND
            [Skills].[SkillEntries].[skillLevel] >= @hasAtLeastTheseSkills_${i}_skillLevel
          )`).join(" OR ")}
        )
      ) = ${skills.length}`;
  }
});


staffAPI.addSearchOption({
  optionName : "isAProjectManager",
  description : "Whether a user is a project manager",
  type : "Bit",
  queryFragment : function(is){
    if(is){
      return `EXISTS (SELECT 1 AS E FROM [projects].[Projects]
        WHERE
          [projects].[Projects].[projectManagerID] = ${this.table}.[ID]
        )`;
    }else{
      return `NOT EXISTS (SELECT 1 AS E FROM [projects].[Projects]
        WHERE
          [projects].[Projects].[projectManagerID] = ${this.table}.[ID]
        )`;
    }
  }
});


staffAPI.addSearchOption({
  optionName : "isATechLead",
  description : "Whether a user is a technical lead",
  type : "Bit",
  queryFragment : function(is){
    if(is){
      return `EXISTS (SELECT 1 AS E FROM [projects].[Projects]
        WHERE
          [projects].[Projects].[techLeadID] = ${this.table}.[ID]
        )`;
    }else{
      return `NOT EXISTS (SELECT 1 AS E FROM [projects].[Projects]
        WHERE
          [projects].[Projects].[techLeadID] = ${this.table}.[ID]
        )`;
    }
  }
});




// Add "name" to search functionaility
/*staffAPI.addSearchOption({
  optionName : "name",
  type : "Text",
  queryFragment : function(_){
    return `CONCAT(${this.table}.[forename], ' ', ${this.table}.[surname]) LIKE ('%' + @name + '%')`;
  }
});*/

// Experimantal!! Add the name to the list
// staffAPI.sqlColList += (`,CONCAT(${staffAPI.table}.[forename], ' ', ${staffAPI.table}.[surname]) AS name`);
// staffAPI.sqlColList += (`,IIF(${staffAPI.table}.[startDate] > GETDATE() OR ${staffAPI.table}.[endDate] < GETDATE(), 1, 0) AS inactive`);
// staffAPI.simpleSelect = `SELECT ${staffAPI.sqlColList}
//   FROM ${staffAPI.table}`;

staffAPI.getCustomerAccountsForStaff = makeAPIFunction(function getCustomerAccountsForStaff({ staffID }, user){
  let query = CustomerAccount.getSimpleSelect(user) + `
    INNER JOIN [staff].[Staff_CustomerAccounts] SCA ON SCA.customerAccountID = ${CustomerAccount.table}.[ID]
    WHERE SCA.staffID = @staffID`;

  return queryDB(query, [ "staffID", "Int", staffID ]);
});

staffAPI.getECIProjectsForStaff = makeAPIFunction(function getECIProjectsForStaff({ staffID }, user){
  let query = ECIProject.getSimpleSelect(user) + `
    INNER JOIN [staff].[Staff_ECIProjectsReadOnTo] ECI ON ECI.ECIProjectID = ${ECIProject.table}.[ID]
    WHERE ECI.staffID = @staffID`;

  return queryDB(query, [ "staffID", "Int", staffID ]);
});

staffAPI.getRolesAssignedToStaff = makeAPIFunction(function getRolesAssignedToStaff({ staffID, currentRoles }, user){
  return Role.search({
    isAssignedTo : staffID,
    isCurrent : currentRoles
  }, user);
});


// Adds the full name to a staff obj and returns it
staffAPI.addFullName = makeAPIFunction(function addFullName(staffObj){
  staffObj.name = `${staffObj.forename} ${staffObj.surname}`;
  return staffObj;
});


staffAPI.getRoleID = makeAPIFunction(function getRoleID({ staffID }, user){
  let query = `SELECT [staff].[Admins].[role]
    FROM [staff].[Admins]
    WHERE [staff].[Admins].[userID] = @staffID`;

  return getOne(query, [ "staffID", "Int", staffID ]).then(a => (a && a.role) || 0);
});

staffAPI.getRole = makeAPIFunction(function getRole({ staffID }, user){
  let query = `SELECT * FROM [staff].[AdminRoles]
    INNER JOIN [staff].[Admins] ON [staff].[AdminRoles].[ID] = [staff].[Admins].[role]
    WHERE [staff].[Admins].[userID] = @staffID`;

  return getOne(query, [ "staffID", "Int", staffID ]);
});

staffAPI.isAProjectManager = makeAPIFunction(function isAProjectManager({ staffID }, user){
  return user.isAProjectManager.call({ ID : staffID }); // Lets hook off the back of this function
});


staffAPI.getByLoginID = makeAPIFunction(function getByLoginID({ loginID }, user){
  let query = `${this.getSimpleSelect(user)} WHERE [loginID] = @loginID`;

  return getOne(query, [ "loginID", undefined, loginID ]);
});



// Fuzzy searching
const FUZZY_SEARCH_SUPPORTED_FIELDS = [
  "basedAt",
  "basedAts",
  "clearanceLevel",
  "clearanceLevels",
  "clearanceHolderID",
  "clearanceHolderIDs",
  "clearanceSponsorID",
  "clearanceSponsorIDs",
  "workingAt",
  "workingAts",
  "customerTopUp",
  "customerTopUps",
  "startConfirmed",
  "gradeID",
  "gradeIDs",
  "SFIAGradeID",
  "SFIAGradeIDs",
  "profession",
  "professions",
  "capability",
  "capabilitys",
  "practiceArea",
  "practiceAreas",
  "hasAtLeastTheseSkills"
];

// Fields to intercept and change but pass on as a strict search
const STRICT_FUZZY_FIELDS_VALUES = {
  hasPercentFreeAt: {
    percentFree: 0
  }
};

staffAPI.fuzzySearch = makeAPIFunction(function fuzzySearch(input, user){
  // Get the criteria that will be searched strictly
  // These are criteria that are not in FUZZY_SEARCH_SUPPORTED_FIELDS
  let strictSearchCriteria = {};
  for(let fieldName in input){
    if(
      input[fieldName] !== undefined &&
      !FUZZY_SEARCH_SUPPORTED_FIELDS.includes(fieldName) &&
      STRICT_FUZZY_FIELDS_VALUES[fieldName] === undefined
    ){
      strictSearchCriteria[fieldName] = input[fieldName];
    }
  }

  // Merge the fields that are intercepted and values changed when they are searched for
  let strictFuzzySearchCriteria = {};
  for(let fieldName in STRICT_FUZZY_FIELDS_VALUES){
    if(input[fieldName]){
      strictFuzzySearchCriteria[fieldName] = Object.assign({}, input[fieldName], STRICT_FUZZY_FIELDS_VALUES[fieldName]);
    }
  }

  // Merged search criteria between strict and strict fuzzy searches
  let mergedSearchCriteria = Object.assign({}, strictSearchCriteria, strictFuzzySearchCriteria);

  return Promise.join(
    this.search(mergedSearchCriteria, user),
    SkillEntry.getAll({ orderBy : { field : "ID", direction : "ASC" }}, user),
    (staffMembers, skillEntries) => {
      // Matching up skill entries to staffMembers
      staffMembers.forEach(staffMember => {
        staffMember.skills = skillEntries.filter(skillEntry => skillEntry.staffID === staffMember.ID);
      });

      let fuzzySearchResults = fuzzySearchFunctions.performFuzzySearch(input, staffMembers, user, this.getSearchOptions(), FUZZY_SEARCH_SUPPORTED_FIELDS);
      return fuzzySearchResults;
    });
});

staffAPI.create = makeAPIFunction(function create({ staffMember }, user){
  return user.canCreateStaffMembers()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "staff.create");
      }
    })
    .then(() => {
      // Trim whitespace from project name
      staffMember.forename = staffMember.forename && staffMember.forename.trim();
      staffMember.surname = staffMember.surname && staffMember.surname.trim();

      let remainingFields = filterFields("staff.create", [
        [ "surname", true ],
        [ "forename", true ],
        [ "email", true ],
        [ "EIN" ],
        [ "bio" ],
        [ "notes" ],
        [ "basedAt", true ],
        [ "workingAt", true ],
        [ "clearanceLevel", true ],
        [ "customerTopUp" ],
        [ "startDate" ],
        [ "endDate" ],
        [ "yearsExperiencePriorToStarting" ],
        [ "loginID" ],
        [ "startConfirmed" ],
        [ "workingHours" ],
        [ "LV1A" ],
        [ "LV2A" ],
        [ "LV1C" ],
        [ "LV2C" ],
        [ "gradeID" ],
        [ "SFIAGradeID" ],
        [ "profession" ],
        [ "capability" ],
        [ "practiceArea" ],
        [ "clearanceHolderID" ],
        [ "clearanceSponsorID" ],
        [ "delegatedStaffMemberID" ]
      ], staffMember);


      // Start transaction
      return getTransaction()
        .then(transaction => {
          // Construct query
          let query = `INSERT INTO ${this.table}
            (${remainingFields.join(",")})
            VALUES (${remainingFields.map(field => `@${field}`).join(",")});
            SELECT SCOPE_IDENTITY() AS new_id;`;

          let params = remainingFields.map(field => [
            field,
            undefined,
            staffMember[field]
          ]);

          return getOne(query, params, transaction)
            .then(({ new_id }) => {
              // Make customer accounts
              return Promise.resolve(Array.isArray(staffMember.customerAccountIDs) &&
                (staffMember.customerAccountIDs.length > 0) &&
                addCustomerAccountsToStaff(new_id, staffMember.customerAccountIDs, transaction))

                // Add the ECIProjects
                .then(() => {
                  return ECIProjectsReadOnTo.createMany({
                    staffID : new_id,
                    ECIProjectsReadOnTo : staffMember.ECIProjectsReadOnTo
                  }, user, transaction);
                })

                // Add skill entries
                .then(() => {
                  return SkillEntry.createMany({
                    staffID : new_id,
                    skillEntries : staffMember.skillEntries
                  }, user, transaction);
                })

                // Commit
                .then(() => {
                  return transaction.commit();
                })
                // Get the new project ID
                .return({ ID : new_id });
            })
            .catch((e) => {
              // Transaction rollback
              return transaction.rollback().throw(e);
            });
        });
    });
});




staffAPI.edit = makeAPIFunction(function edit({ staffID, staffMember }, user){
  return user.canEditStaffMember(staffID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "staff.edit");
      }
    })
    .then(() => {

      let remainingFields = filterFields("staff.edit", [
        "surname",
        "forename",
        "email",
        "EIN",
        "bio",
        "notes",
        "basedAt",
        "workingAt",
        "clearanceLevel",
        "customerTopUp",
        "startDate",
        "endDate",
        "yearsExperiencePriorToStarting",
        "loginID",
        "startConfirmed",
        "workingHours",
        "LV1A",
        "LV2A",
        "LV1C",
        "LV2C",
        "gradeID",
        "SFIAGradeID",
        "profession",
        "capability",
        "practiceArea",
        "clearanceHolderID",
        "clearanceSponsorID",
        "delegatedStaffMemberID"
      ], staffMember);


      // Start transaction
      return getTransaction()
        .then(transaction => {
          let prom;
          if(remainingFields.length > 0){
            // Construct query
            let query = `UPDATE ${this.table} SET
                ${remainingFields.map(field => {
                  return `${field} = @${field}`;
                }).join(",")}
                WHERE [ID] = @staffID;`;


            let params = remainingFields.map(field => [
              field,
              undefined,
              staffMember[field]
            ]);

            // Push on the staff ID
            params.push([
              "staffID",
              undefined,
              staffID
            ]);

            prom = queryDB(query, params, transaction);
          }else{
            prom = Promise.resolve();
          }

          return prom
            .then(() => {
              // Add customer accounts
              return Array.isArray(staffMember.customerAccountIDs) && addCustomerAccountsToStaff(staffID, staffMember.customerAccountIDs, transaction);
            })

            // Delete old ECIProjects
            .then(() => {
              if(Array.isArray(staffMember.ECIProjectsReadOnTo)){
                return ECIProjectsReadOnTo.delAllForStaffMember({ staffID }, user, transaction)
                  // Add the ECIProjects
                  .then(() => {
                    return ECIProjectsReadOnTo.createMany({
                      staffID,
                      ECIProjectsReadOnTo : staffMember.ECIProjectsReadOnTo
                    }, user, transaction);
                  });
              }
            })


            // Add skills...
            .then(() => {
              // Use overwrite skills
              return Array.isArray(staffMember.skillEntries) && SkillEntry.overwriteSkills({ staffID, skillEntries : staffMember.skillEntries }, user, transaction);
            })


            .then(() => {
              // Commit
              return transaction.commit();
            })
            // Get the old staffID
            .return({ ID : staffID })

            .catch((e) => {
              // Transaction rollback
              return transaction.rollback().throw(e);
            });
        });
    });
});


staffAPI.editSelf = makeAPIFunction(function editSelf({ staffMember }, user){
  if(!user.isLoggedIn()){
    throw new AuthorisationError("You do not have permission to do that", "staff.editSelf");
  }

  // Filter to a reduced set, then pass into the normal staff member function
  let remainingFields = filterFields("staff.editSelf", [
    "email",
    "EIN",
    "bio",
    "basedAt",
    "workingAt",
    "clearanceLevel",
    "startDate",
    "yearsExperiencePriorToStarting",
    "workingHours",
    "gradeID",
    "SFIAGradeID",
    "profession",
    "capability",
    "practiceArea"
  ], staffMember);


  let staffID = user.getID();
  let filteredStaffMember = {};
  remainingFields.forEach(f => {
    filteredStaffMember[f] = staffMember[f];
  });

  // Add on customer accounts
  filteredStaffMember.customerAccountIDs = staffMember.customerAccountIDs;

  return this.edit({ staffID, staffMember : filteredStaffMember }, require("../auth/AuthUser").su);
});



// Edit bio function
// Not an API functions
function doEditBio(staffID, bio){
  // Set bio to blank if undef
  bio = bio || "";

  let query = `UPDATE ${staffAPI.table} SET
    bio = @bio
    WHERE [ID] = @staffID`;

  return queryDB(query, [
    [ "staffID", "Int", staffID ],
    [ "bio", "Text", bio ]
  ])
  .return({ ID : staffID });
}


staffAPI.editBio = makeAPIFunction(function editBio({ staffID, bio }, user){
  return user.canEditStaffBio(staffID)
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "staff.editBio");
      }
    })
    .then(() => doEditBio(staffID, bio));
});

staffAPI.editMyBio = makeAPIFunction(function editMyBio({ bio }, user){
  if(!user.isLoggedIn()){
    throw new AuthorisationError("You do not have permission to do that", "staff.editMyBio");
  }

  return doEditBio(user.getID(), bio);
});


// Change a user's role
staffAPI.editUserRole = makeAPIFunction(function editUserRole({ staffID, roleID }, user){
  return user.isAdmin()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "staff.editUserRole");
      }
    })
    .then(() => {
      if(roleID < 0 || roleID > 2){
        throw new APIInputError("Role must be between 0 and 2 inclusive", "roleID", "staff.editUserRole");
      }

      if(roleID === 0){
        // This case we can just delete the row from Admins,
        // as it doesn't matter if they have an entry already or not
        let query = `DELETE FROM [staff].[Admins]
          WHERE [userID] = @staffID`;

        return queryDB(query, [
          [ "staffID", "Int", staffID ]
        ]);
      }

      // Else we need to know if the use has an entry already
      let query = `SELECT [role] FROM [staff].[Admins]
        WHERE [userID] = @staffID`;

      return queryDB(query, [
        [ "staffID", "Int", staffID ]
      ])
        .then(results => {
          let query;
          if(results.length > 0){
            // If there is a result, update the entry
            query = `UPDATE [staff].[Admins] SET
              [role] = @roleID
              WHERE [userID] = @staffID`;
          }else{
            // Else, insert
            query = `INSERT INTO [staff].[Admins] ([userID], [role])
              VALUES (@staffID, @roleID)`;
          }

          return queryDB(query, [
            [ "roleID", "Int", roleID ],
            [ "staffID", "Int", staffID ],
          ]);
        });
    })
    .return({ ID : staffID });
});





function addCustomerAccountsToStaff(staffID, customerAccountIDs, transaction){
  // Delete all and then remake the list
  let query = `DELETE FROM [staff].[Staff_CustomerAccounts]
    WHERE staffID = @staffID`;

  return queryDB(query, [ "staffID", "Int", staffID ], transaction)
    .then(() => {
      let query = `INSERT INTO [staff].[Staff_CustomerAccounts]
          ([customerAccountID], [staffID])
          VALUES (@customerAccountID, @staffID)`;

      return Promise.reduce(customerAccountIDs || [], (_, customerAccountID) => {
        return queryDB(query, [
          [ "customerAccountID", "Int", customerAccountID ],
          [ "staffID", "Int", staffID ]
        ], transaction);
      }, null);
    });
}



module.exports = staffAPI;
