// ECIProjects Read on to functions
const Promise = require("bluebird");

const APIGenerator = require("./APIGenerator");
const { makeAPIFunction, filterFields } = APIGenerator;
const { AuthorisationError, APIInputError } = require("./Errors.js");
const { getTransaction, queryDB } = require("../database");

const eciProjectsReadOnToAPI = new APIGenerator({
  tableName : "Staff_ECIProjectsReadOnTo",
  schemaName : "staff",
  includeID : false,
  columns : [
    [ "staffID", "FK" ],
    [ "ECIProjectID", "FK" ],
    [ "readOnDate", "Date" ]
  ]
});



eciProjectsReadOnToAPI.create = makeAPIFunction(function create({ staffID, ECIProjectReadOnTo }, user, higherTransaction){
  return user.canCreateClearanceApplications()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "eciProjectsReadOnToAPI.create");
      }
    })
    .then(() => {
      ECIProjectReadOnTo = Object.assign({}, ECIProjectReadOnTo, { staffID : staffID });

      let remainingFields = filterFields("eciProjectsReadOnToAPI.create", [
        [ "staffID", true ],
        [ "ECIProjectID", true ],
        [ "readOnDate", true ],
      ], ECIProjectReadOnTo);


      let query = `INSERT INTO ${this.table}
        (${remainingFields.join(",")})
        VALUES (${remainingFields.map(field => `@${field}`).join(",")})`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        ECIProjectReadOnTo[field]
      ]);

      return queryDB(query, params, higherTransaction)
        .return({
          staffID : ECIProjectReadOnTo.staffID,
          ECIProjectID : ECIProjectReadOnTo.ECIProjectID
        });
    });
});

eciProjectsReadOnToAPI.createMany = makeAPIFunction(function createMany({ staffID, ECIProjectsReadOnTo }, user, higherTransaction){
  if(!staffID){
    throw new APIInputError("staffID is required", "staffID", "eciProjectsReadOnToAPI.createMany");
  }
  return user.canCreateClearanceApplications()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "eciProjectsReadOnToAPI.createMany");
      }
    })
    .then(() => {
      if(!Array.isArray(ECIProjectsReadOnTo) || ECIProjectsReadOnTo.length === 0){
        return [];
      }

      let transactionPromise = Promise.resolve(higherTransaction || getTransaction());
      return transactionPromise.then(transaction => {
        return Promise.map(ECIProjectsReadOnTo, (ECIProjectReadOnTo) => {
          return eciProjectsReadOnToAPI.create({
            staffID,
            ECIProjectReadOnTo
          }, user, transaction);
        }, { concurrency : 1 })
        .tap(() => !higherTransaction && transaction.commit())
        .tapCatch(() => !higherTransaction && transaction.rollback());
      });
    });
});


eciProjectsReadOnToAPI.del = makeAPIFunction(function del({ staffID, ECIProjectID }, user, higherTransaction){
  if(!staffID){
    throw new APIInputError("staffID is required", "staffID", "eciProjectsReadOnToAPI.del");
  }
  if(!ECIProjectID){
    throw new APIInputError("ECIProjectID is required", "ECIProjectID", "eciProjectsReadOnToAPI.del");
  }

  return user.canCreateClearanceApplications()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "eciProjectsReadOnToAPI.del");
      }
    })
    .then(() => {
      let query = `DELETE FROM ${this.table}
        WHERE [staffID] = @staffID AND [ECIProjectID] = @ECIProjectID`;

      let params = [
        [ "staffID", "Int", staffID ],
        [ "ECIProjectID", "Int", ECIProjectID ]
      ];

      return queryDB(query, params, higherTransaction).return(true);
    });
});

eciProjectsReadOnToAPI.delMany = makeAPIFunction(function delMany({ staffID, ECIProjectIDs }, user, higherTransaction){
  return user.canCreateClearanceApplications()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "eciProjectsReadOnToAPI.delMany");
      }
    })
    .then(() => {
      if(!Array.isArray(ECIProjectIDs) || ECIProjectIDs.length === 0){
        return true;
      }

      let transactionPromise = Promise.resolve(higherTransaction || getTransaction());
      return transactionPromise.then(transaction => {
        return Promise.map(ECIProjectIDs, (ECIProjectID) => {
          return eciProjectsReadOnToAPI.del({
            staffID,
            ECIProjectID
          }, user, transaction);
        }, { concurrency : 1 })
        .tap(() => !higherTransaction && transaction.commit())
        .tapCatch(() => !higherTransaction && transaction.rollback())
        .return(true);
      });
    });
});

eciProjectsReadOnToAPI.delAllForStaffMember = makeAPIFunction(function delAllForStaffMember({ staffID }, user, higherTransaction){
  if(!staffID){
    throw new APIInputError("staffID is required", "staffID", "eciProjectsReadOnToAPI.delAllForStaffMember");
  }
  return user.canCreateClearanceApplications()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "eciProjectsReadOnToAPI.delAllForStaffMember");
      }
    })
    .then(() => {
      let query = `DELETE FROM ${this.table}
        WHERE [staffID] = @staffID`;

      let params = [
        [ "staffID", "Int", staffID ]
      ];

      return queryDB(query, params, higherTransaction).return(true);
    });
});


module.exports = eciProjectsReadOnToAPI;
