// Clearance Applications API functions

const APIGenerator = require("./APIGenerator");
const { makeAPIFunction, filterFields } = APIGenerator;

const { queryDB, getOne } = require("../database");
const { AuthorisationError } = require("./Errors.js");


const clearanceApplication = new APIGenerator({
  tableName : "ClearanceApplications",
  schemaName : "staff",
  columns : [
    [ "staffID", "FK" ],
    [ "clearanceReferenceNumber", "Text" ],
    [ "clearanceAppliedDate", "Date" ],
    [ "clearanceIssuedDate", "Date" ],
    [ "clearanceExpiryDate", "Date" ],
    [ "STRAPReadOnDate", "Date" ],
    [ "clearanceLevelID", "FK" ],
    [ "clearanceSponsorID", "FK" ],
    [ "clearanceSponsorProjectID", "FK" ],
    [ "privilegedUserChecked", "Bit" ],
    [ "STRAPWaiverRequired", "Bit" ],
    [ "STRAPWaiverApprovalDate", "Date" ],
    [ "clearanceTransferStatus", "Text" ],
    [ "clearanceNotes", "Text" ],
  ]
});



clearanceApplication.addInputChanger("search", function(args, user){
  // If there is a search via staff ID, we can skip the SQL
  // if the user does not have enough permissions
  let staffID = args.staffID;
  if(staffID){
    return user.canSeeClearanceApplication(staffID).then(can => can ? args : null);
  }
  return args;
});

clearanceApplication.addSQLManipulator([ "search", "getAll", "getOne" ], function(query, args, user){
  return user.canCreateClearanceApplications()
    .then(can => {
      if(can){
        return query;
      }

      // Add a clause to only get results for this user
      return query + ` AND staffID = ${user.getID()}`;
    });
});



clearanceApplication.getLatestAppliedApplicationForStaffMember = makeAPIFunction(function getLatestAppliedApplicationForStaffMember({ staffID }, user){
  return user.canSeeClearanceApplication(staffID)
    .then(allowed => {
      if(!allowed){
        return null;
      }

      let query = `SELECT ${this.getSQLSelectFieldsString(user)} FROM [staff].[LatestAppliedClearanceApplications] WHERE staffID = @staffID`;

      return getOne(query, [ "staffID", "Int", staffID ]);
    });
});

clearanceApplication.getLatestIssuedApplicationForStaffMember = makeAPIFunction(function getLatestIssuedApplicationForStaffMember({ staffID }, user){
  return user.canSeeClearanceApplication(staffID)
    .then(allowed => {
      if(!allowed){
        return null;
      }

      let query = `SELECT ${this.getSQLSelectFieldsString(user)} FROM [staff].[LatestIssuedClearanceApplications] WHERE staffID = @staffID`;

      return getOne(query, [ "staffID", "Int", staffID ]);
    });
});

clearanceApplication.getLatestExpiredApplicationForStaffMember = makeAPIFunction(function getLatestExpiredApplicationForStaffMember({ staffID }, user){
  return user.canSeeClearanceApplication(staffID)
    .then(allowed => {
      if(!allowed){
        return null;
      }

      let query = `SELECT ${this.getSQLSelectFieldsString(user)} FROM [staff].[LatestExpiredClearanceApplications] WHERE staffID = @staffID`;

      return getOne(query, [ "staffID", "Int", staffID ]);
    });
});


// This function will be useful when clearanceLevel is removed from staff
clearanceApplication.getLatestClearanceLevelForStaffMember = makeAPIFunction(function getLatestClearanceLevelForStaffMember({ staffID }, user){
  let query = `SELECT * FROM [core].[Clearance] C
    INNER JOIN [staff].[LatestIssuedClearanceApplications] A ON A.[clearanceLevelID] = C.[ID]
    WHERE A.staffID = @staffID`;

  return getOne(query, [ "staffID", "Int", staffID ]);
});



clearanceApplication.create = makeAPIFunction(function create({ clearanceApplication, staffID }, user){
  return user.canCreateClearanceApplications()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "clearanceApplication.create");
      }
    })
    .then(() => {
      clearanceApplication.staffID = staffID;

      let remainingFields = filterFields("clearanceApplication.create", [
        [ "staffID", true ],
        [ "clearanceReferenceNumber" ],
        [ "clearanceAppliedDate" ],
        [ "clearanceIssuedDate" ],
        [ "clearanceExpiryDate" ],
        [ "STRAPReadOnDate" ],
        [ "clearanceLevelID", true ],
        [ "clearanceSponsorID" ],
        [ "clearanceSponsorProjectID" ],
        [ "privilegedUserChecked" ],
        [ "STRAPWaiverRequired" ],
        [ "STRAPWaiverApprovalDate" ],
        [ "clearanceTransferStatus" ],
        [ "clearanceNotes" ],
      ], clearanceApplication);

      // Construct query
      let query = `INSERT INTO ${this.table}
        (${remainingFields.join(",")})
        VALUES (${remainingFields.map(field => `@${field}`).join(",")});
        SELECT SCOPE_IDENTITY() AS ID;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        clearanceApplication[field]
      ]);

      return getOne(query, params);
    });
});


clearanceApplication.edit = makeAPIFunction(function edit({ clearanceApplicationID, clearanceApplication }, user){
  return user.canCreateClearanceApplications()
    .tap(allowed => {
      if(!allowed){
        throw new AuthorisationError("You do not have permission to do that", "clearanceApplication.edit");
      }
    })
    .then(() => {
      let remainingFields = filterFields("clearanceApplication.edit", [
        "clearanceReferenceNumber",
        "clearanceAppliedDate",
        "clearanceIssuedDate",
        "clearanceExpiryDate",
        "STRAPReadOnDate",
        "clearanceLevelID",
        "clearanceSponsorID",
        "clearanceSponsorProjectID",
        "privilegedUserChecked",
        "STRAPWaiverRequired",
        "STRAPWaiverApprovalDate",
        "clearanceTransferStatus",
        "clearanceNotes",
      ], clearanceApplication);

      // Construct query
      let query = `UPDATE ${this.table} SET
          ${remainingFields.map(field => {
            return `${field} = @${field}`;
          }).join(",")}
          WHERE [ID] = @clearanceApplicationID;`;

      let params = remainingFields.map(field => [
        field,
        undefined,
        clearanceApplication[field]
      ]);

      params.push([
        "clearanceApplicationID",
        undefined,
        clearanceApplicationID
      ]);

      return queryDB(query, params).return({ ID : clearanceApplicationID });
    });
});



module.exports = clearanceApplication;
