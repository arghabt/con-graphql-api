// Role functions
const {
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");
const { RoleInputType } = require("../types");

let rolesQL = graphQLFunctionsGenerator("Role");


rolesQL.functions.mutations.create = {
  type : rolesQL.type,

  args : {
    projectID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    role : {
      type : new GraphQLNonNull(RoleInputType)
    }
  },

  resolve(_, args, { user }){
    return rolesQL.apiFunctions.create(args, user)
      .then((r) => rolesQL.apiFunctions.getOne(r, user));
  }
};

rolesQL.functions.mutations.createMany = {
  type : rolesQL.type,

  args : {
    projectID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    roles : {
      type : new GraphQLNonNull(new GraphQLList(RoleInputType))
    }
  },

  resolve(_, args, { user }){
    return rolesQL.apiFunctions.createMany(args, user)
      .map((r) => rolesQL.apiFunctions.getOne(r, user));
  }
};

rolesQL.functions.mutations.duplicate = {
  type : rolesQL.type,

  args : {
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    changes : {
      type : RoleInputType
    }
  },

  resolve(_, args, { user }){
    return rolesQL.apiFunctions.duplicate(args, user)
      .then((r) => rolesQL.apiFunctions.getOne(r, user));
  }
};


module.exports = rolesQL.functions;
