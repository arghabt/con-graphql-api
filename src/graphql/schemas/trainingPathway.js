
const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");
const mutationFunctionGenerator = require("./simple/mutationFunctionGenerator");

let TrainingPathwayQL = graphQLFunctionsGenerator("TrainingPathway");
mutationFunctionGenerator(TrainingPathwayQL);

module.exports = TrainingPathwayQL.functions;
