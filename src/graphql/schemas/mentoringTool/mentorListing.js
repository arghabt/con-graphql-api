// Mentor listing functions
const {
  GraphQLBoolean,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const graphQLFunctionsGenerator = require("../graphQLFunctionsGenerator");
const { MentorMenteeListingInputType } = require("../../types");

let mentorListing = graphQLFunctionsGenerator("MentorListing");

mentorListing.functions.mutations.create = {
  type: mentorListing.type,

  args: {
    mentorListing: {
      type: new GraphQLNonNull(MentorMenteeListingInputType)
    }
  },
  resolve(_, args, { user }){
    return mentorListing.apiFunctions.create(args, user)
      .then((r) => mentorListing.apiFunctions.getOne(r, user));
  }
};

mentorListing.functions.mutations.edit = {
  type: mentorListing.type,

  args: {
    mentorListingID: {
      type: new GraphQLNonNull(GraphQLInt)
    },
    mentorListing: {
      type: new GraphQLNonNull(MentorMenteeListingInputType)
    }
  },
  resolve(_, args, { user }) {
    return mentorListing.apiFunctions.edit(args, user)
      .then((r) => mentorListing.apiFunctions.getOne(r, user));
  }
};


mentorListing.functions.mutations.del = {
  type: GraphQLBoolean,

  args: {
    mentorListingID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }) {
    return mentorListing.apiFunctions.del(args, user).return(true);
  }
};

module.exports = mentorListing.functions;
