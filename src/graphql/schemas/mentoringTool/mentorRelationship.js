// Mentor relationship functions
const {
  GraphQLBoolean,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const graphQLFunctionsGenerator = require("../graphQLFunctionsGenerator");
const { MentorRelationshipInputType } = require("../../types");

let mentorRelationship = graphQLFunctionsGenerator("MentorRelationship");

const STATUS = {
  Requested : 1,
  Seen : 2,
  Accepted : 3,
  Rejected : 4,
  Finished : 5
};

mentorRelationship.functions.mutations.create = {
  type: mentorRelationship.type,

  args: {
    mentorRelationship: {
      type: new GraphQLNonNull(MentorRelationshipInputType)
    }
  },
  resolve(_, args, { user }) {
    return mentorRelationship.apiFunctions.create(args, user)
      .then((r) => mentorRelationship.apiFunctions.getOne(r, user));
  }
};


mentorRelationship.functions.mutations.changeRelationshipStatus = {
  type: mentorRelationship.type,

  args: {
    mentorRelationshipID: {
      type: new GraphQLNonNull(GraphQLInt)
    },

    statusID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }) {
    return mentorRelationship.apiFunctions.changeRelationshipStatus(args, user)
      .then((r) => mentorRelationship.apiFunctions.getOne(r, user));
  }
};

mentorRelationship.functions.mutations.seenMentorRelationship = {
  type: mentorRelationship.type,

  args: {
    mentorRelationshipID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }) {
    args.statusID = STATUS.Seen;
    return mentorRelationship.apiFunctions.changeRelationshipStatus(args, user)
      .then((r) => mentorRelationship.apiFunctions.getOne(r, user));
  }
};

mentorRelationship.functions.mutations.acceptMentorRelationship = {
  type: mentorRelationship.type,

  args: {
    mentorRelationshipID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }) {
    args.statusID = STATUS.Accepted;
    return mentorRelationship.apiFunctions.changeRelationshipStatus(args, user)
      .then((r) => mentorRelationship.apiFunctions.getOne(r, user));
  }
};

mentorRelationship.functions.mutations.rejectMentorRelationship = {
  type: mentorRelationship.type,

  args: {
    mentorRelationshipID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }) {
    args.statusID = STATUS.Rejected;
    return mentorRelationship.apiFunctions.changeRelationshipStatus(args, user)
      .then((r) => mentorRelationship.apiFunctions.getOne(r, user));
  }
};

mentorRelationship.functions.mutations.finishMentorRelationship = {
  type: mentorRelationship.type,

  args: {
    mentorRelationshipID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }) {
    args.statusID = STATUS.Finished;
    return mentorRelationship.apiFunctions.changeRelationshipStatus(args, user)
      .then((r) => mentorRelationship.apiFunctions.getOne(r, user));
  }
};



mentorRelationship.functions.mutations.del = {
  type: GraphQLBoolean,

  args: {
    mentorRelationshipID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }) {
    return mentorRelationship.apiFunctions.del(args, user).return(true);
  }
};

module.exports = mentorRelationship.functions;
