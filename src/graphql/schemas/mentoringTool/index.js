// Mentoring tool functions
const simpleSchemaFunctionGenerator = require("../simple/simpleSchemaFunctionGenerator");

const menteeListing = require("./menteeListing");
const mentorListing = require("./mentorListing");
const mentorRelationship = require("./mentorRelationship");

const mentorRelationshipStatus = simpleSchemaFunctionGenerator("MentorRelationshipStatus");

module.exports = {
  queries : {
    getMenteeListing     : menteeListing.getOne,
    getMenteeListings    : menteeListing.getAll,
    searchMenteeListings : menteeListing.search,

    getMentorListing     : mentorListing.getOne,
    getMentorListings    : mentorListing.getAll,
    searchMentorListings : mentorListing.search,

    getMentorRelationship     : mentorRelationship.getOne,
    getMentorRelationships    : mentorRelationship.getAll,
    searchMentorRelationships : mentorRelationship.search,

    getMentorRelationshipStatus      : mentorRelationshipStatus.getOne,
    getMentorRelationshipStatuses    : mentorRelationshipStatus.getAll,
    searchMentorRelationshipStatuses : mentorRelationshipStatus.search,
  },

  mutations : {
    // Mentoring tool
    createMenteeListing      : menteeListing.mutations.create,
    createMentorListing      : mentorListing.mutations.create,
    createMentorRelationship : mentorRelationship.mutations.create,

    editMenteeListing      : menteeListing.mutations.edit,
    editMentorListing      : mentorListing.mutations.edit,
    changeRelationshipStatus : mentorRelationship.mutations.changeRelationshipStatus,
    seenMentorRelationship : mentorRelationship.mutations.seenMentorRelationship,
    acceptMentorRelationship : mentorRelationship.mutations.acceptMentorRelationship,
    rejectMentorRelationship : mentorRelationship.mutations.rejectMentorRelationship,
    finishMentorRelationship : mentorRelationship.mutations.finishMentorRelationship,

    deleteMenteeListing      : menteeListing.mutations.del,
    deleteMentorListing      : mentorListing.mutations.del,
    deleteMentorRelationship : mentorRelationship.mutations.del,
  }
};


