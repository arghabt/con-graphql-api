// Mentee listing functions
const {
  GraphQLBoolean,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const graphQLFunctionsGenerator = require("../graphQLFunctionsGenerator");
const { MentorMenteeListingInputType } = require("../../types");

let menteeListing = graphQLFunctionsGenerator("MenteeListing");

menteeListing.functions.mutations.create = {
  type: menteeListing.type,

  args: {
    menteeListing: {
      type: new GraphQLNonNull(MentorMenteeListingInputType)
    }
  },
  resolve(_, args, { user }){
    return menteeListing.apiFunctions.create(args, user)
      .then((r) => menteeListing.apiFunctions.getOne(r, user));
  }
};

menteeListing.functions.mutations.edit = {
  type: menteeListing.type,

  args: {
    menteeListingID: {
      type: new GraphQLNonNull(GraphQLInt)
    },
    menteeListing: {
      type: new GraphQLNonNull(MentorMenteeListingInputType)
    }
  },
  resolve(_, args, { user }) {
    return menteeListing.apiFunctions.edit(args, user)
      .then((r) => menteeListing.apiFunctions.getOne(r, user));
  }
};

menteeListing.functions.mutations.del = {
  type: GraphQLBoolean,

  args: {
    menteeListingID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }) {
    return menteeListing.apiFunctions.del(args, user).return(true);
  }
};

module.exports = menteeListing.functions;
