// CTOTheme functions
const {
    GraphQLNonNull,
    GraphQLInt
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");

let clearanceApplicationQL = graphQLFunctionsGenerator("ClearanceApplication");
const { ClearanceApplicationInputType } = require("../types");

clearanceApplicationQL.functions.mutations.create = {
  type : clearanceApplicationQL.type,

  args : {
    staffID : {
      description : "The staff member this application is for",
      type : new GraphQLNonNull(GraphQLInt)
    },
    clearanceApplication : {
      type : new GraphQLNonNull(ClearanceApplicationInputType)
    }
  },

  resolve(_, args, { user }){
    return clearanceApplicationQL.apiFunctions.create(args, user)
      .then((r) => clearanceApplicationQL.apiFunctions.getOne(r, user));
  }
};

clearanceApplicationQL.functions.mutations.edit = {
  type : clearanceApplicationQL.type,

  args : {
    clearanceApplicationID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    clearanceApplication : {
      type : new GraphQLNonNull(ClearanceApplicationInputType)
    }
  },

  resolve(_, args, { user }){
    return clearanceApplicationQL.apiFunctions.edit(args, user)
      .then((r) => clearanceApplicationQL.apiFunctions.getOne(r, user));
  }
};



module.exports = clearanceApplicationQL.functions;
