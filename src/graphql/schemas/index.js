// The schemas - contains the functions graphql

const login = require("./login");
const misc = require("./misc");

const CTOTheme = require("./CTOTheme");

// Skills
const skill = require("./skill");
const skillSubcategory = require("./skillSubcategory");
const trainingPathway = require("./trainingPathway");
const skillEntry = require("./skillEntry");

// Staff
const staff = require("./staff");
const clearanceApplication = require("./clearanceApplication");
const staffAttachment = require("./staffAttachment");


// Projects
const project = require("./project");
const role = require("./role");
const roleSkillEntry = require("./roleSkillEntry");
const assignment = require("./assignment");
const assignmentRequest = require("./assignmentRequest");
const assignmentEstimate = require("./assignmentEstimate");
const latestAssignmentEstimate = require("./latestAssignmentEstimate");

const root_fields = Object.assign({
  getCurrentUser : login.currentUser,


  getCTOTheme : CTOTheme.getOne,
  getCTOThemes : CTOTheme.getAll,
  searchCTOThemes : CTOTheme.search,


  // Skills
  getSkill : skill.getOne,
  getSkills : skill.getAll,
  searchSkills : skill.search,

  getSkillSubcategory : skillSubcategory.getOne,
  getSkillSubcategories : skillSubcategory.getAll,
  searchSkillSubcategories : skillSubcategory.search,

  getTrainingPathway : trainingPathway.getOne,
  getTrainingPathways : trainingPathway.getAll,
  searchTrainingPathways : trainingPathway.search,

  getSkillEntry : skillEntry.getOne,
  getSkillEntries : skillEntry.getAll,
  searchSkillEntries : skillEntry.search,


  // Staff
  getStaffMember : staff.getOne,
  getStaff : staff.getAll,
  searchStaff : staff.search,
  searchStaffFuzzy : staff.fuzzySearch,
  getStaffMemberByLoginID : staff.getStaffMemberByLoginID,

  getClearanceApplication : clearanceApplication.getOne,
  getClearanceApplications : clearanceApplication.getAll,
  searchClearanceApplications : clearanceApplication.search,

  getStaffAttachment : staffAttachment.getOne,
  getStaffAttachments : staffAttachment.getAll,
  searchStaffAttachments : staffAttachment.search,


  // Project
  getProject : project.getOne,
  getProjects : project.getAll,
  searchProjects : project.search,

  getRole : role.getOne,
  getRoles : role.getAll,
  searchRoles : role.search,

  getRoleSkillEntry : roleSkillEntry.getOne,
  getRoleSkillEntries : roleSkillEntry.getAll,
  searchRoleSkillEntries : roleSkillEntry.search,

  getAssignment : assignment.getOne,
  getAssignments : assignment.getAll,
  searchAssignments : assignment.search,

  // No GET option, as there is no ID column
  getAssignmentRequests : assignmentRequest.getAll,
  searchAssignmentRequests : assignmentRequest.search,

  getAssignmentEstimate : assignmentEstimate.getOne,
  getAssignmentEstimates : assignmentEstimate.getAll,
  searchAssignmentEstimates : assignmentEstimate.search,

  getLatestAssignmentEstimate : latestAssignmentEstimate.getOne,
  getLatestAssignmentEstimates : latestAssignmentEstimate.getAll,
  searchLatestAssignmentEstimates : latestAssignmentEstimate.search,
},
misc.queries,
require("./simple").queries,

// Opportunities Marketplace
require("./opportunitiesMarketplace").queries,

// Mentoring tool
require("./mentoringTool").queries
);


const mutation_fields = Object.assign({
  login : login.login,

  // Staff
  addStaffMember : staff.mutations.create,
  editStaffMember : staff.mutations.edit,
  editStaffMemberBio : staff.mutations.editBio,
  editMe : staff.mutations.editSelf,
  editMyBio : staff.mutations.editMyBio,
  editUserRole : staff.mutations.editUserRole,

  deleteStaffAttachment : staffAttachment.mutations.deleteFile,

  createClearanceApplication : clearanceApplication.mutations.create,
  editClearanceApplication : clearanceApplication.mutations.edit,

  addSkill : skill.mutations.create,
  editSkill : skill.mutations.edit,
  deleteSkill : skill.mutations.del,

  createSkillSubcategory : skillSubcategory.mutations.create,
  editSkillSubcategory : skillSubcategory.mutations.edit,
  deleteSkillSubcategory : skillSubcategory.mutations.del,

  createTrainingPathway : trainingPathway.mutations.create,
  editTrainingPathway : trainingPathway.mutations.edit,
  deleteTrainingPathway : trainingPathway.mutations.del,

  addSkillEntry : skillEntry.mutations.create,
  addSkillEntries : skillEntry.mutations.createMany,
  editSkillEntry : skillEntry.mutations.edit,
  deleteSkillEntry : skillEntry.mutations.del,

  addMySkillEntry : skillEntry.mutations.my_create,
  addMySkillEntries : skillEntry.mutations.my_createMany,
  editMySkillEntry : skillEntry.mutations.my_edit,
  deleteMySkillEntry : skillEntry.mutations.my_del,

  // Projects
  createProject : project.mutations.create,
  editProject : project.mutations.edit,
  deleteProject : project.mutations.del,

  createPotentialProject : project.mutations.createPotentialProject,
  createDiscoveryProject : project.mutations.createDiscoveryProject,

  addRole : role.mutations.create,
  duplicateRole : role.mutations.duplicate,


  // Assignments
  assignRole : assignment.mutations.create,
  unassignRole : assignment.mutations.del,

  changeAssignmentApproval : assignment.mutations.changeApproval,
  approveAssignment : assignment.mutations.approve,
  rejectAssignment : assignment.mutations.reject,

  createAssignmentRequest : assignmentRequest.mutations.create,
  deleteAssignmentRequest : assignmentRequest.mutations.del,
  convertAssignmentRequestToAssignment : assignmentRequest.mutations.convertToAssignment,

  createOrRequestAssignment : assignment.mutations.createOrRequestAssignment,


  addNewAssignmentEstimate : assignmentEstimate.mutations.addNewAssignmentEstimate,
  approveLatestAssignmentEstimate : assignmentEstimate.mutations.approveLatestAssignmentEstimate,
  rejectLatestAssignmentEstimate : assignmentEstimate.mutations.rejectLatestAssignmentEstimate,
},
misc.mutations,
require("./simple").mutations,

// Mentoring tool
require("./mentoringTool").mutations
);



module.exports = { queries : root_fields, mutations : mutation_fields };
