// Login

const {
  GraphQLString,
  GraphQLNonNull
} = require("graphql/type");


const auth = require("../../auth");
const { Staff } = require("../../api");
const { LoginResponseType, StaffMemberType } = require("../types");

module.exports = {
  login : {
    type : LoginResponseType,

    description : "Authenticates a user against the ActiveDirectory.\nReturns an access token that can be used to authorise subsequent requests",

    args : {
      loginID : {
        type: new GraphQLNonNull(GraphQLString)
      },

      password : {
        type: new GraphQLNonNull(GraphQLString)
      }
    },

    resolve(_, args){
      return auth.authenticateUser(args.loginID, args.password).then(result => {
        result.authenticated = true;
        return result;
      });
    }
  },

  currentUser : {
    type : StaffMemberType,
    
    description : "Gets the user represented by the supplied access token. Or null if none is sent",

    resolve(_, __, { user }){
      return user.ID ? Staff.getOne({ ID : user.getID() }, user) : null;
    }
  }
};

