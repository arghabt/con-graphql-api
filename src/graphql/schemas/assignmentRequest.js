// Assignment functions
const {
  GraphQLNonNull,
  GraphQLInt,
  GraphQLBoolean
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");

let assignmentRequestsQL = graphQLFunctionsGenerator("AssignmentRequest");

const { AssignmentType } = require("../types"); 
const { Assignment } = require("../../api");


assignmentRequestsQL.functions.mutations.create = {
  type : assignmentRequestsQL.type,

  description : [
    "Creates an assignment request.",
    "To conditionally create a request or an actual assignment",
    "depending on permissions, use createOrRequestAssignment"
  ].join(" "),

  args : {
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return assignmentRequestsQL.apiFunctions.create(args, user)
      .then(result => assignmentRequestsQL.apiFunctions.get(result, user));
  }
};

assignmentRequestsQL.functions.mutations.del = {
  type : GraphQLBoolean,

  args : {
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return assignmentRequestsQL.apiFunctions.del(args, user).return(true);
  }
};


assignmentRequestsQL.functions.mutations.convertToAssignment = {
  type : AssignmentType,

  description : "Convert an assignment request to an assignment",

  args : {
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return assignmentRequestsQL.apiFunctions.convertToAssignment(args, user)
      .then((r) => Assignment.getOne(r, user));
  }
};


module.exports = assignmentRequestsQL.functions;
