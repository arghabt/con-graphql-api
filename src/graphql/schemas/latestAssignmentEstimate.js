// Latest assignment estimate functions

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");
const { AssignmentEstimateType } = require("../types");

let assignmentQL = graphQLFunctionsGenerator("LatestAssignmentEstimate", AssignmentEstimateType);

module.exports = assignmentQL.functions;
