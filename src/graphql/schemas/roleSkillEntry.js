// Role Skill Entry functions

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");

let skillEntriesQL = graphQLFunctionsGenerator("RoleSkillEntry");

module.exports = skillEntriesQL.functions;
