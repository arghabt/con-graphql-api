// Simple Schema Function generator
// Generates simple schema functions for given types
const graphQLFunctionsGenerator = require("../graphQLFunctionsGenerator");
const mutationFunctionGenerator = require("./mutationFunctionGenerator");


module.exports = function simpleSchemaFunctionGenerator(name){
  let generator = graphQLFunctionsGenerator(name);
  mutationFunctionGenerator(generator);

  return generator.functions;
};

