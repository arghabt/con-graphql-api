// Simple Schema Function generator
// Generates simple schema functions for given types
const {
  GraphQLInt,
  GraphQLBoolean,
  GraphQLNonNull
} = require("graphql/type");

const types = require("../../types");

module.exports = function mutationFunctionGenerator(generator){
  let name = generator.name;
  let funcs = generator.functions;
  let apiFunctions = generator.apiFunctions;
  let type = generator.type;
  let inputType = types[name + "InputType"];

  let nameWithLower = name.charAt(0).toLowerCase() + name.slice(1);
  let nameWithLowerID = nameWithLower + "ID";

  funcs.mutations.create =  {
    type : type,

    description : `Creates one ${name}`,

    args : {
      [nameWithLower] : {
        type : new GraphQLNonNull(inputType)
      }
    },

    resolve(_, args, { user }){
      let a = {
        input : args[nameWithLower]
      };
      return apiFunctions.create(a, user)
        .then((r) => apiFunctions.getOne(r, user));
    }
  };


  funcs.mutations.edit = {
    type : type,

    description : `Edits a ${name}`,

    args : {
      [nameWithLowerID] : {
        type : new GraphQLNonNull(GraphQLInt)
      },

      [nameWithLower] : {
        type : new GraphQLNonNull(inputType)
      }
    },

    resolve(_, args, { user }){
      let a = {
        ID : args[nameWithLowerID],
        input : args[nameWithLower]
      };
      return apiFunctions.edit(a, user)
        .then((r) => apiFunctions.getOne(r, user));
    }
  };

  funcs.mutations.del = {
    type : GraphQLBoolean,

    description : `Deletes a ${name}`,

    args : {
      [nameWithLowerID] : {
        type : new GraphQLNonNull(GraphQLInt)
      }
    },

    resolve(_, args, { user }){
      let a = {
        ID : args[nameWithLowerID]
      };
      return apiFunctions.del(a, user);
    }
  };

  return generator;
};

