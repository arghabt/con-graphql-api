const simpleSchemaFunctionGenerator = require("./simpleSchemaFunctionGenerator");

const adminRole = simpleSchemaFunctionGenerator("AdminRole");

const location = simpleSchemaFunctionGenerator("Location");
const clearance = simpleSchemaFunctionGenerator("Clearance");
const capabilityArea = simpleSchemaFunctionGenerator("CapabilityArea");
const fundingType = simpleSchemaFunctionGenerator("FundingType");
const practiceArea = simpleSchemaFunctionGenerator("PracticeArea");
const profession = simpleSchemaFunctionGenerator("Profession");
const SFIAGrade = simpleSchemaFunctionGenerator("SFIAGrade");
const grade = simpleSchemaFunctionGenerator("Grade");
const customerTopUp = simpleSchemaFunctionGenerator("CustomerTopUp");
const customerAccount = simpleSchemaFunctionGenerator("CustomerAccount");
const clearanceHolder = simpleSchemaFunctionGenerator("ClearanceHolder");
const clearanceSponsor = simpleSchemaFunctionGenerator("ClearanceSponsor");
const customer = simpleSchemaFunctionGenerator("Customer");

const skillTag = simpleSchemaFunctionGenerator("SkillTag");

const projectStage = simpleSchemaFunctionGenerator("ProjectStage");
const roleTag = simpleSchemaFunctionGenerator("RoleTag");

const eciProject = simpleSchemaFunctionGenerator("ECIProject");

module.exports = {
  queries : {
    getAdminRole : adminRole.getOne,
    getAdminRoles : adminRole.getAll,
    searchAdminRoles : adminRole.search,

    // Core
    getLocation : location.getOne,
    getLocations : location.getAll,
    searchLocations : location.search,

    getClearance : clearance.getOne,
    getClearances : clearance.getAll,
    searchClearances : clearance.search,

    getCapabilityArea : capabilityArea.getOne,
    getCapabilityAreas : capabilityArea.getAll,
    searchCapabilityAreas : capabilityArea.search,

    getFundingType : fundingType.getOne,
    getFundingTypes : fundingType.getAll,
    searchFundingTypes : fundingType.search,

    getPracticeArea : practiceArea.getOne,
    getPracticeAreas : practiceArea.getAll,
    searchPracticeAreas : practiceArea.search,

    getProfession : profession.getOne,
    getProfessions : profession.getAll,
    searchProfessions : profession.search,

    getSFIAGrade : SFIAGrade.getOne,
    getSFIAGrades : SFIAGrade.getAll,
    searchSFIAGrades : SFIAGrade.search,

    getGrade : grade.getOne,
    getGrades : grade.getAll,
    searchGrades : grade.search,

    getCustomerTopUp : customerTopUp.getOne,
    getCustomerTopUps : customerTopUp.getAll,
    searchCustomerTopUps : customerTopUp.search,

    getCustomerAccount : customerAccount.getOne,
    getCustomerAccounts : customerAccount.getAll,
    searchCustomerAccounts : customerAccount.search,

    getClearanceHolder : clearanceHolder.getOne,
    getClearanceHolders : clearanceHolder.getAll,
    searchClearanceHolders : clearanceHolder.search,

    getClearanceSponsor : clearanceSponsor.getOne,
    getClearanceSponsors : clearanceSponsor.getAll,
    searchClearanceSponsors : clearanceSponsor.search,

    getCustomer : customer.getOne,
    getCustomers : customer.getAll,
    searchCustomers : customer.search,

    getECIProject : eciProject.getOne,
    getECIProjects : eciProject.getAll,
    searchECIProjects : eciProject.search,


    // Skills

    getSkillTag : skillTag.getOne,
    getSkillTags : skillTag.getAll,
    searchSkillTags : skillTag.search,


    // Projects/roles
    getProjectStage : projectStage.getOne,
    getProjectStages : projectStage.getAll,
    searchProjectStages : projectStage.search,

    getRoleTag : roleTag.getOne,
    getRoleTags : roleTag.getAll,
    searchRoleTags : roleTag.search,
  },

  mutations : {
    createLocation : location.mutations.create,
    editLocation : location.mutations.edit,
    deleteLocation : location.mutations.del,

    createClearance : clearance.mutations.create,
    editClearance : clearance.mutations.edit,
    deleteClearance : clearance.mutations.del,

    createCapabilityArea : capabilityArea.mutations.create,
    editCapabilityArea : capabilityArea.mutations.edit,
    deleteCapabilityArea : capabilityArea.mutations.del,

    createFundingType : fundingType.mutations.create,
    editFundingType : fundingType.mutations.edit,
    deleteFundingType : fundingType.mutations.del,

    createPracticeArea : practiceArea.mutations.create,
    editPracticeArea : practiceArea.mutations.edit,
    deletePracticeArea : practiceArea.mutations.del,

    createProfession : profession.mutations.create,
    editProfession : profession.mutations.edit,
    deleteProfession : profession.mutations.del,

    createSFIAGrade : SFIAGrade.mutations.create,
    editSFIAGrade : SFIAGrade.mutations.edit,
    deleteSFIAGrade : SFIAGrade.mutations.del,

    createGrade : grade.mutations.create,
    editGrade : grade.mutations.edit,
    deleteGrade : grade.mutations.del,

    createCustomerTopUp : customerTopUp.mutations.create,
    editCustomerTopUp : customerTopUp.mutations.edit,
    deleteCustomerTopUp : customerTopUp.mutations.del,

    createCustomerAccount : customerAccount.mutations.create,
    editCustomerAccount : customerAccount.mutations.edit,
    deleteCustomerAccount : customerAccount.mutations.del,

    createClearanceHolder : clearanceHolder.mutations.create,
    editClearanceHolder : clearanceHolder.mutations.edit,
    deleteClearanceHolder : clearanceHolder.mutations.del,

    createClearanceSponsor : clearanceSponsor.mutations.create,
    editClearanceSponsor : clearanceSponsor.mutations.edit,
    deleteClearanceSponsor : clearanceSponsor.mutations.del,

    createCustomer : customer.mutations.create,
    editCustomer : customer.mutations.edit,
    deleteCustomer : customer.mutations.del,

    createSkillTag : skillTag.mutations.create,
    editSkillTag : skillTag.mutations.edit,
    deleteSkillTag : skillTag.mutations.del,




    createProjectStage : projectStage.mutations.create,
    editProjectStage : projectStage.mutations.edit,
    deleteProjectStage : projectStage.mutations.del,

    createRoleTag : roleTag.mutations.create,
    editRoleTag : roleTag.mutations.edit,
    deleteRoleTag : roleTag.mutations.del,

  }
};
