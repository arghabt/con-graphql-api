// Skill functions
const {
    GraphQLNonNull,
    GraphQLInt,
    GraphQLBoolean
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");

let skillsQL = graphQLFunctionsGenerator("Skill");
const { SkillInputType } = require("../types");

skillsQL.functions.mutations.create = {
  type : skillsQL.type,

  args : {
    skill : {
      type : new GraphQLNonNull(SkillInputType)
    }
  },

  resolve(_, args, { user }){
    return skillsQL.apiFunctions.create(args, user)
      .then((r) => skillsQL.apiFunctions.getOne(r, user));
  }
};

skillsQL.functions.mutations.edit = {
  type : skillsQL.type,

  args : {
    skillID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    skill : {
      type : new GraphQLNonNull(SkillInputType)
    }
  },

  resolve(_, args, { user }){
    return skillsQL.apiFunctions.edit(args, user)
      .then((r) => skillsQL.apiFunctions.getOne(r, user));
  }
};

skillsQL.functions.mutations.del = {
  type : GraphQLBoolean,

  description : "Deletes a skill",

  args : {
    skillID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return skillsQL.apiFunctions.del(args, user);
  }
};


module.exports = skillsQL.functions;
