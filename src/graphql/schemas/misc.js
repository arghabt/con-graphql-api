// Misc GQL Endpoints
const {
    GraphQLNonNull,
    GraphQLString
} = require("graphql/type");

const config = require("config");

module.exports = {
  queries : {
    getServerEnvironment : {
      type : new GraphQLNonNull(GraphQLString),

      resolve(_, __, user){
        return config.util.getEnv("NODE_ENV");
      }
    },

    ping : {
      type : new GraphQLNonNull(GraphQLString),

      args : {
        question : {
          type : GraphQLString
        }
      },

      resolve(args){
        return (args && args.question) || "pong";
      }
    }
  }
};
