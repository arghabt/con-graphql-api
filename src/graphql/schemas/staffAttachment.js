// StaffAttachment functions
const {
    GraphQLNonNull,
    GraphQLInt,
    GraphQLBoolean
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");

let staffAttachmentQL = graphQLFunctionsGenerator("StaffAttachment");

staffAttachmentQL.functions.mutations.deleteFile = {
  type : GraphQLBoolean,

  args : {
    attachmentID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return staffAttachmentQL.apiFunctions.deleteFile(args, user);
  }
};

module.exports = staffAttachmentQL.functions;
