// Staff functions
const {
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt,
    GraphQLString
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");
const { StaffMemberType, StaffMemberInputType, FuzzedStaffMemberType } = require("../types");

let staffQL = graphQLFunctionsGenerator("Staff", StaffMemberType);


staffQL.functions.getStaffMemberByLoginID = {
  type : staffQL.type,

  args : {
    loginID : {
      type : new GraphQLNonNull(GraphQLString)
    }
  },

  resolve(_, args, { user }){
    return staffQL.apiFunctions.getByLoginID(args, user);
  }
};


staffQL.functions.fuzzySearch = Object.assign({}, staffQL.functions.search, {
  type : new GraphQLList(FuzzedStaffMemberType),

  description : "Perform a fuzzy search",

  resolve(_, args, { user }){
    return staffQL.apiFunctions.fuzzySearch(args, user);
  }
});


staffQL.functions.mutations.create = {
  type : staffQL.type,

  args : {
    staffMember : {
      type : new GraphQLNonNull(StaffMemberInputType)
    }
  },

  resolve(_, args, { user }){
    return staffQL.apiFunctions.create(args, user)
      .then((r) => staffQL.apiFunctions.getOne(r, user));
  }
};

staffQL.functions.mutations.edit = {
  type : staffQL.type,

  args : {
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    staffMember : {
      type : StaffMemberInputType
    }
  },

  resolve(_, args, { user }){
    return staffQL.apiFunctions.edit(args, user)
      .then((r) => staffQL.apiFunctions.getOne(r, user));
  }
};

staffQL.functions.mutations.editSelf = {
  type : staffQL.type,

  args : {
    staffMember : {
      type : StaffMemberInputType
    }
  },

  resolve(_, args, { user }){
    return staffQL.apiFunctions.editSelf(args, user)
      .then((r) => staffQL.apiFunctions.getOne(r, user));
  }
};

staffQL.functions.mutations.editBio = {
  type : staffQL.type,

  args : {
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    bio : {
      type : new GraphQLNonNull(GraphQLString)
    }
  },

  resolve(_, args, { user }){
    return staffQL.apiFunctions.editBio(args, user)
      .then((r) => staffQL.apiFunctions.getOne(r, user));
  }
};

staffQL.functions.mutations.editMyBio = {
  type : staffQL.type,

  args : {
    bio : {
      type : new GraphQLNonNull(GraphQLString)
    }
  },

  resolve(_, args, { user }){
    return staffQL.apiFunctions.editMyBio(args, user)
      .then((r) => staffQL.apiFunctions.getOne(r, user));
  }
};

staffQL.functions.mutations.editUserRole = {
  type : staffQL.type,

  args : {
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return staffQL.apiFunctions.editUserRole(args, user)
      .then((r) => staffQL.apiFunctions.getOne(r, user));
  }
};


module.exports = staffQL.functions;
