// Project functions
const {
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt,
    GraphQLBoolean
} = require("graphql/type");


const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");
const { ProjectInputType, DiscoveryProjectInputType, PotentialProjectInputType, RoleInputType, EditRoleInputType } = require("../types");

let projectsQL = graphQLFunctionsGenerator("Project");

projectsQL.functions.mutations.create = {
  type : projectsQL.type,

  args : {
    project : {
      type : new GraphQLNonNull(ProjectInputType)
    },
    roles : {
      type : new GraphQLList(RoleInputType)
    }
  },

  resolve(_, args, { user }){
    return projectsQL.apiFunctions.create(args, user)
      .then((r) => projectsQL.apiFunctions.getOne(r, user));
  }
};

projectsQL.functions.mutations.createDiscoveryProject = {
  type : projectsQL.type,

  args : {
    project : {
      type : new GraphQLNonNull(DiscoveryProjectInputType)
    }
  },

  resolve(_, args, { user }){
    return projectsQL.apiFunctions.createDiscoveryProject(args, user)
      .then((r) => projectsQL.apiFunctions.getOne(r, user));
  }
};

projectsQL.functions.mutations.createPotentialProject = {
  type : projectsQL.type,

  args : {
    project : {
      type : new GraphQLNonNull(PotentialProjectInputType)
    }
  },

  resolve(_, args, { user }){
    return projectsQL.apiFunctions.createPotentialProject(args, user)
      .then((r) => projectsQL.apiFunctions.getOne(r, user));
  }
};

projectsQL.functions.mutations.edit = {
  type : projectsQL.type,

  args : {
    projectID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    project : {
      type : ProjectInputType
    },
    addRoles : {
      type : new GraphQLList(RoleInputType)
    },
    editRoles : {
      type : new GraphQLList(EditRoleInputType)
    },
    deleteRoles : {
      type : new GraphQLList(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return projectsQL.apiFunctions.edit(args, user)
      .then((r) => projectsQL.apiFunctions.getOne(r, user));
  }
};

projectsQL.functions.mutations.del = {
  type : GraphQLBoolean,

  args : {
    projectID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    force : {
      type : GraphQLBoolean
    }
  },

  resolve(_, args, { user }){
    return projectsQL.apiFunctions.del(args, user).return(true);
  }
};


module.exports = projectsQL.functions;
