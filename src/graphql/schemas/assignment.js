// Assignment functions
const {
    GraphQLNonNull,
    GraphQLInt,
    GraphQLBoolean
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");

let assignmentsQL = graphQLFunctionsGenerator("Assignment");

let AssignmentRequest = require("../../api/AssignmentRequest");
let { AssignmentAndAssignmentRequestUnionType } = require("../types");

assignmentsQL.functions.mutations.create = {
  type : assignmentsQL.type,

  description : [
    "Creates an assignment.",
    "To conditionally create a request or an actual assignment",
    "depending on permissions, use createOrRequestAssignment"
  ].join(" "),

  args : {
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return assignmentsQL.apiFunctions.create(args, user)
      .then((r) => assignmentsQL.apiFunctions.getOne(r, user));
  }
};

assignmentsQL.functions.mutations.del = {
  type : GraphQLBoolean,

  description : [
    "Deletes an assignment.",
    "Deleting assignments can only be done if",
    "the win probability is < 100 for the role or the project",
    "or the role has not yet started"
  ].join(" "),

  args : {
    assignmentID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
  },

  resolve(_, args, { user }){
    return assignmentsQL.apiFunctions.del(args, user);
  }
};

assignmentsQL.functions.mutations.createOrRequestAssignment = {
  type : AssignmentAndAssignmentRequestUnionType,

  description : [
    "Creates an assignment if the users has sufficent permissions to do so.",
    "If not, an assignment request will be created instead"
  ].join(" "),

  args : {
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return assignmentsQL.apiFunctions.createOrRequestAssignment(args, user)
      .then((r) => r._assignmentRequest ? AssignmentRequest.get(r, user) : assignmentsQL.apiFunctions.getOne(r, user));
  }
};

assignmentsQL.functions.mutations.changeApproval = {
  type : assignmentsQL.type,

  args : {
    assignmentID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    approved : {
      type : GraphQLBoolean
    }
  },

  resolve(_, args, { user }){
    return assignmentsQL.apiFunctions.changeApproval(args, user)
      .then((r) => assignmentsQL.apiFunctions.getOne(r, user));
  }
};

assignmentsQL.functions.mutations.approve = {
  type : assignmentsQL.type,

  args : {
    assignmentID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, { assignmentID }, { user }){
    return assignmentsQL.apiFunctions.changeApproval({ assignmentID, approved : true }, user)
      .then((r) => assignmentsQL.apiFunctions.getOne(r, user));
  }
};

assignmentsQL.functions.mutations.reject = {
  type : assignmentsQL.type,

  args : {
    assignmentID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, { assignmentID }, { user }){
    return assignmentsQL.apiFunctions.changeApproval({ assignmentID, approved : false }, user)
      .then((r) => assignmentsQL.apiFunctions.getOne(r, user));
  }
};


module.exports = assignmentsQL.functions;
