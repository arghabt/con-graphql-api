// AssignmentEstimate functions
const {
    GraphQLNonNull,
    GraphQLInt
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");
let assignmentEstimatesQL = graphQLFunctionsGenerator("AssignmentEstimate");

const { Assignment } = require("../../api");
const { AssignmentEstimateInputType } = require("../types");

assignmentEstimatesQL.functions.mutations.addNewAssignmentEstimate = {
  type : assignmentEstimatesQL.type,

  description : [
    "Adds a new assignment estimate. If the submitter is not the project manager approved cannot be set.", 
    "The first estimate for the assignment must contain startDate and endDate."
  ].join(" "),

  args : {
    assignmentID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    assignmentEstimate : {
      type : new GraphQLNonNull(AssignmentEstimateInputType)
    }
  },

  resolve(_, args, { user }){
    return Assignment.addNewAssignmentEstimate(args, user)
      .then((r) => assignmentEstimatesQL.apiFunctions.getOne(r, user));
  }
};

// This is basically an alias for above but only requires the assignmentID
// and will autoset the "approved" field to true
assignmentEstimatesQL.functions.mutations.approveLatestAssignmentEstimate = {
  type : assignmentEstimatesQL.type,

  description : "Adds a new assignment estimate that is the same as the previous one but with approved set to true.",

  args : {
    assignmentID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, { assignmentID }, { user }){
    return Assignment.addNewAssignmentEstimate({
      assignmentID,
      assignmentEstimate : { approved : true }
    }, user)
      .then((r) => assignmentEstimatesQL.apiFunctions.getOne(r, user));
  }
};

// Same as above with "approved" = false
assignmentEstimatesQL.functions.mutations.rejectLatestAssignmentEstimate = {
  type : assignmentEstimatesQL.type,

  description : "Adds a new assignment estimate that is the same as the previous one but with approved set to false.",

  args : {
    assignmentID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, { assignmentID }, { user }){
    return Assignment.addNewAssignmentEstimate({
      assignmentID,
      assignmentEstimate : { approved : false }
    }, user)
      .then((r) => assignmentEstimatesQL.apiFunctions.getOne(r, user));
  }
};


module.exports = assignmentEstimatesQL.functions;
