// CTOTheme functions

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");

let CTOThemeQL = graphQLFunctionsGenerator("CTOTheme");

module.exports = CTOThemeQL.functions;
