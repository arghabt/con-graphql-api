// Class to generate basic graphql function sets

const {
  GraphQLList,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLNonNull
} = require("graphql/type");


const api = require("../../api");
const types = require("../types");

const { convertDBRolesToGraphQLFields } = require("../utils");

function graphQLFunctionsGenerator(name, type, apiFunctions){
  if(!name){
    throw new Error("Name is required");
  }

  apiFunctions = apiFunctions || api[name];
  type = type || types[name + "Type"];

  if(!type){
    throw new Error(`A type was not provided for ${name} and could not be inferred`);
  }

  // Functions
  let ex = {
    name : name,
    apiFunctions : apiFunctions,
    type : type,


    functions : {
      getAll : {
        type : new GraphQLList(type),

        description : `Gets all ${name}s`,

        // If there is an inactive search option, allow this to be used with getAll
        args : Object.assign({
          orderBy : {
            type : types.OrderByInputType
          }
        },
          (function(){
            if(apiFunctions.hasInactiveField()){
              return {
                inactive : {
                  type : GraphQLBoolean
                },
                includeInactive : {
                  type : GraphQLBoolean
                }
              };
            }
          })()
        ),

        resolve(_, args, { user }){
          return ex.apiFunctions.getAll(args, user);
        }
      },

      search : {
        type : new GraphQLList(type),

        description : `Search ${name}s`,

        args : convertDBRolesToGraphQLFields(apiFunctions.getSearchOptions()),

        resolve(_, args, { user }){
          return ex.apiFunctions.search(args, user);
        }
      },
    
      mutations : {}
    },

  };


  // Only create a getOne function if it includes an ID
  if(apiFunctions.getColumnNames().includes("ID")){
    ex.functions.getOne = {
      type : type,

      description : `Gets one ${name} by its ID`,

      args : {
        ID : {
          type : new GraphQLNonNull(GraphQLInt)
        }
      },

      resolve(_, args, { user }){
        return ex.apiFunctions.getOne(args, user);
      }
    };
  }

  return ex;
}


module.exports = graphQLFunctionsGenerator;
