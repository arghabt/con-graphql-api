
const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");
const mutationFunctionGenerator = require("./simple/mutationFunctionGenerator");

let SkillSubcategoryQL = graphQLFunctionsGenerator("SkillSubcategory");
mutationFunctionGenerator(SkillSubcategoryQL);


module.exports = SkillSubcategoryQL.functions;
