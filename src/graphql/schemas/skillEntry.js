// Skill Entry functions
const {
    GraphQLNonNull,
    GraphQLInt,
    GraphQLBoolean,
    GraphQLList
} = require("graphql/type");

const graphQLFunctionsGenerator = require("./graphQLFunctionsGenerator");
const { SkillEntryInputType } = require("../types");

let skillEntriesQL = graphQLFunctionsGenerator("SkillEntry");

// Set to false to turn off people editing their own skills
const EXPOSE_SELF_EDITING_FUNCTIONS = true;


skillEntriesQL.functions.mutations.create = {
  type : skillEntriesQL.type,

  args : {
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    skillEntry : {
      type : new GraphQLNonNull(SkillEntryInputType)
    }
  },

  resolve(_, args, { user }){
    return skillEntriesQL.apiFunctions.create(args, user)
      .then((r) => skillEntriesQL.apiFunctions.getOne(r, user));
  }
};

skillEntriesQL.functions.mutations.createMany = {
  type : new GraphQLList(skillEntriesQL.type),

  args : {
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    skillEntries : {
      type : new GraphQLList(SkillEntryInputType)
    }
  },

  resolve(_, args, { user }){
    return skillEntriesQL.apiFunctions.createMany(args, user)
      .map((r) => skillEntriesQL.apiFunctions.getOne({ ID : r }, user));
  }
};

skillEntriesQL.functions.mutations.edit = {
  type : skillEntriesQL.type,

  args : {
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    skillEntry : {
      type : new GraphQLNonNull(SkillEntryInputType)
    }
  },

  resolve(_, args, { user }){
    return skillEntriesQL.apiFunctions.edit(args, user)
      .then((r) => skillEntriesQL.apiFunctions.getOne(r, user));
  }
};

skillEntriesQL.functions.mutations.del = {
  type : GraphQLBoolean,

  args : {
    staffID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    skillID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  },

  resolve(_, args, { user }){
    return skillEntriesQL.apiFunctions.del(args, user).return(true);
  }
};






// Self editing functions
if(EXPOSE_SELF_EDITING_FUNCTIONS){
  skillEntriesQL.functions.mutations.my_create = {
    type : skillEntriesQL.type,

    args : {
      skillEntry : {
        type : new GraphQLNonNull(SkillEntryInputType)
      }
    },

    resolve(_, args, { user }){
      return skillEntriesQL.apiFunctions.my_create(args, user)
        .then((r) => skillEntriesQL.apiFunctions.getOne(r, user));
    }
  };

  skillEntriesQL.functions.mutations.my_createMany = {
    type : new GraphQLList(skillEntriesQL.type),

    args : {
      skillEntries : {
        type : new GraphQLList(SkillEntryInputType)
      }
    },

    resolve(_, args, { user }){
      return skillEntriesQL.apiFunctions.my_createMany(args, user)
        .map((r) => skillEntriesQL.apiFunctions.getOne({ ID : r }, user));
    }
  };

  skillEntriesQL.functions.mutations.my_edit = {
    type : skillEntriesQL.type,

    args : {
      skillEntry : {
        type : new GraphQLNonNull(SkillEntryInputType)
      }
    },

    resolve(_, args, { user }){
      return skillEntriesQL.apiFunctions.my_edit(args, user)
        .then((r) => skillEntriesQL.apiFunctions.getOne(r, user));
    }
  };

  skillEntriesQL.functions.mutations.my_del = {
    type : GraphQLBoolean,

    args : {
      skillID : {
        type : new GraphQLNonNull(GraphQLInt)
      }
    },

    resolve(_, args, { user }){
      return skillEntriesQL.apiFunctions.my_del(args, user).return(true);
    }
  };
}


module.exports = skillEntriesQL.functions;
