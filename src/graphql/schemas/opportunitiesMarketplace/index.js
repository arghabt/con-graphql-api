
const savedSearch = require("./savedSearch");

module.exports = {
  queries : {
    getSavedSearch      : savedSearch.getOne,
    getSavedSearches    : savedSearch.getAll,
    searchSavedSearches : savedSearch.search,
  }
};
