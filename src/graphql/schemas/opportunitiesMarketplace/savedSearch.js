// Skill functions

const graphQLFunctionsGenerator = require("../graphQLFunctionsGenerator");

let savedSearchsQL = graphQLFunctionsGenerator("SavedSearch");

module.exports = savedSearchsQL.functions;
