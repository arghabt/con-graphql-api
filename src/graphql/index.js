// Root schema

const {
	GraphQLSchema,
	GraphQLObjectType
} = require("graphql");

const { queries, mutations } = require("./schemas");
const loggerHook = require("./loggerHook");

const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType',
    fields: queries
  }),

  mutation : new GraphQLObjectType({
    name: 'MutationType',
    fields: loggerHook(mutations)
  })
});

module.exports = { schema };
