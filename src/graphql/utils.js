// Functions to convert the SQL "type" strings to the graphql type

const {
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLBoolean,
  GraphQLList
} = require("graphql/type");

const DateType = require("./types/Date");
const {
  NumberComparatorInputType,
  DateComparatorInputType,
  OrderByInputType,
  SkillSearchInputType,
  PercentFreeAtInputType
} = require("./types/inputTypes");

function isArrayType(type){
  return /^\[(.*)\]$/.exec(type);
}

let ex = {
  apiToGraphQLType(type){
    let at = isArrayType(type);
    if(at){
      return new GraphQLList(ex.apiToGraphQLType(at[1]));
    }

    switch(type){
      case "FK" :
      case "PK" :
      case "Int" :
        return GraphQLInt;

      case "Float" :
        return GraphQLFloat;

      case "Text" :
      case "NVarChar" :
        return GraphQLString;
      case "Bit" :
        return GraphQLBoolean;

      case "Date" :
      case "Time" :
      case "DateTime" :
      case "DateTime2" :
        return DateType;


      case "NumberComparator" :
        return NumberComparatorInputType;
      case "DateComparator" :
        return DateComparatorInputType;

      case "OrderBy" :
        return OrderByInputType;


      // Custom types
      case "Skill" :
        return SkillSearchInputType;

      case "PercentFreeAt" :
        return PercentFreeAtInputType;


      default :
        throw new Error(`Type "${type}" not recognised. Please add a mapping to graphql/utils.js:apiToGraphQLType`);
    }
  },

  convertDBRolesToGraphQLFields(cols){
    let fields = {};
    cols.forEach(({ name, type, description }) => {
      fields[name] = {
        type : ex.apiToGraphQLType(type),
        description
      };
    });
    return fields;
  }
};


module.exports = ex;
