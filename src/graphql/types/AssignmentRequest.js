// Assignment Request GraphQL type

const {
  GraphQLObjectType,
  GraphQLBoolean
} = require("graphql/type");

const AssignmentType = require("./Assignment");

const { Staff, Role } = require("../../api");
const DateType = require("./Date");

module.exports = new GraphQLObjectType({
  name: 'AssignmentRequestType',
  fields: () => {
    const RoleType = require("./Role");
    const StaffMemberType = require("./StaffMember");

    return {
      role : {
        type : RoleType,
        resolve({ roleID:ID }, _, { user }){
          return Role.getOne({ ID }, user);
        }
      },

      requestedStaffMember : {
        type : StaffMemberType,
        resolve({ staffID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      submittedBy : {
        type : StaffMemberType,
        resolve({ submittedBy:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      timestamp : {
        type : DateType
      },

      roleHasAssignment : {
        type : GraphQLBoolean,
        resolve({ roleID }, _, { user }){
          return Role.assignmentExistsForRole({ roleID }, user);
        }
      },

      roleAssignment : {
        type : AssignmentType,
        resolve({ roleID }, _, { user }){
          return Role.getAssignmentForRole({ roleID }, user);
        }
      }
    };
  }
});
