// Role Editing input type
// For adding/editing roles

const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLNonNull
} = require("graphql/type");

const RoleSkillInputType = require("./RoleSkillInput");

module.exports = new GraphQLInputObjectType({
  name : "EditRoleSkillInputType",

  fields : {
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    skillEntry : {
      type : new GraphQLNonNull(RoleSkillInputType)
    }
  }
});