// Staff member intput type

const {
    GraphQLInputObjectType,
    GraphQLList,
    GraphQLInt,
    GraphQLString,
    GraphQLBoolean
} = require("graphql/type");


module.exports = new GraphQLInputObjectType({
  name : "SkillInputType",

  fields : {
    skillName : {
      type : GraphQLString
    },

    category : {
      type : GraphQLInt
    },

    inactive : {
      type : GraphQLBoolean
    },

    skillTagIDs : {
      type : new GraphQLList(GraphQLInt)
    }
  }
});
