// Project input type
// For adding/editing projects

const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString
} = require("graphql/type");


module.exports = new GraphQLInputObjectType({
  name : "DiscoveryProjectInputType",

  fields : {
    name : {
      type : GraphQLString
    },
    description : {
      type : GraphQLString
    },
    sensitivity : {
      type : GraphQLInt
    },
    CTOThemeID : {
      type : GraphQLInt
    }
  }
});
