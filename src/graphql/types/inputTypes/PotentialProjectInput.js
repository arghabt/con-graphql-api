// Project input type
// For adding/editing projects

const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString
} = require("graphql/type");

const DateType = require("../Date");

module.exports = new GraphQLInputObjectType({
  name : "PotentialProjectInputType",

  fields : {
    name : {
      type : GraphQLString
    },
    description : {
      type : GraphQLString
    },
    startDate : {
      type : DateType
    },
    winProbability : {
      type : GraphQLInt
    },
    sensitivity : {
      type : GraphQLInt
    },
    capabilityAreaID : {
      type : GraphQLInt
    }
  }
});
