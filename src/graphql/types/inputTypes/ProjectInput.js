// Project input type
// For adding/editing projects

const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLBoolean
} = require("graphql/type");

const DateType = require("../Date");

module.exports = new GraphQLInputObjectType({
  name : "ProjectInputType",

  fields : {
    name : {
      type : GraphQLString
    },
    projectManagerID : {
      type : GraphQLInt
    },
    techLeadID : {
      type : GraphQLInt
    },
    description : {
      type : GraphQLString
    },
    startDate : {
      type : DateType
    },
    endDate : {
      type : DateType
    },
    fundingTypeID : {
      type : GraphQLInt
    },
    winProbability : {
      type : GraphQLInt
    },
    projectStageID : {
      type : GraphQLInt
    },
    approved : {
      type : GraphQLBoolean
    },
    sensitivity : {
      type : GraphQLInt
    },

    CTOThemeID : {
      type : GraphQLInt
    },
    capabilityAreaID : {
      type : GraphQLInt
    }
  }
});
