// Clearance application intput type

const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLBoolean
} = require("graphql/type");

const DateType = require("../Date");

module.exports = new GraphQLInputObjectType({
  name : "ClearanceApplicationInputType",

  fields : {
    clearanceReferenceNumber : {
      type : GraphQLString
    },

    clearanceAppliedDate : {
      type : DateType
    },

    clearanceIssuedDate : {
      type : DateType
    },

    clearanceExpiryDate : {
      type : DateType
    },

    STRAPReadOnDate : {
      type : DateType
    },

    clearanceLevelID : {
      type : GraphQLInt
    },

    clearanceSponsorID : {
      type : GraphQLInt
    },

    clearanceSponsorProjectID : {
      type : GraphQLInt
    },

    privilegedUserChecked : {
      type : GraphQLBoolean
    },

    STRAPWaiverRequired : {
      type : GraphQLBoolean
    },

    STRAPWaiverApprovalDate : {
      type : DateType
    },

    clearanceTransferStatus : {
      type : GraphQLString
    },

    clearanceNotes : {
      type : GraphQLString
    },

  }
});
