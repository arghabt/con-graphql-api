const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString,
} = require("graphql/type");

module.exports = new GraphQLInputObjectType({
  name : "SkillSubcategoryInputType",

  fields : {
    name : {
      type : GraphQLInt
    },
    professionID : {
      type : GraphQLInt
    },
    subSubjectMatterOwnerID : {
      type : GraphQLInt
    },
    confluenceURL : {
      type : GraphQLString
    },
  }
});
