// Date and int comparison types


const {
  GraphQLInputObjectType,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString
} = require("graphql/type");

const DateType = require("../Date");

class ComparatorInputType extends GraphQLInputObjectType {}


let NumberComparatorInputType = new ComparatorInputType({
  name : "NumberComparatorInputType",

  description : "Represents a search comparison for a number value. Queries are constructed as: equals OR (isLessThan AND isGreaterThan).",

  fields : {
    equals : {
      type : GraphQLInt
    },
    isLessThan : {
      type : GraphQLInt
    },
    isGreaterThan : {
      type : GraphQLInt
    },
    isEqualOrLessThan : {
      type : GraphQLInt
    },
    isEqualOrGreaterThan : {
      type : GraphQLInt
    }
  }
});


let DateComparatorInputType = new ComparatorInputType({
  name : "DateComparatorInputType",

  description : "Represents a search comparison for an date value. Queries are constructed as: equals OR (isAfter AND isBefore).",

  fields : {
    equals : {
      type : DateType
    },
    isBefore : {
      type : DateType
    },
    isAfter : {
      type : DateType
    },
    isSameOrBefore : {
      type : DateType
    },
    isSameOrAfter : {
      type : DateType
    }
  }
});


let OrderByOrderEnum = new GraphQLEnumType({
  name : "OrderByOrderEnum",

  description : "Represents the direct in which to order the response to a query",

  values : {
    accesending : {
      value : "ASC",
      deprecationReason : "Incorrect spelling, please use ascending instead"
    },
    desending : {
      value : "DESC",
      deprecationReason : "Incorrect spelling, please use descending instead"
    },

    ascending : {
      value : "ASC"
    },
    descending : {
      value : "DESC"
    }
  }
});

let OrderByInputType = new ComparatorInputType({
  name : "OrderByInputType",

  description : "Represents how to order the returned query. Can only order on direct scalar types returned by the query.",

  fields : {
    field : {
      type : new GraphQLNonNull(GraphQLString)
    },

    direction : {
      type : OrderByOrderEnum,
      defaultValue : "ASC"
    }
  }
});


module.exports = { NumberComparatorInputType, DateComparatorInputType, OrderByInputType };
