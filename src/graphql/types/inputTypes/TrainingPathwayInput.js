
const {
  GraphQLInputObjectType,
  GraphQLString,
  GraphQLInt
} = require("graphql/type");

const DateType = require("../Date");

module.exports = new GraphQLInputObjectType({
  name: 'TrainingPathwayInputType',
  fields: {
    name : {
      type : GraphQLString
    },

    capabilityID : {
      type : GraphQLInt
    },

    professionID : {
      type : GraphQLInt
    },

    curatorID : {
      type : GraphQLInt
    },

    lastReviewedDate : {
      type : DateType
    },
  }
});
