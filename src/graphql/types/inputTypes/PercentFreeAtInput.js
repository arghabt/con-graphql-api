const {
    GraphQLInputObjectType,
    GraphQLInt
} = require("graphql/type");

const DateType = require("../Date");

module.exports = new GraphQLInputObjectType({
  name : "PercentFreeAtInputType",

  description : "Searches on a percentage a staff member has free at a particular datetime",

  fields : {
    at : {
      type : DateType,
      get defaultValue(){
        return new Date();
      }
    },
    percentFree : {
      type : GraphQLInt,
      defaultValue : 0
    }
  }
});
