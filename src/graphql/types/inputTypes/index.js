// Input type reconciler

module.exports = Object.assign({
  ProjectInputType : require("./ProjectInput"),
  PotentialProjectInputType : require("./PotentialProjectInput"),
  DiscoveryProjectInputType : require("./DiscoveryProjectInput"),

  RoleInputType : require("./RoleInput"),
  EditRoleInputType : require("./EditRoleInput"),

  SkillInputType : require("./SkillInput"),
  SkillSubcategoryInputType : require("./SkillSubcategoryInput"),
  TrainingPathwayInputType : require("./TrainingPathwayInput"),

  RoleSkillInputType : require("./RoleSkillInput"),
  EditRoleSkillInputType : require("./EditRoleSkillInput"),

  AssignmentEstimateInputType : require("./AssignmentEstimateInput"),

  SkillSearchInputType : require("./SkillSearchInput"),


  StaffMemberInputType : require("./StaffMemberInput"),
  SkillEntryInputType : require("./SkillEntryInput"),
  ECIProjectsReadOnInputType : require("./ECIProjectsReadOnInput"),

  ClearanceApplicationInputType : require("./ClearanceApplicationInput"),

  PercentFreeAtInputType : require("./PercentFreeAtInput"),

}, require("./ComparatorTypes"));
