// Role input type
// For adding/editing roles

const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLList,
    GraphQLBoolean
} = require("graphql/type");

const DateType = require("../Date");

const RoleSkillInputType = require("./RoleSkillInput");

module.exports = new GraphQLInputObjectType({
  name : "RoleInputType",

  fields : {
    gradeID : {
      type : GraphQLInt
    },
    professionID : {
      type : GraphQLInt
    },
    capabilityAreaID : {
      type : GraphQLInt
    },
    winProbability : {
      type : GraphQLInt
    },
    SFIAGradeID : {
      type : GraphQLInt
    },

    description : {
      type : GraphQLString
    },
    clearanceID : {
      type : GraphQLInt
    },

    startDate : {
      type : DateType
    },
    endDate : {
      type : DateType
    },

    workload : {
      type : GraphQLInt
    },
    briefed : {
      type : GraphQLBoolean
    },
    otherRequirements : {
      type : GraphQLString
    },
    advertise : {
      type : GraphQLBoolean
    },
    approved : {
      type : GraphQLBoolean
    },

    // Foreign tables
    locationIDs : {
      type : new GraphQLList(GraphQLInt)
    },

    accountIDs : {
      type : new GraphQLList(GraphQLInt)
    },

    customerTopUpIDs : {
      type : new GraphQLList(GraphQLInt)
    },

    roleTagIDs : {
      type : new GraphQLList(GraphQLInt)
    },

    skillEntries : {
      type : new GraphQLList(RoleSkillInputType)
    }
  }
});