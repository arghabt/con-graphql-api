// Assignment Estimate input type

const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLBoolean
} = require("graphql/type");

const DateType = require("../Date");

module.exports = new GraphQLInputObjectType({
  name : "AssignmentEstimateInputType",

  fields : {
    startDatetime : {
      type : DateType
    },
    endDatetime : {
      type : DateType
    },

    approved : {
      type : GraphQLBoolean
    },

    percentageOfTimeOnAssignment : {
      type : GraphQLInt
    },

    fedUp : {
      type : GraphQLBoolean
    }
  }
});