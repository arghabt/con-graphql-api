const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLNonNull
} = require("graphql/type");

module.exports = new GraphQLInputObjectType({
  name : "SkillSearchInputType",

  description : "Searches on skills",

  fields : {
    skillID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    skillLevel : {
      type : GraphQLInt,
      defaultValue : 1
    }
  }
});
