// Role Editing input type
// For adding/editing roles

const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLNonNull
} = require("graphql/type");

const RoleInputType = require("./RoleInput");

module.exports = new GraphQLInputObjectType({
  name : "EditRoleInputType",

  fields : {
    roleID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    role : {
      type : new GraphQLNonNull(RoleInputType)
    }
  }
});