// Staff member intput type

const {
    GraphQLInputObjectType,
    GraphQLList,
    GraphQLInt,
    GraphQLFloat,
    GraphQLString,
    GraphQLBoolean
} = require("graphql/type");

const DateType = require("../Date");

const SkillEntryInputType = require("./SkillEntryInput");
const ECIProjectsReadOnInputType = require("./ECIProjectsReadOnInput");

module.exports = new GraphQLInputObjectType({
  name : "StaffMemberInputType",

  fields : {
    surname : {
      type : GraphQLString
    },
    forename : {
      type : GraphQLString
    },
    email : {
      type : GraphQLString
    },
    EIN : {
      type : GraphQLInt
    },

    bio : {
      type : GraphQLString
    },
    notes : {
      type : GraphQLString
    },

    basedAt : {
      type : GraphQLInt
    },
    workingAt : {
      type : GraphQLInt
    },

    clearanceLevel : {
      type : GraphQLInt
    },

    customerTopUp : {
      type : GraphQLInt
    },

    startDate : {
      type : DateType
    },
    endDate : {
      type : DateType
    },
    yearsExperiencePriorToStarting : {
      type : GraphQLInt
    },

    loginID : {
      type : GraphQLString
    },

    startConfirmed : {
      type : GraphQLBoolean
    },

    workingHours : {
      type : GraphQLFloat
    },

    LV1A : {
      type : DateType
    },
    LV2A : {
      type : DateType
    },
    LV1C : {
      type : DateType
    },
    LV2C : {
      type : DateType
    },

    gradeID : {
      type : GraphQLInt
    },
    SFIAGradeID : {
      type : GraphQLInt
    },
    profession : {
      type : GraphQLInt
    },
    capability : {
      type : GraphQLInt
    },
    practiceArea : {
      type : GraphQLInt
    },
    clearanceHolderID : {
      type : GraphQLInt
    },
    clearanceSponsorID: {
      type : GraphQLInt
    },
    delegatedStaffMemberID: {
      type : GraphQLInt
    },


    customerAccountIDs : {
      type : new GraphQLList(GraphQLInt)
    },

    skillEntries : {
      type : new GraphQLList(SkillEntryInputType)
    },

    ECIProjectsReadOnTo : {
      type : new GraphQLList(ECIProjectsReadOnInputType)
    }
  }
});
