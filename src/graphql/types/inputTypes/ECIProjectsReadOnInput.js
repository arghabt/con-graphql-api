const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLNonNull
} = require("graphql/type");

const DateType = require("../Date");

module.exports = new GraphQLInputObjectType({
  name : "ECIProjectsReadOnInputType",

  fields : {
    ECIProjectID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    readOnDate : {
      type : new GraphQLNonNull(DateType)
    }
  }
});
