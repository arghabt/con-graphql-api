const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLNonNull
} = require("graphql/type");

module.exports = new GraphQLInputObjectType({
  name : "SkillEntryInputType",

  fields : {
    skillID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    skillLevel : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  }
});
