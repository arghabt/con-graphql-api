// Skill Entry GraphQL type

const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const DateType = require("./Date");

const { Staff, Skill } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'SkillEntryType',

  // "Thunked" to allow the circular dependency for staff
  fields : () => {
    const SkillType = require("./Skill");
    const StaffMemberType = require("./StaffMember");

    return {
      ID : {
        type: new GraphQLNonNull(GraphQLInt)
      },

      staffMember : {
        type : new GraphQLNonNull(StaffMemberType),
        resolve({ staffID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      skill : {
        type : new GraphQLNonNull(SkillType),
        resolve({ skillID:ID }, _, { user }){
          return Skill.getOne({ ID }, user);
        }
      },

      skillLevel : {
        type : new GraphQLNonNull(GraphQLInt)
      },

      lastUpdateTimestamp : {
        type : new GraphQLNonNull(DateType)
      }
    };
  }
});
