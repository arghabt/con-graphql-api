// Role GraphQL type

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLList,
  GraphQLNonNull
} = require("graphql/type");

const DateType = require("./Date");
const { NumberComparatorInputType } = require("./inputTypes");
const { ClearanceType, GradeType, ProfessionType, CapabilityAreaType, SFIAGradeType, CustomerAccountType, CustomerTopUpType, LocationType, RoleTagType } = require("./simple");
const RoleSkillEntryType = require("./RoleSkillEntry");
const AssignmentType = require("./Assignment");


const { Role, Project, Clearance, Grade, Profession, CapabilityArea, SFIAGrade, RoleSkillEntry, Staff } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'RoleType',
  fields: () => {
    const ProjectType = require("./Project");
    const StaffMemberType = require("./StaffMember");

    return {
      ID : {
        type: new GraphQLNonNull(GraphQLInt)
      },

      project : {
        type : new GraphQLNonNull(ProjectType),
        resolve({ projectID }, _, { user }){
          return Project.getOne({ ID : projectID }, user);
        }
      },

      description : {
        type : GraphQLString
      },

      clearance : {
        type : ClearanceType,
        resolve({ clearanceID }, _, { user }){
          return Clearance.getOne({ ID : clearanceID }, user);
        }
      },

      startDate : {
        type : DateType
      },

      endDate : {
        type : DateType
      },

      grade : {
        type : GradeType,
        resolve({ gradeID }, _, { user }){
          return Grade.getOne({ ID : gradeID }, user);
        }
      },

      workload : {
        type : GraphQLInt
      },

      briefed : {
        type : GraphQLBoolean
      },

      profession : {
        type : ProfessionType,
        resolve({ professionID }, _, { user }){
          return Profession.getOne({ ID : professionID }, user);
        }
      },

      capabilityArea : {
        type : CapabilityAreaType,
        resolve({ capabilityAreaID }, _, { user }){
          return CapabilityArea.getOne({ ID : capabilityAreaID }, user);
        }
      },

      winProbability : {
        type: GraphQLInt
      },


      SFIAGrade : {
        type : SFIAGradeType,
        resolve({ SFIAGradeID }, _, { user }){
          return SFIAGrade.getOne({ ID : SFIAGradeID }, user);
        }
      },

      otherRequirements : {
        type : GraphQLString
      },

      advertise : {
        type : GraphQLBoolean
      },

      approved : {
        type : GraphQLBoolean
      },

      submittedBy : {
        type : StaffMemberType,
        resolve({ submittedBy:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      customerTopUps : {
        type : new GraphQLList(CustomerTopUpType),
        resolve({ ID }, _, { user }){
          return Role.getCustomerTopUpsForRole({ roleID : ID }, user);
        }
      },

      customerAccounts : {
        type : new GraphQLList(CustomerAccountType),
        resolve({ ID }, _, { user }){
          return Role.getAccountsForRole({ roleID : ID }, user);
        }
      },

      locations : {
        type : new GraphQLList(LocationType),
        resolve({ ID }, _, { user }){
          return Role.getLocationsForRole({ roleID : ID }, user);
        }
      },

      skills : {
        type : new GraphQLList(RoleSkillEntryType),

        args : {
          skillIDs : {
            type : new GraphQLList(GraphQLInt)
          },
          skillLevel : {
            type : NumberComparatorInputType
          }
        },

        resolve({ ID }, args, { user }){
          return RoleSkillEntry.search(Object.assign({ roleID : ID }, args), user);
        }
      },

      assignment : {
        type : AssignmentType,
        resolve({ ID }, _, { user }){
          return Role.getAssignmentForRole({ roleID : ID }, user);
        }
      },


      roleTags : {
        type : new GraphQLList(RoleTagType),
        resolve({ ID }, _, { user }){
          return Role.getTagsForRole({ roleID : ID }, user);
        }
      },

    };
  }
});
