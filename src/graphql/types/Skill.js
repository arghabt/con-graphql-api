// Skill GraphQL type

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLList,
  GraphQLNonNull
} = require("graphql/type");

const { SkillTagType } = require("./simple");
const SkillSubcategoryType = require("./SkillSubcategory");

const { SkillSubcategory, Skill } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'SkillType',
  fields: {
    ID: {
      type : new GraphQLNonNull(GraphQLInt)
    },

    skillName : {
      type : new GraphQLNonNull(GraphQLString)
    },

    subCategory : {
      type : new GraphQLNonNull(SkillSubcategoryType),
      resolve({ subCategoryID:ID }, _, { user }){
        return SkillSubcategory.getOne({ ID }, user);
      }
    },

    skillTags : {
      type : new GraphQLList(SkillTagType),
      resolve({ ID }, _, { user }){
        return Skill.getTagsForSkill({ skillID : ID }, user);
      }
    },

    inactive : {
      type : GraphQLBoolean
    }
  }
});
