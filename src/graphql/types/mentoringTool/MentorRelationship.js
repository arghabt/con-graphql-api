// Mentor relationship Type

const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLNonNull
} = require("graphql/type");

const { MenteeListing, MentorListing, MentorRelationshipStatus } = require("../../../api");

const DateType = require("../Date");
const { MentorRelationshipStatusType } = require("../simple");
const MentorListingType = require("./MentorListing");
const MenteeListingType = require("./MenteeListing");

module.exports = new GraphQLObjectType({
  name: "MentorRelationshipType",

  fields: {
    ID: {
      type: new GraphQLNonNull(GraphQLInt)
    },

    mentorListing: {
      type: new GraphQLNonNull(MentorListingType),
      resolve({ mentorListingID : ID }) {
        return MentorListing.getOne({ ID });
      }
    },

    menteeListing: {
      type: new GraphQLNonNull(MenteeListingType),
      resolve({ menteeListingID : ID }) {
        return MenteeListing.getOne({ ID });
      }
    },

    status: {
      type: new GraphQLNonNull(MentorRelationshipStatusType),
      resolve({ status : ID }) {
        return MentorRelationshipStatus.getOne({ ID });
      }
    },

    lastUpdate: {
      type: new GraphQLNonNull(DateType)
    },

    wasSentByMentor: {
      type: new GraphQLNonNull(GraphQLBoolean)
    }
  }
});