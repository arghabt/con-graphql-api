// Mentoring tool reconciler

module.exports = Object.assign({
  // Mentoring tool
  MentorListingType : require("./MentorListing"),
  MenteeListingType : require("./MenteeListing"),
  MentorRelationshipType : require("./MentorRelationship"),

  MentorMenteeListingInputType : require("./MentorMenteeListingInput"),
  MentorRelationshipInputType : require("./MentorRelationshipInput"),
});

