const {
  GraphQLInputObjectType,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

module.exports = new GraphQLInputObjectType({
  name : "MentorMenteeListingInputType",

  fields : {
    skillID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    maxRelationships : {
      type : GraphQLInt
    },
  }
});