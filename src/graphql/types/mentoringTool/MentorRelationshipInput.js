const {
  GraphQLInputObjectType,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

module.exports = new GraphQLInputObjectType({
  name : "MentorRelationshipInputType",

  fields : {
    menteeListingID : {
      type : new GraphQLNonNull(GraphQLInt)
    },
    mentorListingID : {
      type : new GraphQLNonNull(GraphQLInt)
    }
  }
});