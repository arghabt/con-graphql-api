// Mentor listing / mentee listing type

const {
    GraphQLObjectType,
    GraphQLList,
    GraphQLInt,
    GraphQLNonNull
} = require("graphql/type");

const { Staff, Skill, MentorRelationship } = require("../../../api");

const DateType = require("../Date");

module.exports = new GraphQLObjectType({
  name: "MenteeListingType",

  fields: () => {
    const StaffType = require("../StaffMember");
    const SkillType = require("../Skill");
    const MentorRelationshipType = require("./MentorRelationship");

    return {
      ID: {
        type: new GraphQLNonNull(GraphQLInt)
      },

      staff: {
        type: new GraphQLNonNull(StaffType),
        resolve({ staffID : ID }, _, { user }) {
          return Staff.getOne({ ID }, user);
        }
      },

      skill: {
        type: new GraphQLNonNull(SkillType),

        resolve({ skillID : ID }, _, { user }) {
          return Skill.getOne({ ID }, user);
        }
      },

      timestamp: {
        type: new GraphQLNonNull(DateType)
      },

      maxRelationships: {
        type: new GraphQLNonNull(GraphQLInt)
      },

      relationships : {
        type : new GraphQLList(MentorRelationshipType),
        resolve({ ID }, _, { user }){
          return MentorRelationship.search({ menteeListingID : ID }, user);
        }
      }
    };
  }
});