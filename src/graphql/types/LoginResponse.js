// GraphQL type for login responses

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
  GraphQLNonNull
} = require("graphql/type");

const StaffType = require("./StaffMember");

const { Staff } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'LoginResponseType',

  description : [
    "Represents the response to a correctly authenticated login request.",
    "authenticated will always equal true, failed logins are returned as errors"
  ],

  fields: {
    authenticated : {
      type : new GraphQLNonNull(GraphQLBoolean)
    },

    token : {
      type : GraphQLString
    },

    user : {
      type : StaffType,
      resolve(args, _, { user }){
        return Staff.getOne({ ID : args.user.ID }, user);
      }
    }
  }
});


