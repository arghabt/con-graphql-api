// CTOTheme GraphQL type

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const { Staff, CapabilityArea } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'CTOThemeType',
  fields: () => {
    const { CapabilityAreaType } = require("./simple");
    const StaffMemberType = require("./StaffMember");

    return {
      ID: {
        type : new GraphQLNonNull(GraphQLInt)
      },

      name : {
        type : new GraphQLNonNull(GraphQLString)
      },

      themeOwner : {
        type : StaffMemberType,
        resolve({ themeOwner:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      capabilityArea : {
        type : CapabilityAreaType,
        resolve({ capabilityAreaID:ID }, _, { user }){
          return CapabilityArea.getOne({ ID }, user);
        }
      }
    };
  }
});
