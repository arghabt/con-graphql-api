// Creates a graphQL type object, plus adds some cheeky chappys on top
const {
  GraphQLObjectType
} = require("graphql/type");

const api = require("../../api");

const { convertDBRolesToGraphQLFields } = require("../utils");

function createGraphQLType(def, apiFunctions){
  let name = def.name;
  def.name = name + "Type";

  apiFunctions = apiFunctions || api[name];
  let searchOptions = convertDBRolesToGraphQLFields(apiFunctions.getSearchOptions());


  let type = new GraphQLObjectType(def);
  type.searchOptions = searchOptions;

  return type;
}

module.exports = createGraphQLType;
