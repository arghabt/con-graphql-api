// A type for fuzzed fields

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList
} = require("graphql/type");


module.exports = new GraphQLObjectType({
  name: 'FuzzedFieldsType',
  fields: {
    fields: {
      type : new GraphQLList(GraphQLString)
    },

    rank : {
      type : GraphQLInt
    }
  }
});
