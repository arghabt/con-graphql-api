
module.exports = {
  FuzzedStaffMemberType : require("./FuzzedStaffMember"),
  FuzzedFieldsType : require("./FuzzedFields"),

  FuzzedSkillsType : require("./FuzzedSkills"),
  FuzzedSkillType : require("./FuzzedSkill")
};
