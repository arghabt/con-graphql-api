// A type for fuzzed skill fields

const {
  GraphQLObjectType,
  GraphQLInt
} = require("graphql/type");

const SkillType = require("../Skill");
const { Skill } = require("../../../api");

module.exports = new GraphQLObjectType({
  name: 'FuzzedSkillType',
  fields: {
    skill: {
      type : SkillType,
      resolve({ skillID }, _, { user }){
        return Skill.getOne({ ID : skillID }, user);
      }
    },

    rank : {
      type : GraphQLInt
    }
  }
});
