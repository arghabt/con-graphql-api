// A type for fuzzed skill fields

const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLList
} = require("graphql/type");

const FuzzedSkillType = require("./FuzzedSkill");

module.exports = new GraphQLObjectType({
  name: 'FuzzedSkillsType',
  fields: {
    skills: {
      type : new GraphQLList(FuzzedSkillType)
    },

    rank : {
      type : GraphQLInt
    }
  }
});
