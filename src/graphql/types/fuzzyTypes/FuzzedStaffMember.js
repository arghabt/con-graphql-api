// A type for fuzzed staff members

const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const StaffMemberType = require("../StaffMember");
const FuzzedFieldsType = require("./FuzzedFields");
const FuzzedSkillsType = require("./FuzzedSkills");

module.exports = new GraphQLObjectType({
  name: 'FuzzedStaffMemberType',
  fields: {
    staffMember: {
      type : new GraphQLNonNull(StaffMemberType)
    },

    fuzzedFields : {
      type : FuzzedFieldsType
    },
    fuzzedSkills : {
      type : FuzzedSkillsType
    },

    rank : {
      type : GraphQLInt
    }
  }
});
