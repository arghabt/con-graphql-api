
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const { Staff, Profession } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'SkillSubcategoryType',
  fields: () => {
    const { ProfessionType } = require("./simple");
    const StaffMemberType = require("./StaffMember");

    return {
      ID: {
        type : new GraphQLNonNull(GraphQLInt)
      },

      name : {
        type : new GraphQLNonNull(GraphQLString)
      },

      profession : {
        type : new GraphQLNonNull(ProfessionType),
        resolve({ professionID:ID }, _, { user }){
          return Profession.getOne({ ID }, user);
        }
      },

      subSubjectMatterOwner : {
        type : StaffMemberType,
        resolve({ subSubjectMatterOwnerID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      confluenceURL : {
        type : GraphQLString
      },

    };
  }
});
