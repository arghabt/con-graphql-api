// Assignment GraphQL type

const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLList,
  GraphQLNonNull
} = require("graphql/type");


const { Staff, Role, Assignment } = require("../../api");
const DateType = require("./Date");

module.exports = new GraphQLObjectType({
  name: 'AssignmentType',
  fields: () => {
    const RoleType = require("./Role");
    const StaffMemberType = require("./StaffMember");
    const AssignmentEstimateType = require("./AssignmentEstimate");

    return {
      ID : {
        type: new GraphQLNonNull(GraphQLInt)
      },

      role : {
        type : RoleType,
        resolve({ roleID:ID }, _, { user }){
          return Role.getOne({ ID }, user);
        }
      },

      assignedStaffMember : {
        type : StaffMemberType,
        resolve({ staffID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      approved : {
        type : GraphQLBoolean,
      },

      submittedBy : {
        type : StaffMemberType,
        resolve({ submittedBy:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      timestamp : {
        type : DateType
      },




      assignmentEstimates : {
        type : new GraphQLList(AssignmentEstimateType),
        resolve({ ID }, _, { user }){
          return Assignment.getAssignmentEstimates({ assignmentID : ID }, user);
        }
      },

      latestAssignmentEstimate : {
        type : AssignmentEstimateType,
        resolve({ ID }, _, { user }){
          return Assignment.getLatestAssignmentEstimate({ assignmentID : ID }, user);
        }
      },

      latestAssigneeEstimate : {
        type : AssignmentEstimateType,
        resolve({ ID }, _, { user }){
          return Assignment.getLatestAssigneeEstimate({ assignmentID : ID }, user);
        }
      },

      latestProjectManagerEstimate : {
        type : AssignmentEstimateType,
        resolve({ ID }, _, { user }){
          return Assignment.getLatestProjectManagerEstimate({ assignmentID : ID }, user);
        }
      },

      latestApprovedEstimate : {
        type : AssignmentEstimateType,
        resolve({ ID }, _, { user }){
          return Assignment.getLatestApprovedEstimate({ assignmentID : ID }, user);
        }
      },

      
    };
  }
});
