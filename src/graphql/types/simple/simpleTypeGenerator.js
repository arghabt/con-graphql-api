// Generates simple GraphQL types
// See also: api/simpleAPIGenerator

const {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const api = require("../../../api");
const { convertDBRolesToGraphQLFields } = require("../../utils");

function objectFieldsResolver(objectFields){
  return function resolveObjectFieldProxy(){
    for(let f in objectFields){
      if(objectFields.hasOwnProperty(f)){
        let field = objectFields[f];
        if(field && field.call){
          field = field();
        }
        // Get only the valid properties for an output type
        // If there is no type or output type then do not create
        // the output
        if(field && (field.type || field.outputType)){
          objectFields[f] = Object.assign({
            type : field.outputType
          }, field);
        }else{
          delete objectFields[f];
        }
      }
    }

    return objectFields;
  };
}

function inputObjectFieldsResolver(objectFields){
  return function resolveInputObjectFieldProxy(){
    for(let f in objectFields){
      if(objectFields.hasOwnProperty(f)){
        let field = objectFields[f];
        if(field && field.call){
          field = field();
        }

        // Get only the valid properties for an input type
        // If there is no type or input type then do not create
        // the input
        if(field && (field.type || field.inputType)){
          objectFields[f] = Object.assign({
            type : field.inputType,
          }, field, {
            resolve : undefined // Remove resolve from the input type
          });
        }else{
          delete objectFields[f];
        }
      }
    }

    return objectFields;
  };
}

module.exports = function simpleTypeGenerator(typeName, additions = {}){
  let fncs = api[typeName];
  let searchOptions = convertDBRolesToGraphQLFields(fncs.getSearchOptions());

  let cols = fncs.getColumns();


  let objectFields = Object.assign(convertDBRolesToGraphQLFields(cols), additions, {
    ID: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  });


  let objectType = new GraphQLObjectType({
    name: typeName + "Type",
    fields: objectFieldsResolver(objectFields)
  });

  objectType.searchOptions = searchOptions;

  let gqlInputFields = convertDBRolesToGraphQLFields(cols);
  delete gqlInputFields.ID;

  let inputTypeFields = Object.assign({}, gqlInputFields, additions);

  let inputType = new GraphQLInputObjectType({
    name: typeName + "InputType",
    fields: inputObjectFieldsResolver(inputTypeFields)
  });

  return { objectType, inputType };
};

