// This file is to defined specific additions to simple types, and how to resolve them
const {
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const api = require("../../../api");

const additions = {};
module.exports = additions;


// Add additions here

// Location
/*additions.Location = {
  staffBasedHere : () => {
    let StaffMemberType = require("../StaffMember");

    return {
      outputType : new GraphQLList(StaffMemberType),
      //inputType : GraphQLInt,
      resolve({ ID }, _, { user }){
        return api.Staff.search({ basedAt : ID }, user);
      }
    };
  }
};*/



additions.CustomerAccount = {

  customer: () => {
    let { CustomerType } = require("./index");

    return {
      outputType : CustomerType,
      resolve({ customerID }, _, { user }){
        return api.Customer.getOne({ ID : customerID }, user);
      }
    };
  },

  customerID : {
    inputType : GraphQLInt
  }
};


additions.ClearanceSponsor = {

  customer: () => {
    let { CustomerType } = require("./index");

    return {
      outputType : CustomerType,
      resolve({ customerID }, _, { user }){
        return api.Customer.getOne({ ID : customerID }, user);
      }
    };
  },

  customerID : {
    inputType : GraphQLInt
  }
};


additions.ClearanceHolder = {

  customer: () => {
    let { CustomerType } = require("./index");

    return {
      outputType : CustomerType,
      resolve({ customerID }, _, { user }){
        return api.Customer.getOne({ ID : customerID }, user);
      }
    };
  },

  customerID : {
    inputType : GraphQLInt
  }
};



additions.CustomerTopUp = {

  customer: () => {
    let { CustomerType } = require("./index");

    return {
      outputType : CustomerType,
      resolve({ customerID }, _, { user }){
        return api.Customer.getOne({ ID : customerID }, user);
      }
    };
  },

  customerID : {
    inputType : GraphQLInt
  }
};


additions.Profession = {

  subjectMatterOwner : () => {
    let StaffMemberType = require("../StaffMember");

    return {
      outputType : new GraphQLNonNull(StaffMemberType),
      resolve({ subjectMatterOwnerID }, _, { user }){
        return api.Staff.getOne({ ID : subjectMatterOwnerID }, user);
      }
    };
  },

  subjectMatterOwnerID : {
    inputType : GraphQLInt
  }
};

additions.CapabilityArea = {

  technicalCapabilitySpecialist : () => {
    let StaffMemberType = require("../StaffMember");

    return {
      outputType : new GraphQLNonNull(StaffMemberType),
      resolve({ technicalCapabilitySpecialistID }, _, { user }){
        return api.Staff.getOne({ ID : technicalCapabilitySpecialistID }, user);
      }
    };
  },

  technicalCapabilitySpecialistID : {
    inputType : GraphQLInt
  }
};
