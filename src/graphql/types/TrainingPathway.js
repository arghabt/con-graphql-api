
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const { Staff, Profession, CapabilityArea } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'TrainingPathwayType',
  fields: () => {
    const { ProfessionType, CapabilityAreaType } = require("./simple");
    const DateType = require("./Date");
    const StaffMemberType = require("./StaffMember");

    return {
      ID: {
        type : new GraphQLNonNull(GraphQLInt)
      },

      name : {
        type : new GraphQLNonNull(GraphQLString)
      },

      capability : {
        type : CapabilityAreaType,
        resolve({ capabilityID:ID }, _, { user }){
          return CapabilityArea.getOne({ ID }, user);
        }
      },

      profession : {
        type : new GraphQLNonNull(ProfessionType),
        resolve({ professionID:ID }, _, { user }){
          return Profession.getOne({ ID }, user);
        }
      },

      curator : {
        type : StaffMemberType,
        resolve({ curatorID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      lastReviewedDate : {
        type : new GraphQLNonNull(DateType)
      },

    };
  }
});
