// Union between an Assignment and Assignment Request

const {
  GraphQLUnionType
} = require("graphql/type");

const AssignmentType = require("./Assignment");
const AssignmentRequestType = require("./AssignmentRequest");

module.exports = new GraphQLUnionType({
  name: 'AssignmentAndAssignmentRequestUnionType',
  types : [ AssignmentType, AssignmentRequestType ],
  resolveType(value){
    // AssignmentRequests do not have an ID field
    if(value.ID){
      return AssignmentType;
    }else{
      return AssignmentRequestType;
    }
  }
});
