// ECI Project read on to GraphQL type

const {
  GraphQLObjectType,
  GraphQLNonNull
} = require("graphql/type");


const { Staff, ECIProject } = require("../../api");
const { ECIProjectType } = require("./simple");
const DateType = require("./Date");


module.exports = new GraphQLObjectType({
  name: 'ECIProjectReadOnToType',

  // "Thunked" to allow the circular dependency for staff
  fields : () => {
    const StaffMemberType = require("./StaffMember");

    return {
      staffMember : {
        type : new GraphQLNonNull(StaffMemberType),
        resolve({ staffID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      ECIProject : {
        type : new GraphQLNonNull(ECIProjectType),
        resolve({ ECIProjectID:ID }, _, { user }){
          return ECIProject.getOne({ ID }, user);
        }
      },

      readOnDate : {
        type : new GraphQLNonNull(DateType)
      }
    };
  }
});
