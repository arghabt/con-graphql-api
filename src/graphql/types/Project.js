// Project GraphQL type

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLList,
  GraphQLNonNull
} = require("graphql/type");

const DateType = require("./Date");
const { FundingTypeType, ProjectStageType, CapabilityAreaType } = require("./simple");
const StaffMemberType = require("./StaffMember");
const RoleType = require("./Role");
const CTOThemeType = require("./CTOTheme");


const {
  Project,
  Staff,
  FundingType,
  Role,
  ProjectStage,
  CapabilityArea,
  CTOTheme
} = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'ProjectType',
  fields: {
    ID : {
      type: new GraphQLNonNull(GraphQLInt)
    },

    name : {
      type : new GraphQLNonNull(GraphQLString)
    },

    projectManager : {
      type : StaffMemberType,
      resolve({ projectManagerID:ID }, _, { user }){
        return Staff.getOne({ ID }, user);
      }
    },

    techLead : {
      type : StaffMemberType,
      resolve({ techLeadID:ID }, _, { user }){
        return Staff.getOne({ ID }, user);
      }
    },

    description : {
      type : GraphQLString
    },

    startDate : {
      type : DateType
    },

    endDate : {
      type : DateType
    },

    fundingType : {
      type : FundingTypeType,
      resolve({ fundingTypeID:ID }, _, { user }){
        return FundingType.getOne({ ID }, user);
      }
    },

    winProbability : {
      type: GraphQLInt
    },

    projectStage : {
      type : ProjectStageType,
      resolve({ projectStageID:ID }, _, { user }){
        return ProjectStage.getOne({ ID }, user);
      }
    },

    approved : {
      type : GraphQLBoolean
    },

    submittedBy : {
      type : StaffMemberType,
      resolve({ submittedBy:ID }, _, { user }){
        return Staff.getOne({ ID }, user);
      }
    },

    sensitivity : {
      type : GraphQLInt
    },

    capabilityArea : {
      type : CapabilityAreaType,
      resolve({ capabilityAreaID:ID }, _, { user }){
        return CapabilityArea.getOne({ ID }, user);
      }
    },

    CTOTheme : {
      type : CTOThemeType,
      resolve({ CTOThemeID:ID }, _, { user }){
        return CTOTheme.getOne({ ID }, user);
      }
    },


    roles : {
      type : new GraphQLList(RoleType),
      resolve({ ID }, _, { user }){
        return Role.search({ projectID : ID }, user);
      }
    },


    stakeholders : {
      type : new GraphQLList(StaffMemberType),
      resolve({ ID }, _, { user }){
        return Project.getStakeholdersForProject({ projectID : ID }, user);
      }
    },

    assignedStaff : {
      type : new GraphQLList(StaffMemberType),
      resolve({ ID }, _, { user }){
        return Project.getStaffAssignedToProject({ projectID : ID }, user);
      }
    }
  }
});
