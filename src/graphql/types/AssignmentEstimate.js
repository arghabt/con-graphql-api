// AssignmentEstimate GraphQL type

const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLNonNull
} = require("graphql/type");

const DateType = require("./Date");

const { Staff, Assignment } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'AssignmentEstimateType',
  fields: () => {
    const StaffMemberType = require("./StaffMember");
    const AssignmentType = require("./Assignment");

    return {
      ID : {
        type: new GraphQLNonNull(GraphQLInt)
      },

      timestamp : {
        type : DateType
      },


      submittedBy : {
        type : StaffMemberType,
        resolve({ submittedBy:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      startDatetime : {
        type : DateType
      },

      endDatetime : {
        type : DateType
      }, 
      
      approved : {
        type : GraphQLBoolean
      },

      percentageOfTimeOnAssignment : {
        type : GraphQLInt
      },

      fedUp : {
        type : GraphQLBoolean
      },


      assignment : {
        type : AssignmentType,
        resolve({ assignmentID:ID }, _, { user }){
          return Assignment.getOne({ ID }, user);
        }
      }
    };
  }
});
