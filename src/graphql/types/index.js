// Type reconciler

const simpleTypes = require("./simple");
const inputTypes = require("./inputTypes");
const fuzzyTypes = require("./fuzzyTypes");

module.exports = Object.assign({
  LoginResponseType : require("./LoginResponse"),

  DateType : require("./Date"),
  
  StaffMemberType : require("./StaffMember"),
  ClearanceApplicationType : require("./ClearanceApplication"),
  StaffAttachmentType : require("./StaffAttachment"),
  ECIProjectReadOnToType : require("./ECIProjectReadOnTo"),

  SkillType : require("./Skill"),
  SkillSubcategoryType : require("./SkillSubcategory"),
  TrainingPathwayType : require("./TrainingPathway"),
  SkillEntryType : require("./SkillEntry"),

  ProjectType : require("./Project"),
  RoleType : require("./Role"),
  RoleSkillEntryType : require("./RoleSkillEntry"),
  AssignmentType : require("./Assignment"),
  AssignmentRequestType : require("./AssignmentRequest"),
  AssignmentEstimateType : require("./AssignmentEstimate"),
  AssignmentAndAssignmentRequestUnionType : require("./AssignmentAndAssignmentRequestUnion"),

  CTOThemeType : require("./CTOTheme"),

}, simpleTypes, inputTypes, fuzzyTypes,
// Opportunities Marketplace
require("./opportunitiesMarketplace"),
// Mentoring tool
require("./mentoringTool")
);
