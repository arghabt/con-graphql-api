// Role Skill Entry GraphQL type

const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");


const { Role, Skill } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'RoleSkillEntryType',

  // "Thunked" to allow the circular dependency for roles
  fields : () => {
    const SkillType = require("./Skill");
    const RoleType = require("./Role");

    return {
      ID : {
        type: new GraphQLNonNull(GraphQLInt)
      },

      role : {
        type : new GraphQLNonNull(RoleType),
        resolve({ staffID:ID }, _, { user }){
          return Role.getOne({ ID }, user);
        }
      },

      skill : {
        type : new GraphQLNonNull(SkillType),
        resolve({ skillID:ID }, _, { user }){
          return Skill.getOne({ ID }, user);
        }
      },

      skillLevel : {
        type : new GraphQLNonNull(GraphQLInt)
      }
    };
  }
});
