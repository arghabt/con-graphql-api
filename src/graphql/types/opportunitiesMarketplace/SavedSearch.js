// Skill GraphQL type

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");


const StaffMember = require("../StaffMember");

const { Staff } = require("../../../api");


module.exports = new GraphQLObjectType({
  name: 'SavedSearchType',
  fields: {
    ID: {
      type : new GraphQLNonNull(GraphQLInt)
    },

    name : {
      type : new GraphQLNonNull(GraphQLString)
    },

    staffMember : {
      type : new GraphQLNonNull(StaffMember),
      resolve({ staffID:ID }, _, { user }){
        return Staff.getOne({ ID }, user);
      }
    },

    searchJSON : {
      type : GraphQLString
    }
  }
});
