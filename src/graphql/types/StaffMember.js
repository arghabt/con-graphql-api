// StaffMember GraphQL type

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLBoolean,
  GraphQLList,
  GraphQLNonNull
} = require("graphql/type");

const DateType = require("./Date");
const {
  LocationType,
  CapabilityAreaType,
  ClearanceType,
  PracticeAreaType,
  ProfessionType,
  CustomerTopUpType,
  CustomerAccountType,
  GradeType,
  SFIAGradeType,
  ClearanceSponsorType,
  ClearanceHolderType,
  AdminRoleType
} = require("./simple");

const SkillEntryType = require("./SkillEntry");
const RoleType = require("./Role");
const StaffAttachmentType = require("./StaffAttachment");
const ClearanceApplicationType = require("./ClearanceApplication");
const ECIProjectReadOnToType = require("./ECIProjectReadOnTo");

const {
  Staff,
  Location,
  Clearance,
  Profession,
  CapabilityArea,
  PracticeArea,
  SkillEntry,
  CustomerTopUp,
  Grade,
  Project,
  SFIAGrade,
  ClearanceHolder,
  ClearanceSponsor,
  StaffAttachment,
  ClearanceApplication,
  ECIProjectsReadOnTo
} = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'StaffMemberType',
  fields: () => {
    const ProjectType = require("./Project");

    return {
      ID : {
        type: new GraphQLNonNull(GraphQLInt)
      },

      forename : {
        type: new GraphQLNonNull(GraphQLString)
      },
      surname : {
        type: new GraphQLNonNull(GraphQLString)
      },

      name : {
        type : new GraphQLNonNull(GraphQLString)
      },

      email : {
        type: GraphQLString
      },

      EIN : {
        type : GraphQLInt
      },

      bio : {
        type : GraphQLString
      },

      notes : {
        type : GraphQLString
      },

      basedAt : {
        type: LocationType,
        resolve({ basedAt:ID }, _, { user }){
          return Location.getOne({ ID }, user);
        }
      },
      workingAt : {
        type: LocationType,
        resolve({ workingAt:ID }, _, { user }){
          return Location.getOne({ ID }, user);
        }
      },

      inactive : {
        type : GraphQLBoolean
      },

      clearanceLevel : {
        type : ClearanceType,
        resolve({ clearanceLevel:ID }, _, { user }){
          return Clearance.getOne({ ID }, user);
        }
      },

      customerTopUp : {
        type : CustomerTopUpType,
        resolve({ customerTopUp:ID }, _, { user }){
          return CustomerTopUp.getOne({ ID }, user);
        }
      },

      startDate : {
        type : DateType
      },
      endDate : {
        type : DateType
      },
      startConfirmed : {
        type : GraphQLBoolean
      },
      yearsExperiencePriorToStarting : {
        type : GraphQLInt
      },

      workingHours : {
        type : GraphQLFloat
      },

      loginID : {
        type : GraphQLString
      },

      LV1A : {
        type : DateType
      },
      LV2A : {
        type : DateType
      },
      LV1C : {
        type : DateType
      },
      LV2C : {
        type : DateType
      },


      grade : {
        type : GradeType,
        resolve({ gradeID:ID }, _, { user }){
          return Grade.getOne({ ID }, user);
        }
      },
      SFIAGrade : {
        type : SFIAGradeType,
        resolve({ SFIAGradeID }, _, { user }){
          return SFIAGrade.getOne({ ID : SFIAGradeID }, user);
        }
      },
      profession : {
        type : ProfessionType,
        resolve({ profession:ID }, _, { user }){
          return Profession.getOne({ ID }, user);
        }
      },
      capability : {
        type : CapabilityAreaType,
        resolve({ capability:ID }, _, { user }){
          return CapabilityArea.getOne({ ID }, user);
        }
      },
      practiceArea : {
        type : PracticeAreaType,
        resolve({ practiceArea:ID }, _, { user }){
          return PracticeArea.getOne({ ID }, user);
        }
      },

      clearanceHolder : {
        type : ClearanceHolderType,
        resolve({ clearanceHolderID:ID }, _, { user }){
          return ClearanceHolder.getOne({ ID }, user);
        }
      },

      clearanceSponsor : {
        type : ClearanceSponsorType,
        resolve({ clearanceSponsorID:ID }, _, { user }){
          return ClearanceSponsor.getOne({ ID }, user);
        }
      },

      delegatedStaffMember : {
        type : module.exports, // self
        resolve({ delegatedStaffMemberID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },


      roleID : {
        type : GraphQLInt,
        resolve({ ID }, _, { user }){
          return Staff.getRoleID({ staffID : ID }, user);
        }
      },
      role : {
        type : AdminRoleType,
        resolve({ ID }, _, { user }){
          return Staff.getRole({ staffID : ID }, user);
        }
      },

      isAProjectManager : {
        type : GraphQLBoolean,
        resolve({ ID }, _, { user }){
          return Staff.isAProjectManager({ staffID : ID }, user);
        }
      },


      skills : {
        type : new GraphQLList(SkillEntryType),
        resolve({ ID }, _, { user }){
          return SkillEntry.search({ staffID : ID }, user);
        }
      },


      customerAccounts : {
        type : new GraphQLList(CustomerAccountType),
        resolve({ ID }, _, { user }){
          return Staff.getCustomerAccountsForStaff({ staffID : ID }, user);
        }
      },

      ECIProjectsReadOnTo : {
        type : new GraphQLList(ECIProjectReadOnToType),
        resolve({ ID }, _, { user }){
          return ECIProjectsReadOnTo.search({ staffID : ID }, user);
        }
      },


      assignedRoles : {
        type : new GraphQLList(RoleType),

        args : {
          currentRoles : {
            type : GraphQLBoolean
          }
        },

        resolve({ ID }, { currentRoles }, { user }){
          return Staff.getRolesAssignedToStaff({ staffID : ID, currentRoles }, user);
        }
      },


      managedProjects : {
        type : new GraphQLList(ProjectType),
        description : "Projects that are managed by this staff member",

        args : {
          inactive : {
            type : GraphQLBoolean
          }
        },

        resolve({ ID }, { inactive }, { user }){
          return Project.search({ projectManagerID : ID, inactive }, user);
        }
      },

      techLeadProjects : {
        type : new GraphQLList(ProjectType),
        description : "Projects that this staff member tech leads",

        args : {
          inactive : {
            type : GraphQLBoolean
          }
        },

        resolve({ ID }, { inactive }, { user }){
          return Project.search({ techLeadID : ID, inactive }, user);
        }
      },


      attachments : {
        type : new GraphQLList(StaffAttachmentType),
        description : "Attachments associated with this staff member",

        resolve({ ID }, _, { user }){
          return StaffAttachment.search({ staffID : ID }, user);
        }
      },


      clearanceApplications : {
        type : new GraphQLList(ClearanceApplicationType),
        description : "All Clearance Applications for this staff member",

        resolve({ ID }, _, { user }){
          return ClearanceApplication.search({ staffID : ID }, user);
        }
      },

      latestAppliedApplication : {
        type : ClearanceApplicationType,
        description : "Get the latest clearance application for a user",

        resolve({ ID }, _, user){
          return ClearanceApplication.getLatestAppliedApplicationForStaffMember({ staffID : ID }, user);
        }
      },
      latestIssuedApplication : {
        type : ClearanceApplicationType,
        description : "Get the latest issued clearance application for a user",

        resolve({ ID }, _, user){
          return ClearanceApplication.getLatestIssuedApplicationForStaffMember({ staffID : ID }, user);
        }
      },
      latestExpiredApplication : {
        type : ClearanceApplicationType,
        description : "Get the latest expired clearance application for a user",

        resolve({ ID }, _, user){
          return ClearanceApplication.getLatestExpiredApplicationForStaffMember({ staffID : ID }, user);
        }
      }
    };
  }
});
