// Staff Attachment GraphQL type

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull
} = require("graphql/type");

const { Staff } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'StaffAttachmentType',

  description : "Represents an attached in the database. Returns a url relative to root of the app that will serve that file.",

  fields: () => {
    const StaffMemberType = require("./StaffMember");

    return {
      ID: {
        type : new GraphQLNonNull(GraphQLInt)
      },

      filename : {
        type : new GraphQLNonNull(GraphQLString)
      },

      url : {
        type : GraphQLString
      },

      staffMember : {
        type : StaffMemberType,
        resolve({ staffID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },
    };
  }
});
