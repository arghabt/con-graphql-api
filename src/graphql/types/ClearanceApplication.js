// ClearanceApplication GraphQL type

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLNonNull
} = require("graphql/type");

const DateType = require("./Date");

const { ClearanceType, ClearanceSponsorType } = require("./simple");

const { Staff, Project, Clearance, ClearanceSponsor } = require("../../api");


module.exports = new GraphQLObjectType({
  name: 'ClearanceApplicationType',
  fields: () => {
    const StaffMemberType = require("./StaffMember");
    const ProjectType = require("./Project");

    return {
      ID : {
        type : new GraphQLNonNull(GraphQLInt)
      },

      staffMember : {
        type : new GraphQLNonNull(StaffMemberType),
        resolve({ staffID:ID }, _, { user }){
          return Staff.getOne({ ID }, user);
        }
      },

      clearanceReferenceNumber : {
        type : GraphQLString
      },

      clearanceAppliedDate : {
        type : DateType
      },

      clearanceIssuedDate : {
        type : DateType
      },

      clearanceExpiryDate : {
        type : DateType
      },

      STRAPReadOnDate : {
        type : DateType
      },

      clearanceLevel : {
        type : new GraphQLNonNull(ClearanceType),
        resolve({ clearanceLevelID:ID }, _, { user }){
          return Clearance.getOne({ ID }, user);
        }
      },

      clearanceSponsor : {
        type : ClearanceSponsorType,
        resolve({ clearanceSponsorID:ID }, _, { user }){
          return ClearanceSponsor.getOne({ ID }, user);
        }
      },

      clearanceSponsorProject : {
        type : ProjectType,
        resolve({ clearanceSponsorProjectID:ID }, _, { user }){
          return Project.getOne({ ID }, user);
        }
      },

      privilegedUserChecked : {
        type : new GraphQLNonNull(GraphQLBoolean)
      },

      STRAPWaiverRequired : {
        type : new GraphQLNonNull(GraphQLBoolean)
      },

      STRAPWaiverApprovalDate : {
        type : DateType
      },

      clearanceTransferStatus : {
        type : GraphQLString
      },

      clearanceNotes : {
        type : GraphQLString
      },
    };
  }
});
