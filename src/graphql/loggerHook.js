/*
 * This function hooks into a mutation function and converts the query into
 * an object the represents the query
 *
 * This object is then passed on to a logging function
 */

const Promise = require("bluebird");

module.exports = function loggerHook(schemas){
  for(let schema in schemas){
    if(schemas.hasOwnProperty(schema)){
      schemas[schema].resolve = createHookForResolver(schemas[schema].resolve);
    }
  }
  return schemas;
};

function createHookForResolver(resolveFunc){
  return function resolverLoggingProxy(_, args, req){
    const log = {
      user : req.user,
      query : req.body.query,
      operationName : req.body.operationName || null,
      args : args
    };


    return Promise.try(() => {
      return resolveFunc.apply(this, arguments)
        // Add the result or error to the log
        .tap(result => {
          log.result = result;
        })
        .tapCatch(e => {
          log.error = e;
          log.isError = true;
        })
        .finally(() => sendObjectToLogger(log));
    });
  };
}


// eslint-disable-next-line no-unused-vars
function sendObjectToLogger(log){
  // TODO
}

