// Express middleware for using JWTs and converting them into user objects
const AuthUser = require("./AuthUser");
const { checkToken } = require("./tokens");
const { AuthenticationError, convertErrorToGQLJSON } = require("../api/Errors");


module.exports = function express_auth_middleware(req, res, next){
  // Token from: -------Header---------
  let token = req.get("Authorization")
    //--------------------------------- GQL-V ----------------------------------
    || (req.body && req.body.variables && (req.body.variables.token || req.body.variables.t))
    //--------- Query string ---------
    || (req.query && (req.query.token || req.query.t))
    //---- Result body (json) -----
    || (req.body && (req.body.token || req.body.t))
    //------------- Cookie --------------
    || (req.cookies && (req.cookies.token || req.cookies.t));


  if(token){
    checkToken(token)
    .then(data => {
      req.user = new AuthUser(data);
      next();
    }).catch(oError => {
      if(!oError){
        oError = new AuthenticationError("Token is invalid", "tokens.checkToken");
      }
      res.status(oError.statusCode || 401).json(convertErrorToGQLJSON(oError));
    });
  }else{
    req.user = AuthUser.none;
    next();
  }
};

