// Deals with creation/verification of tokens
const Promise = require("bluebird");
const crypto = require("crypto");
const fs = require("fs");
const path = require("path");

const { AuthenticationError } = require("../api/Errors.js");

const jwt = Promise.promisifyAll(require("jsonwebtoken"));
const config = require("config");

const expiryTime = config.get("auth.tokens.expiry");
let secret;

if(config.has("auth.tokens.secret") && config.get("auth.tokens.secret") !== null){
  secret = config.get("auth.tokens.secret");
}else{
  let filePath = path.join(__dirname, "..", "..", "secret");
  try{
    // Try to find a token file
    // Done on startup, so sync is fine
    let buffer = fs.readFileSync(filePath); // eslint-disable-line no-sync
    if(!buffer){
      throw new Error();
    }
    secret = buffer.toString();
  }catch(e){
    // Not found, generate one
    secret = crypto.randomBytes(20).toString("hex");
    fs.writeFileSync(filePath, secret); // eslint-disable-line no-sync
  }
}

const TOKEN_VERSION = 2;

module.exports = {
  // Generate a JWT
  generateToken(ID, loginID, role){
    let obj = { i : ID, l : loginID, r : role, v : TOKEN_VERSION };
    return jwt.signAsync(obj, secret, { expiresIn : expiryTime });
  },

  // Verify a JWT
  checkToken(token){
    return jwt.verifyAsync(token, secret)
      .then(function(data){
        if(!data.i || !data.l){
          throw new Error();
        }
        // Check which version of the token we are on
        if(data.v !== TOKEN_VERSION){
          throw new Error();
        }

        return {
          ID : data.i,
          loginID : data.l,
          role : data.r
        };
      })
      .catch(() => {
        throw new AuthenticationError("Token is invalid", "tokens.checkToken");
      });
  }

};

