// Object that allows authorisation of server actions
const Promise = require("bluebird");
const api = require("../api");

function AuthUser(user = {}){
  this.ID = user.ID;
  this.loginID = user.loginID;
  this.role = Object.assign({}, user.role);

  this.cache = {};
}

AuthUser.prototype.toPlainObject = function toPlainObject(){
  return {
    ID : this.ID,
    loginID : this.loginID,
    role : this.role
  };
};

AuthUser.prototype.toJSON = function toJSON(){
  return this.toPlainObject();
};

AuthUser.prototype.getID = function getID(){
  return this.ID;
};
AuthUser.prototype.getLoginID = function getLoginID(){
  return this.loginID;
};
AuthUser.prototype.getRole = function getRole(){
  return this.role;
};
AuthUser.prototype.isLoggedIn = function isLoggedIn(){
  return this.ID !== undefined;
};

// Functions defined for use here
let authFunctions = {
  // Role based functions
  isAdmin(){
    // If this function is hit then the user is not an admin
    // as that check is done in the proxy function.
    // So always return false 
    return false;
  },
  hasRole(roleName){
    return this.role && this.role.name === roleName;
  },
  hasOneOfRoles(roleNames){
    return this.role && roleNames.includes(this.role.name);
  },
  isResourcingManager(){
    return this.hasRoleRaw("RESOURCING_MANAGER");
  },
  isVettingAdmin(){
    return this.hasRoleRaw("VETTING_ADMIN");
  },
  isAProjectManager(){
    return api.Project.search({ projectManagerID : this.ID }, AuthUser.su)
      .then(results => results.length > 0);
  },



  // Declarative functions
  canCreateProjects(){
    return this.isResourcingManagerRaw();
  },

  canCreateDiscoveryProject(){
    return !!this.ID;
  },

  canCreatePotentialProject(){
    return !!this.ID;
  },

  canModifyProject(project){
    if(this.isResourcingManagerRaw()){
      return true;
    }

    return !!this.ID && this.getAPIObject("Project", project, [ "projectManagerID", "submittedBy" ]).then(project => {
      return !!project && (this.ID === project.projectManagerID || this.ID === project.submittedBy);
    });
  },

  canSeeProject(project){
    if(this.isResourcingManagerRaw()){
      return true;
    }

    return this.getAPIObject("Project", project, [ "projectManagerID", "submittedBy", "sensitivity" ]).then(project => {
      return !!project && (
        this.ID === project.projectManagerID ||
        this.ID === project.submittedBy ||
        project.sensitivity <= 1
      );
    });
  },

  canSeeRole(role){
    if(this.isResourcingManagerRaw()){
      return true;
    }

    return this.getAPIObject("Role", role, [ "advertise", "projectID" ]).then(role => {
      if(!role){
        return false;
      }

      return this.getAPIObject("Project", role.projectID, [ "projectManagerID", "submittedBy", "sensitivity" ]).then(project => {
        // Project managers can see it
        if(this.ID === project.projectManagerID){
          return true;
        }

        if(!role.advertise){
          return false;
        }

        // If they cannot see a project, they cannot see a role either
        return this.canSeeProject(project);
      });
    });
  },


  canAddAssignmentEstimate(assignment, project){
    return this.ID && this.getAPIObject("Assignment", assignment, [ "staffID" ]).then(assignment => {
      if(assignment.staffID === this.ID){
        return true;
      }else{
        let prom = (project) ?
          this.getAPIObject("Project", project, [ "projectManagerID" ]) :
          api.Assignment.getProjectForAssignment({ assignmentID : assignment.ID });

        return prom.then(project => project.projectManagerID === this.ID);
      }
    });
  },

  canGetAttachment(attachment){ // eslint-disable-line
    return this.isResourcingManagerRaw(); // Only resoucing managers can get this
  },
  canUploadAttachmentForStaffMember(staff){ // eslint-disable-line
    return this.isResourcingManagerRaw();
  },


  canCreateStaffMembers(){
    return this.isResourcingManagerRaw();
  },

  canEditStaffMember(staffMember){ // eslint-disable-line
    // Alias for isResourcingManager, for now
    return this.isResourcingManagerRaw();

    /*
    return this.ID && this.getAPIObject("Staff", staff, [ "ID" ]).then(staffMember => {
      return this.ID === staffMember.ID;
    });
    */
  },

  canEditStaffBio(staffMember){
    // Staff can edit their own bios
    return !!this.ID && this.getAPIObject("Staff", staffMember, [ "ID" ]).then(staffMember => {
      return this.ID === staffMember.ID;
    });
  },


  canCreateClearanceApplications(){
    return this.isVettingAdminRaw();
  },

  canSeeClearanceApplication(clearanceApplication){
    return this.canCreateClearanceApplications()
      .then(can => {
        if(can){
          return true;
        }


        return !!this.ID && this.getAPIObject("ClearanceApplication", clearanceApplication, [ "staffID" ]).then(clearanceApplication => {
          return clearanceApplication && clearanceApplication.staffID === this.ID;
        });
      });
  },


  canCreateMentorListing() {
    return !!this.ID;
  },
  canCreateMenteeListing() {
    return !!this.ID;
  },

  canEditMenteeListing(menteeListing){
    return this.getAPIObject("MenteeListing", menteeListing, [ "staffID" ]).then(data => {
      return !!data && this.ID === data.staffID;
    });
  },
  canEditMentorListing(mentorListing){
    return this.getAPIObject("MentorListing", mentorListing, [ "staffID" ]).then(data => {
      return !!data && this.ID === data.staffID;
    });
  },

  canCreateMentorMenteeRelationship(menteeListing, mentorListing){
    // Check if they are a staff member associated with a mentor or mentee lisiting
    return Promise.join(
      this.canEditMenteeListing(menteeListing),
      this.canEditMentorListing(mentorListing),
      ((a, b) => a || b)
    );
  },

  canEditMentorMenteeRelationship(mentorRelationship){
    return this.getAPIObject("MentorRelationship", mentorRelationship, [ "menteeListingID", "mentorListingID" ]).then(currentMR => {
      return this.canCreateMentorMenteeRelationship(currentMR.menteeListingID, currentMR.mentorListingID);
    });
  }
};





// Util functions for auth functions
/*
 * Gets an API object (of type objectName) with the wanted fields
 * Will reuse the supplied obj if it is passed in with the wanted fields
 * Or will get from the DB if not
 * If wanted is not supplied, [ID] is assumed
 */
AuthUser.prototype.getAPIObject = function getAPIObject(objectName, object, wanted){
  return Promise.try(() => {
    if(!object){
      return null;
    }

    if(!Array.isArray(wanted) || wanted.length === 0){
      if(object.ID){
        return object;
      }else{
        return {
          ID : object
        };
      }
    }

    let hasAll = wanted.every(w => object[w] !== undefined);

    if(hasAll){
      return object;
    }

    let ID = object.ID || object;

    // Use cache if possible
    return (this.cache[objectName] && this.cache[objectName][ID]) || api[objectName].getOne({ ID }, AuthUser.su);
  }).tap(obj => {
    // Store in cache
    if(obj){
      if(!this.cache[objectName]){
        this.cache[objectName] = {};
      }
      this.cache[objectName][obj.ID] = obj;
    }
  });
};




function adminAuthPassthrough(func, this2, args){
  // Check is full permission admin and don't call function if true
  if(this2._isSU || (this2.role && this2.role.name === "ADMIN")){
    return true;
  }else{
    return func.apply(this2, args);
  }
}

function createAuthFunction(func){
  return function authFunctionProxy(){
    return Promise.try(() => adminAuthPassthrough(func, this, arguments));
  };
}

function createRawAuthFunction(func){
  return function rawAuthFunctionProxy(){
    return adminAuthPassthrough(func, this, arguments);
  };
}

/*function createAuthThrowFunction(func){
  let authProxy = createAuthFunction(func);
  return function authFunctionThrowProxy(){
    return authProxy.apply(this, arguments).tap(allowed => { 
      if(!allowed){
        throw new AuthorisationError();
      }
    });
  }
}

AuthUser.prototype.throwOnFail = {};*/

for(let f in authFunctions){
  if(authFunctions.hasOwnProperty(f)){
    AuthUser.prototype[f] = createAuthFunction(authFunctions[f]);
    AuthUser.prototype[`${f}Raw`] = createRawAuthFunction(authFunctions[f]); // Create raw versions
    //AuthUser.prototype.throwOnFail[f] = createAuthThrowFunction(authFunctions[f]);
  }
}



AuthUser.none = new AuthUser();
AuthUser.su = Object.assign(
  new AuthUser({ ID : 0, loginID : "systemuser" }),
  { _isSU : true }
);


module.exports = AuthUser;
