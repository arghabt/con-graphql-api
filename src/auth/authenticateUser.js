// Authenticates the username and password
const { getOne } = require("../database");
const { generateToken } = require("./tokens");

const Promise = require("bluebird");
const request = require("request-promise");
const config = require("config");
const { sysLogger } = require("../logger");

const { AuthenticationError } = require("../api/Errors.js");

const useDummyAuth = config.get("auth.dummyAuth");
let authenticationFunction;
let issUrl;

if(!useDummyAuth){
  issUrl = config.get("auth.issUrl");
  authenticationFunction = issAuth;
  sysLogger.info("Using ISS Auth");
  sysLogger.info("ISSUrl:", issUrl);
}else{
  authenticationFunction = dummyAuth;
  sysLogger.info("Using dummy Auth");
}

function authenticateUser(loginID, password){
  return authenticationFunction(loginID, password)
    .tap(authenticated => {
      if(!authenticated){
        throw new AuthenticationError("Invalid loginID and password", "authenticateUser");
      }
    })
    .then(() => {
      return getRoleInfo(loginID)
        .tap(info => {
          if(!info || !info.ID){
            throw new AuthenticationError("You are not listed in the resource database, please contact Jonathan Blakesley for access.", "authenticateUser");
          }
        })
        .then(info => {
          return generateToken(info.ID, info.loginID, info.role).then(token => {
            return {
              token : token,
              user : info
            };
          });
        });
    });
}

function dummyAuth(loginID, password){
  // Will login a user with the same loginID as password
  return Promise.resolve(
    (loginID && password)
  && loginID === password
  );
}

function issAuth(loginID, password){
  return request({
    uri : issUrl,
    auth: {
      user : loginID,
      pass : password
    }
  }).promise().return(true).catchReturn(false);
}


function getRoleInfo(loginID){
  let query = `SELECT [staff].[Staff].[ID], [staff].[Staff].[loginID], [staff].[Admins].[role], [staff].[AdminRoles].[name] AS 'roleName'
    FROM [staff].[Staff]
    LEFT JOIN [staff].[Admins] ON [staff].[Admins].[userID] = [staff].[Staff].[ID]
    LEFT JOIN [staff].[AdminRoles] ON [staff].[Admins].[role] = [staff].[AdminRoles].[ID]
    WHERE [staff].[Staff].[loginID] = @loginID`;

  return getOne(query, [ "loginID", "NVarChar", loginID ])
    .then(staff => {
      return staff && {
        ID : staff.ID,
        loginID : staff.loginID,
        role : {
          ID : staff.role,
          name : staff.roleName
        }
      };
    });
}


module.exports = authenticateUser;
