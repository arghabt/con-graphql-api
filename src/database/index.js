// Database

const sql = require("mssql");
const Promise = require("bluebird");

sql.Promise = Promise;

let databases = {
  main : require("./main"),
  extra : require("./extra")
};

function initAll(){
  return Promise.each(Object.keys(databases), k => databases[k].init())
    .return(databases);
}

module.exports = Object.assign({
  initAll,
  sql,
  databases
}, databases.main);

