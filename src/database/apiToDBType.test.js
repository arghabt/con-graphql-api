const sql = require("mssql");
const apiToDBType = require("./apiToDBType");

test("returns correctly for 'Int'", () => {
  expect(apiToDBType("Int")).toBe(sql.Int);
});

test("returns correctly for 'PK'", () => {
  expect(apiToDBType("PK")).toBe(sql.Int);
});

test("returns correctly for 'FK'", () => {
  expect(apiToDBType("FK")).toBe(sql.Int);
});

test("returns correctly for all valid types", () => {
  let types = sql.TYPES;
  let typeNames = Object.keys(types);
  typeNames.forEach(n => {
    expect(apiToDBType(n)).toBe(types[n]);
  });
});

test("throws for invalid value", () => {
  expect(() => apiToDBType("definitely not a type")).toThrow();
});