const database = require("./index");
const config = require("config");


function testBasicCall(){
  return database.queryDB.apply(database, [
    "SELECT * FROM dbo.SchemaVersion"
  ].concat(arguments))
    .then(results => expect(results.length).toBeGreaterThan(0));
}

test("can connect to the database", () => {
  return database.init();
});

test("can get correct database version", () => {
  const verToBe = config.get("db.version");
  expect(database.getDatabaseVersion()).toBe(verToBe);
});

test("can query the database", () => {
  return testBasicCall();
});

// Skip because of console dumping
test.skip("a bad query will reject", () => {
  return database.queryDB("SELECT * FROM dbo.SchemaVersion WHERE notaproperty = @invalidParam")
    .then(() => { throw new Error() })
    .catch(() => true);
});

test("can call a function", () => {
  return database.execFunction("dbo.SplitString", [
    [ "a", undefined, "a,b,c"],
    [ "b", undefined, ","]
  ])
    .map(result => result.Item)
    .then(results => {
      expect(results).toEqual(["a", "b", "c"]);
    })
});

test("can get one by ID", () => {
  return database.getOneByID("SELECT * FROM core.FundingTypes WHERE ID = @ID", 1)
    .then(result => expect(result).toBeDefined());
});


test("can get a transaction", () => {
  const sql = require("mssql");
  return database.getTransaction()
    .tap(transaction => expect(transaction).toBeInstanceOf(sql.Transaction))
    .tap(transaction => testBasicCall(transaction))
    .then(transaction => transaction.commit());
});

test("can rollback a transaction", () => {
  return database.getTransaction()
    .tap(transaction => database.queryDB("INSERT INTO core.FundingTypes (name) VALUES (@name)", [ [ "name", undefined, "TEST" ] ], transaction))
    .tap(transaction => 
      database.queryDB("SELECT * FROM core.FundingTypes WHERE name = 'TEST'", undefined, transaction)
        .then(results => expect(results.length).toBeGreaterThan(0))
    )
    .then(transaction => transaction.rollback())
    .then(() =>
      database.queryDB("SELECT * FROM core.FundingTypes WHERE name = 'TEST'")
        .then(results => expect(results.length).toBe(0))
    )
});
