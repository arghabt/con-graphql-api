const sql = require("mssql");

module.exports = function apiToDBType(type){
  switch(type){
    // Int is an example; most types can be caught the the default case
    case "Int" : return sql.Int;

    case "FK" :
    case "PK" :
      return sql.Int;



    default : // eslint-disable-line
      let i = sql.TYPES[type];
      if(!i){
        throw new Error("Type not recognised. Please add a mapping to database/apiToDBType.js");
      }
      return i;
  }
};
