const createDatabase = require("./createDatabase");
const config = require("config");

let extraDBConfig = config.util.cloneDeep(config.get("db-extra"));

module.exports = createDatabase(extraDBConfig);
