// Database object

const sql = require("mssql");
const Promise = require("bluebird");
const config = require("config");
const { sysLogger } = require("../logger");
const apiToDBType = require("./apiToDBType");


function guaranteeArray(arr){
  if(Array.isArray(arr) && arr.length){
    if(!Array.isArray(arr[0])){
      arr = [ arr ];
    }
  }else{
    arr = [];
  }
  return arr;
}

function errorConverter(err){
  sysLogger.debug("Database error:");
  sysLogger.debug(err.query);
  sysLogger.debug(err);
  throw err; // TODO
}

function applyOpts(opts, r){
  opts = guaranteeArray(opts);

  opts.forEach((opt) => {
    if(opt[1]){
      r.input(opt[0], apiToDBType(opt[1]), opt[2]);
    }else{
      r.input(opt[0], opt[2]);
    }
  });
}


function createDatabase(cnf){
  if(!cnf.version){
    throw new Error("database version needs to be provided");
  }

  cnf = config.util.cloneDeep(cnf);

  // Add extra options to the cnf
  cnf.options = Object.assign({}, cnf.options, {
    // Look into using this instead of manual rollbacks
    // This is probably a safer option
    // abortTransactionOnError : true
  });

  let ex = {
    config : cnf,
    connection : null,
    version : cnf.version,

    databaseName : cnf.database,

    init(){
      let conn = new sql.ConnectionPool(ex.config);

      return conn.connect(ex.config)
        .then(conn => {
          ex.connection = conn;
        })
        .then(() => {
          // Check the database version the server should be running against
          let query = `
            SELECT TOP (1) [major], [minor], [build], [revision]
              FROM [dbo].[SchemaVersion]
            ORDER BY [timestampCreated] DESC
          `;

          return ex.getOne(query)
            .then(({ major, minor, build, revision }) => {
              const dbV = `${major}.${minor}.${build}.${revision}`;
              if(dbV !== ex.version){
                throw new Error(`Database version does not match version in config file. Database: ${dbV}, config: ${ex.version}`);
              }
            });
        })
        .return(ex);
    },

    close(){
      return ex.connection && ex.connection.close();
    },

    getDatabaseVersion(){
      return ex.version;
    },

    getTransaction(){
      let transaction = ex.connection.transaction();
      return transaction.begin();
    },

    streamQuery(query, opts, transaction){
      return Promise.try(() => {
        // Use transaction if given, otherwise just use connection
        let r = (transaction || ex.connection).request();
        r.stream = true;
        applyOpts(opts, r);

        r.query(query);
        return r;
      }).catch(errorConverter);
    },

    queryDB(query, opts, transaction){
      return Promise.try(() => {
        // Use transaction if given, otherwise just use connection
        let r = (transaction || ex.connection).request();
        applyOpts(opts, r);

        return r.query(query).get("recordset");
      })
      .catch(e => {
        e.query = query;
        e.params = opts;
        e.inTransaction = !!transaction;
        throw e;
      })
      .catch(errorConverter);
    },

    getOne(query, opts, transaction){
      return ex.queryDB(query, opts, transaction)
        .get(0);
    },

    getOneByID(query, ID, transaction){
      return ex.getOne(query, [ "ID", "Int", ID ], transaction);
    },


    execProcedure(procName, inputs, transaction){
      return Promise.try(() => {
        // Use transaction if given, otherwise just use connection
        let r = (transaction || ex.connection).request();
        applyOpts(inputs, r);

        return r.execute(procName);
      }).catch(errorConverter);
    },

    getOneFromProcedure(procName, inputs, transaction){
      return ex.execProcedure(procName, inputs, transaction)
        .get(0);
    },

    // Wraps the function in a simple query for execution
    execFunction(functionName, inputs, transaction){
      inputs = guaranteeArray(inputs);

      let query = `SELECT * FROM ${functionName}(${
        inputs.map(([ name ]) => `@${name}`).join(",")
      })`;

      return ex.queryDB(query, inputs, transaction);
    },

    getOneFromFunction(functionName, inputs, transaction){
      return ex.execFunction(functionName, inputs, transaction)
        .get(0);
    },


  };

  return ex;
}


module.exports = createDatabase;
