const createDatabase = require("./createDatabase");
const config = require("config");

let mainDBConfig = config.util.cloneDeep(config.get("db"));

module.exports = createDatabase(mainDBConfig);
