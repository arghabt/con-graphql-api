// Entry point
const config = require("config");
const { sysLogger, requestLogger } = require("./src/logger");
const database = require("./src/database");
const Promise = require("bluebird");

sysLogger.info("Running in: " + config.util.getEnv("NODE_ENV") + " mode");

// Init the database
database.initAll().then(() => {
  const express = require("express");
  const cors = require("cors");
  const bodyParser = require("body-parser");
  const cookieParser = require("cookie-parser");
  const multer = require("multer");
  const graphqlHTTP = require("express-graphql");

  const storage = multer.memoryStorage();
  const upload = multer({ storage });


  const graphql = require("./src/graphql");
  const { APIError, APIInputError, convertErrorToGQLJSON } = require("./src/api/Errors");
  const { express_auth_middleware } = require("./src/auth");
  const { StaffAttachment } = require("./src/api");

  const app = express();

  app.use(bodyParser.json());
  app.use(cookieParser());


  let graphQLEndpoint = config.get("server.graphQLEndpoint");
  let fileEndpoint = config.get("server.fileEndpoint");
  let sendExtendedErrors = config.get("server.sendExtendedErrors");


  // Set CORS only on the gql endpoint
  // Additionally only allow post requests
  // as this is all GQL needs to accept
  // Set origin to true to reflect instead of
  // use a wildcard (*)
  app.use([ graphQLEndpoint, fileEndpoint ], cors({
    methods : "POST",
    origin : true
  }));

  app.use([ graphQLEndpoint, fileEndpoint ], express_auth_middleware); // Add the authentication middleware

  app.use(graphQLEndpoint, graphqlHTTP({
    schema: graphql.schema,
    graphiql: config.get("server.useGraphiQL") ? "local" : false,
    formatError: error => {
      let oError = error.originalError;

      if(!(oError instanceof APIError)){
        requestLogger.error(oError);

        if(!sendExtendedErrors){
          oError = new Error("Internal Server Error");
        }
      }else{
        if(oError){
          requestLogger.warn(oError);
        }
      }

      return {
        type : oError && oError.constructor.name,
        message : (!sendExtendedErrors && oError) ? oError.message : error.message,

        field : oError && oError.field,
        method : oError && oError.method,
        statusCode : oError && oError.statusCode,

        // GraphQL fields
        locations : error.locations,
        path : error.path,
        stack : sendExtendedErrors && error.stack
      };
    }
  }));



  function getStaffAttachment(req, res){
    StaffAttachment.getFile(req.params, req.user)
      .then(a => res.send(a))
      .catch(e => res.status(e.statusCode || 500).send(e.message));
  }

  app.route(fileEndpoint + "/:staffID/:attachmentID/:filename").get(getStaffAttachment).post(getStaffAttachment);


  /*
   * Upload an attachment using multipart/form-data
   * Requires a staff member and a file (staffID, attachment)
   *
   * Can also provided a filename, if not provided it is inferred from the uploaded filename
   */
  app.post(fileEndpoint + "/upload", upload.single("attachment"), function saveStaffAttachement(req, res){
    return Promise.try(() => {
      if(!req.file){
        // Error
        throw new APIInputError("No attachment provided", "attachment", "staffAttachment.saveFile");
      }

      let { staffID, filename } = req.body;
      if(!staffID){
        throw new APIInputError("No staff member provided", "staffID", "staffAttachment.saveFile");
      }


      if(!filename){
        // No filename is passed in, so derive from the uploaded file
        filename = req.file.originalname;
      }

      return StaffAttachment.saveFile({ staffID, filename, attachment : req.file.buffer }, req.user);
    })
    .then(() => {
      res.json({ success : true });
    })
    .catch(e => {
      res.status(e.statusCode || 500).json(convertErrorToGQLJSON(e));
    });
  });



  app.listen(config.get("server.port"));

  sysLogger.info(`Listening on: http://localhost:${config.get("server.port")}/graphql`);
}).catch(e => {
  for(let d in database.databases){
    if(database.databases.hasOwnProperty(d)){
      database.databases[d].close();
    }
  }

  sysLogger.error("Error on startup:");
  throw e;
});
